//
//  SearchCategoryCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 02/02/23.
//

import UIKit
import IQKeyboardManagerSwift

class SearchCategoryCell: UITableViewCell, UITextFieldDelegate {

 //Outlets
    @IBOutlet weak var dropimg: UIImageView!
    @IBOutlet weak var dropviewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropview: BaseView!
    @IBOutlet weak var categoryView: BaseView!
    @IBOutlet weak var catgoryviewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropdownBtn: UIButton!
    @IBOutlet weak var CategoryTV: UITableView!
    @IBOutlet weak var searchView: BaseView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    
    var Ssearching:Bool = false
    var catarray = [[String:Any]]()
    var catlist = [getcatDataModel]()
    var delegate : searchdeletearraydelegate?
    var selectedArray = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        searchTextField.delegate = self
        print(selectedArray.count)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
 //MARK: - Textfield delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
//    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
//        SsearchArrRes = Snewcatarray
//        NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)
//        return true
//    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
         //input text
        let searchText  = textField.text! + string
         //add matching text to arrya
        SsearchArrRes = searchText.isEmpty ? Snewcatarray : Snewcatarray.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})
        print("search count",SsearchArrRes.count)
         if(SsearchArrRes.count == 0){
             Ssearching = false
             NotificationCenter.default.post(name: Notification.Name("false"), object: nil)
         }else{
             Ssearching = true
             NotificationCenter.default.post(name: Notification.Name("true"), object: nil)
         }
        NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)
         
         return true
     }
    
    @IBAction func dismissBTNTapped(_ sender: UIButton) {
        print(sender.tag)
        
        if selectedArray.count >= 1 {
            catgoryviewHeight.constant = CGFloat(selectedArray.count * 50)
            print("height",selectedArray.count)
        }
        else if selectedArray.count == 1{
            catgoryviewHeight.constant = CGFloat(selectedArray.count * 50)
        }
        else{
            print("height norml",selectedArray.count)
            catgoryviewHeight.constant = 50
        }
        
        CategoryTV.reloadData()
        let indexPathToRefresh = IndexPath(row: 0, section: 0)
        self.delegate?.diddelete(index: sender.tag)
    }
    
    func textBorderSetup(){
        categoryView.layer.borderColor = UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1).cgColor
        categoryView.layer.borderWidth = 1
    }
}

extension SearchCategoryCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchcategoryTVcell", for: indexPath) as! SearchcategoryTVcell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchcategoryCVcell", for: indexPath) as! SearchcategoryCVcell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = collectionView.frame.width
        let h:CGFloat = 32
        return CGSize(width: w, height: h)
    }
}


class SearchcategoryCVcell:UICollectionViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
}
class SearchcategoryTVcell:UITableViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
}
