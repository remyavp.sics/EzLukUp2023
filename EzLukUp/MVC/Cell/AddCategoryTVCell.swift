//
//  AddCategoryTVCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 21/11/22.
//

import UIKit

// MARK: -Tableview first cell
class AddCategoryTVCell: UITableViewCell,UITextFieldDelegate {

 //MARK:- Outlets
    @IBOutlet weak var categoryview: BaseView!
    @IBOutlet weak var categorycollectionview: UICollectionView!
    @IBOutlet weak var dropdownimg: UIImageView!
    @IBOutlet weak var categoryviewheight: NSLayoutConstraint!
    @IBOutlet weak var dropdownview: BaseView!
    @IBOutlet weak var dropdownHeight: NSLayoutConstraint!
    
    @IBOutlet weak var categoryTV: UITableView!
    @IBOutlet weak var searchview: BaseView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchviewheight: NSLayoutConstraint!
    
    var Searching:Bool = false
    var catarray = [[String:Any]]()
    var catlist = [getcatDataModel]()
    var delegate : Categorydeletearraydelegate?
    var selectedArray = [String]()
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        print(selectedArray.count)
        searchTextField.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func textBorderSetup(){
        categoryview.layer.borderColor = UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1).cgColor
        categoryview.layer.borderWidth = 1
    }
    
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
      
       let searchText  = textField.text! + string
        //add matching text to arrya
       searchArrayRes = searchText.isEmpty ? newCatarray : newCatarray.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})
       print("search count",searchArrayRes.count)
        if(searchArrayRes.count == 0){
            Searching = false
            NotificationCenter.default.post(name: Notification.Name("false"), object: nil)
        }else{
            Searching = true
            NotificationCenter.default.post(name: Notification.Name("true"), object: nil)
        }
       NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)

        
        return true
    }

    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        print(sender.tag)
        
        if selectedArray.count >= 1 {
            categoryviewheight.constant = CGFloat(selectedArray.count * 40)
            print("height",selectedArray.count)
        }
        else if selectedArray.count == 1{
            categoryviewheight.constant = CGFloat(selectedArray.count * 50)
        }
        else{
            print("height norml",selectedArray.count)
            categoryviewheight.constant = 50
        }
        
        categoryTV.reloadData()
        let indexPathToRefresh = IndexPath(row: 0, section: 0)
        self.delegate?.diddelete(index: sender.tag)
    }
    }
    
   

// MARK: -Tableview second cell
class AddSecondCell: UITableViewCell,UITextViewDelegate{
    
    @IBOutlet weak var descriptionTextview: UITextView!
    @IBOutlet weak var saveBTN: UIButton!
    @IBOutlet weak var checkboxIMG: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        descriptionTextview.delegate = self
        descriptionTextview.text = "Enter your feedback for the services"
        descriptionTextview.textColor = UIColor.lightGray
        descriptionTextview.returnKeyType = .done
    }

    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if descriptionTextview.text == "Enter your feedback for the services" {
                    descriptionTextview.text = ""
                    descriptionTextview.textColor = UIColor.black
                    descriptionTextview.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if descriptionTextview.text == "" {
                    descriptionTextview.text = "Enter your feedback for the services"
                    descriptionTextview.textColor = UIColor.lightGray
                    descriptionTextview.font = UIFont(name: "jost", size: 13.0)
                }
                else{
                    Typedabout = descriptionTextview.text
                }
            }
    
    
}

//MARK: - Collectionview & Tableview
extension AddCategoryTVCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource{
    //MARK:- Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categorytableCell", for: indexPath) as! categorytableCell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBTN.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
 //MARK:- Collectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddCategoryCVCell", for: indexPath) as! AddCategoryCVCell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = collectionView.frame.width
        let h:CGFloat = 32
        return CGSize(width: w, height: h)
    }
    
 
}


class AddCategoryCVCell: UICollectionViewCell{
    
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
}

class categorytableCell:UITableViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBTN: UIButton!
}
