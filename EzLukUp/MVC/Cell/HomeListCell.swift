//
//  HomeListCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 20/10/22.
//

import UIKit


class HomeListCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

//Dashboard fisrt cell
class CardviewCell:UITableViewCell{
    @IBOutlet weak var CellLbl:UILabel!
    @IBOutlet weak var Celimg: UIImageView!
    @IBOutlet weak var CellLbl1:UILabel!
    @IBOutlet weak var CellLbl2:UILabel!
    @IBOutlet weak var rewardcountLBL:UILabel!
    @IBOutlet weak var generalcontactscountLBL:UILabel!
    @IBOutlet weak var providercountLBL:UILabel!
}
//Dashboard second cell

class Listcell:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var recomendationCV:UICollectionView!
}
//Dashboard third cell

class Listcell2:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var recomendationCV:UICollectionView!
}
//Dashboard fourth cell

class Listcell3:UITableViewCell{
    var Dashboardmodel : DashboardDataModel?
    @IBOutlet weak var recomendationCV:UICollectionView!
}
extension Listcell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 10
        return Dashboardmodel?.Data?.recentRecommendProviders.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell", for: indexPath) as! listcollectioncell
        cell.nameLBL.text = Dashboardmodel?.Data?.recentRecommendProviders[indexPath.item].contacts?.name ?? ""
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 2.3
               // let h:CGFloat = (collectionView.frame.size.height - 10) / 1
                return CGSize(width: w, height: 212.0)
    }
}
//second list cell collectionview delegates
extension Listcell2:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        return 10
        return Dashboardmodel?.Data?.recentFeedback.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell2", for: indexPath) as! listcollectioncell2
        cell.feedbackLBL.text = Dashboardmodel?.Data?.recentFeedback[indexPath.item].feedback
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 1.5
              
        return CGSize(width: w, height: 200)
    }
}

//third list cell collectionview delegates
extension Listcell3:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "listcollectioncell3", for: indexPath) as! listcollectioncell3
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = (collectionView.frame.size.width - 8) / 2.3
               // let h:CGFloat = (collectionView.frame.size.height - 10) / 1
                return CGSize(width: w, height: 212.0)
    }
}
class listcollectioncell:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var daysagoLBL:UILabel!
}
class listcollectioncell2:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var feedbackLBL:UILabel!
    @IBOutlet weak var daysagoLBL:UILabel!
}
class listcollectioncell3:UICollectionViewCell{
    @IBOutlet weak var profileImage:UIImageView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var locationLBL:UILabel!
}
