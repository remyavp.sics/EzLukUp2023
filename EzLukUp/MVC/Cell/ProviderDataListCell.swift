//
//  ProviderDataListCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 25/10/22.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire

class ProviderDataListCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//Tableview fisrt cell
class Firstcell:UITableViewCell,UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource{
    
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var delegate:providerdatadelegate!
    
    @IBOutlet weak var userImageview: BaseImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var locationTextField: BaseTextField!
    @IBOutlet weak var searchResultView: UIView!
    @IBOutlet weak var searchResultheight: NSLayoutConstraint!
    @IBOutlet weak var googlelocationBTN: UIButton!
    @IBOutlet weak var loctable:UITableView!
    @IBOutlet weak var nameasterikImagview: UIImageView!
    @IBOutlet weak var locasterikImagview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTextField.delegate = self
       // locationTextField.delegate = self
        
//        locationTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
//        self.nameasterikImagview.isHidden = true
//        self.locasterikImagview.isHidden = true
    }
    

    @objc func doneButtonClicked(_ sender: Any) {
        self.delegate.firtcell(cell: self, didselect: locationTextField.text ?? "")
        Providersignupname = nameTextField.text ?? ""
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    //
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == nameTextField {
            Providersignupname = nameTextField.text ?? ""
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == nameTextField{
            Providersignupname = nameTextField.text ?? ""
            userdetails().saveuserdetail(name: nameTextField.text ?? "", searchtext: "")
        }
        if textField == locationTextField{
           
            self.delegate.firtcell(cell: self, searchtext: locationTextField.text ?? "")

//
        }
        return true
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "searchResultCell") as! searchResultCell
        
        cell.locLbl.text = autocompleteResults[indexPath.row].formattedAddress
      
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.firtcell(cell: self, didselect: autocompleteResults[indexPath.row].formattedAddress)
    }
}


// MARK: -Tableview second cell
class SecondCategorycell:UITableViewCell,UITextFieldDelegate{
    
    var searching:Bool = false
    var catarray = [[String:Any]]()
    var catlist = [getcatDataModel]()
    
    
    @IBOutlet weak var dropimg: UIImageView!
    @IBOutlet weak var dropviewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropview: BaseView!
    @IBOutlet weak var categoryView: BaseView!
    @IBOutlet weak var catgoryviewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropdownBtn: UIButton!
    @IBOutlet weak var CategoryTV: UITableView!
    @IBOutlet weak var CVheight: NSLayoutConstraint!
    @IBOutlet weak var searchView: BaseView!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    
    
    var delegate : deletearraydelegate?
    var selectedArray = [String]()
    override func awakeFromNib() {
        super.awakeFromNib()
        searchTF.delegate = self
        print(selectedArray.count)
    }
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        searchArrRes = newcatarray
        NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)
        return true
    }
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
         //input text
        let searchText  = textField.text! + string
         //add matching text to arrya
        searchArrRes = searchText.isEmpty ? newcatarray : newcatarray.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})
        print("search count",searchArrRes.count)
         if(searchArrRes.count == 0){
             searching = false
             NotificationCenter.default.post(name: Notification.Name("false"), object: nil)
         }else{
             searching = true
             NotificationCenter.default.post(name: Notification.Name("true"), object: nil)
         }
        NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)
//         categoryTableView.reloadData()
         
         return true
     }
//
    @IBAction func closeBTntapped (_ sender: UIButton){
        print(sender.tag)
        
        if selectedArray.count >= 1 {
//            CVheight.constant = CGFloat(selectedArray.count * 35)
            catgoryviewHeight.constant = CGFloat(selectedArray.count * 40)
            print("height",selectedArray.count)
        }
        else if selectedArray.count == 1{
            catgoryviewHeight.constant = CGFloat(selectedArray.count * 50)
        }
        else{
            print("height norml",selectedArray.count)
            //CVheight.constant = 50
            catgoryviewHeight.constant = 50
        }
        
        CategoryTV.reloadData()
        let indexPathToRefresh = IndexPath(row: 0, section: 0)
        self.delegate?.diddelete(index: sender.tag)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func textBorderSetup(){
        categoryView.layer.borderColor = UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1).cgColor
        categoryView.layer.borderWidth = 1
    }
}
extension SecondCategorycell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categorytableviewcell", for: indexPath) as! categorytableviewcell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 32
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categorycollectioncell", for: indexPath) as! categorycollectioncell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
   
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = collectionView.frame.width
        let h:CGFloat = 32
        return CGSize(width: w, height: h)
    }
    
    
}

//Tableview third cell
class Thirdcell:UITableViewCell,UITextViewDelegate, UITextFieldDelegate{
    
    @IBOutlet weak var phoneNumTextField: BaseTextField!
    @IBOutlet weak var companyNameTxt: BaseTextField!
    @IBOutlet weak var personalTxtField: BaseTextField!
    @IBOutlet weak var timeTextField: BaseTextField!
    @IBOutlet weak var descriptionTxt: UITextView!
    @IBOutlet weak var finishBtn: BaseButton!
    @IBOutlet weak var aboutasterikImgvw: UIImageView!
    
    @IBOutlet weak var stateTV: UITableView!
    @IBOutlet weak var cityTV: UITableView!
    @IBOutlet weak var radiusTV: UITableView!
    
    @IBOutlet weak var cityHeightview: NSLayoutConstraint!
    @IBOutlet weak var radiusHeightView: NSLayoutConstraint!
    @IBOutlet weak var stateHeightView: NSLayoutConstraint!
    
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var dropIMG: UIImageView!
    @IBOutlet weak var radiusTF: UITextField!
    @IBOutlet weak var radiusMainView: UIView!
    
    @IBOutlet weak var statesearchview: BaseView!
    @IBOutlet weak var citysearchview: BaseView!
    @IBOutlet weak var radiussearchview: BaseView!
    
    var callback: (() -> Void)?
    var typedcity = ""
    var typedstate = ""
    var countryList : SearchCountryList?
    var stateList : SearchStateList?
    var cityList : SearchCityList?
    
    var activityView: UIActivityIndicatorView?
    var dropstatus = false
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    var radiusArray = ["25 miles","50 miles","75 miles","100 miles"]
    var isClickDown = false
    var typedcountry = "USA"
    var state = ""
    var latitude = "38.00000000"
    var longitude = "-97.00000000"
    var selectedcategory = ""
    var checktableheight : Bool = false
    
    var searchArray = [[String:Any]]()
    var searching:Bool = false
    var statearray = [[String:Any]]()
    var SearchcityArray = [[String:Any]]()
    var cityarray = [[String:Any]]()
    var searchText = ""
    var typedcategory = ""
    var typedradius = ""
    
    
    override func awakeFromNib() {
            super.awakeFromNib()
        companyNameTxt.delegate = self
        timeTextField.delegate = self
        descriptionTxt.delegate = self
            descriptionTxt.text = "About Business"
            descriptionTxt.textColor = UIColor.placeholderText
        descriptionTxt.font = UIFont(name: "jost", size: 15.0)
            descriptionTxt.returnKeyType = .done
        companyNameTxt.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        timeTextField.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        phoneNumTextField.text = providersignupmob
       // finishBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
//        aboutasterikImgvw.isHidden = true
        
        stateTF.delegate = self
        cityTF.delegate = self
        radiusTF.delegate = self
        self.statelistapi()
        cityTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonclicked))
        stateTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonclicked))
        self.radiusMainView.isHidden = true
        }
    
    @objc func doneButtonClicked(_ sender: Any) {
        typedhoursoperation = timeTextField.text ?? ""
        typedcompanyname = companyNameTxt.text ?? ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if descriptionTxt.text == "About Business" {
                    descriptionTxt.text = ""
                    descriptionTxt.textColor = UIColor.black
                    descriptionTxt.font = UIFont(name: "jost", size: 15.0)
                }
            NotificationCenter.default.post(name: NSNotification.Name("aboutclicked"), object: nil, userInfo: nil)
            
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if descriptionTxt.text == "" {
                    descriptionTxt.text = "About Business"
                    descriptionTxt.textColor = UIColor.lightGray
                    descriptionTxt.font = UIFont(name: "jost", size: 15.0)
                }
                else{
                    typedabout = descriptionTxt.text
                }
            }
    
    
    //MARK: - Textfield delegate & functions
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("area of service")
        
       if textField == stateTF{
           
           print("state service")
           statedropstatus = true
           callback?()
//           self.stateHeightView.constant =  350
           
           stateTV.reloadData()
       } else if textField == cityTF{
           self.cityHeightview.constant =  350
       } else if textField == radiusTF{
           self.radiusHeightView.constant =  CGFloat(self.radiusArray.count * 30)
         //  self.radiusTF.text = "25 miles"
           self.radiusTV.reloadData()
       }
       
   }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == companyNameTxt {
            typedcompanyname = companyNameTxt.text ?? ""
        }
        else if textField == cityTF{
            typedcity = cityTF.text ?? ""
         //   cityTVviewheight.constant = 0
            if typedcity == ""{
                self.radiusMainView.isHidden = true
            }else{
                typedradius = self.radiusTF.text ?? ""
                self.radiusMainView.isHidden = false
                self.radiusTF.text = "25 miles"
            }
            
        }else if textField == stateTF{
            typedstate = stateTF.text ?? ""
            stateHeightView.constant = 0
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == companyNameTxt {
            typedcompanyname = companyNameTxt.text ?? ""
        }
        if textField == timeTextField{
            typedhoursoperation = timeTextField.text ?? ""
        }
        if textField == cityTF {
            typedcity = cityTF.text ?? ""
            
        }
        if textField == stateTF{
            typedstate = stateTF.text ?? ""
        }
        return true
    }
    
    @objc func doneButtonclicked(_ sender: Any) {
        typedcity = cityTF.text ?? ""
        typedstate = stateTF.text ?? ""
        searchText = ""
        
        stateHeightView.constant = 0
        cityHeightview.constant = 0
        
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchText  = textField.text! + string
        print(searchText)
        
        if textField == stateTF{
            
            cityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.radiusMainView.isHidden = true
            cityTV.reloadData()
            
            //input text
            
           // statelistapi(state: searchText)
            //add matching text to array
            searchArray = self.statearray.filter({(($0["name"] as! String).localizedCaseInsensitiveContains(searchText))})
            
            if(searchArray.count == 0){
                searching = false
            }
            else{
                searching = true
            }
            self.stateTV.reloadData();
        }
        else if textField == cityTF{
            //input text
//            searchText  = textField.text! + string
//            print(searchText)
           // statelistapi(state: searchText)
            //add matching text to array
            SearchcityArray = self.cityarray.filter({(($0["name"] as! String).localizedCaseInsensitiveContains(searchText))})
            
            if(SearchcityArray.count == 0){
                searching = false
            }else{
                searching = true
            }
            self.cityTV.reloadData();
        }
        
        
        return true
    }
  
    
    //MARK:- Radius BTN Action
    @IBAction func radiusDropBTN(_ sender: UIButton) {
        //dropstatus = !dropstatus
      //  self.searchCategoryTV.reloadSections([2], with: .none)
        if checktableheight == false {
            checktableheight = true
            radiusHeightView.constant = CGFloat(self.radiusArray.count * 30)
            self.dropIMG.image = UIImage(named: "dropup_icon")
        }
        else{
            radiusHeightView.constant = 0
            checktableheight = false
            self.dropIMG.image = UIImage(named: "dropdown_icon")
        }
        self.radiusTV.reloadData()
    }
    
  }

//MARK: - Area of services tableview functions
extension Thirdcell: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == stateTV{
            return 1
        }else if tableView == cityTV{
            return 1
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == stateTV{
            print("searchcount",searchArray)
            //check search text & original text
            if( searching == true){
             return searchArray.count
            }else{
                return statearray.count
            }
        }
        else if tableView == cityTV{
            print("searchcount",SearchcityArray)
            //check search text & original text
            if( searching == true){
             return SearchcityArray.count
            }else{
                return cityarray.count
            }
        }
        else{
            return radiusArray.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if tableView == stateTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "StateTVCell") as! StateTVCell
//             self.stateHeightView.constant = 300
            if( searching == true){
                var dict = searchArray[indexPath.row]
                cell.stateLabel.text = dict["name"] as? String
            }else{
                var dict = statearray[indexPath.row]
                cell.stateLabel.text = dict["name"] as? String

            }
            return cell
        }
        else if tableView == cityTV{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CityTVCell") as! CityTVCell
            
          //  cell.cityLbl.text = citylist?.Data[indexPath.row].name ?? ""
            if( searching == true){
                if SearchcityArray.count > 0{
                    
                    var dict = SearchcityArray[indexPath.row]
                    cell.cityLabel.text = dict["name"] as? String
                }
                
            }else{
                if cityarray.count > 0{
                    
                    var dict = cityarray[indexPath.row]
                    cell.cityLabel.text = dict["name"] as? String
                }
                
               
            }
           
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "RadiusTVCell") as! RadiusTVCell
            cell.radiusLabel.text = radiusArray[indexPath.row]
            return cell
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == stateTV{
            return 30
        }
        
        else if tableView == cityTV{
            return 30
        }
        
        else{
            return 30
        }
    }
   
    
    //MARK: - area of service tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == stateTV{
            if( searching == true){
                let dict = searchArray[indexPath.row]
                print("state name - ",dict["name"] as? String)
                searching = false
                
                stateTF.text = dict["name"] as? String
//                stateTF.resignFirstResponder()
                stateHeightView.constant = 0
                self.state = dict["isoCode"] as? String ?? ""
                self.cityarray.removeAll()
                self.cityTF.text?.removeAll()
                self.cityTV.reloadData()
                self.radiusMainView.isHidden = true
                self.citylistapi(stateiso: dict["isoCode"] as? String ?? "")
                statedropstatus = false
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
                
            }
            else{
               let dict = statearray[indexPath.row]
                print("state name - ",dict["name"] as? String)
                searching = false

                stateTF.text = dict["name"] as? String
                stateHeightView.constant = 0
                self.state = dict["isoCode"] as? String ?? ""
                self.cityarray.removeAll()
                self.cityTF.text?.removeAll()
                self.cityTV.reloadData()
                self.radiusMainView.isHidden = true
                self.citylistapi(stateiso: dict["isoCode"] as? String ?? "")
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
//                self.state = statelist?.Data[indexPath.row].isoCode ?? ""
//                self.citylistapi(stateiso: statelist?.Data[indexPath.row].isoCode ?? "")
//                self.latitude = statelist?.Data[indexPath.row].latitude ?? ""
//                self.longitude = statelist?.Data[indexPath.row].longitude ?? ""
                
            }
        }
        
        else if tableView == cityTV{
            
            if( searching == true){
                let dict = SearchcityArray[indexPath.row]
                print("city name - ",dict["name"] as? String)
                searching = false
                
                cityTF.text = dict["name"] as? String
                cityHeightview.constant = 0
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
            }
            else{
                let dict = cityarray[indexPath.row]
                print("id -" ,dict["_id"] as? String)
                print("city name - ",dict["name"] as? String)
                searching = false
                
                cityTF.text = dict["name"] as? String
                cityHeightview.constant = 0
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
            }
        }
        
        else if tableView == radiusTV{
            radiusTF.text =  radiusArray[indexPath.row]
            radiusTF.resignFirstResponder()
            cityHeightview.constant = 0
          
           // typedradius = self.radiusTF.text ?? ""
        }
    }
}

//MARK: - API's
extension Thirdcell{
    
 //MARK: - Country,State,City API
    func countrylistapi(){
   // showActivityIndicator()
        let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"
              
        
     let url = "http://13.234.177.61/api7/countriesList"
    AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":tokenn]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result)      {
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                 self.countryList = SearchCountryList(from:responsedata)
               //  self.servicestableview.reloadData()
                      
             }
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
  }
 
//MARK: -state
    func statelistapi(){
   // showActivityIndicator()
        let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"
              
        
        let params: [String : Any] = [
                      "country_iso_code": "US"]
    
     let url = "http://13.234.177.61/api7/statesList"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":tokenn]).validate(statusCode: 200..<510) .responseJSON { [self] response in
        print(response)
         switch (response.result)      {
             
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                 self.stateList = SearchStateList(from:responsedata)
                 if let dataa = responsedata["data"] as? [[String:Any]]{
//                        print("dataa = ", dataa)
                    
                     for i in dataa{
                         print("looped data =",i)
                         self.statearray.append(i)
                     }
                 }
             }
                 
                 self.stateTV.reloadData()
                      
           
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
  }

//MARK: -city
    func citylistapi(stateiso:String){
 //   showActivityIndicator()
  let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"
        
        let params: [String : Any] = [
                      "country_iso_code": "US",
                      "state_iso_code": stateiso]
    
     let url = "http://13.234.177.61/api7/citiesList"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":tokenn]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result)      {
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                // self.citylist = SearchCityList(from:responsedata)
                 if let dataa = responsedata["data"] as? [[String:Any]]{

                     for i in dataa{
                         print("looped data =",i)
                         self.cityarray.append(i)
                     }
                 }
             }
             
                 self.cityTV.reloadData()
             
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
}
    
}

//extension Thirdcell{
//    func showActivityIndicator() {
//        activityView = UIActivityIndicatorView(style: .large)
//        activityView?.center = self.view.center
//        self.view.addSubview(activityView!)
//        activityView?.startAnimating()
//    }
//
//    func hideActivityIndicator(){
//        if (activityView != nil){
//            activityView?.stopAnimating()
//        }
//    }
//}


class categorycollectioncell:UICollectionViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
   
}
class categorytableviewcell:UITableViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
   
}

protocol providerdatadelegate{
    func firtcell(cell:Firstcell,searchtext:String)
    func firtcell(cell:Firstcell,didselect item :String)
}


//Ares of service cells

class StateTVCell: UITableViewCell{
    @IBOutlet weak var stateLabel: UILabel!
}

class CityTVCell: UITableViewCell{
    @IBOutlet weak var cityLabel: UILabel!
}

class RadiusTVCell: UITableViewCell{
    @IBOutlet weak var radiusLabel: UILabel!
    
}
