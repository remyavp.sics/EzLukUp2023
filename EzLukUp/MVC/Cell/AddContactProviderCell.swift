//
//  AddContactProviderCell.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/11/22.
//

import UIKit
import Alamofire
import CountryPickerView
import PhoneNumberKit


// MARK: -Tableview first cell
class FirstCell: UITableViewCell,UITextFieldDelegate{
    @IBOutlet weak var nameTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTextField.delegate = self
    }

    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == nameTextField{
            Typedname = nameTextField.text ?? ""
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == nameTextField{
            Typedname = nameTextField.text ?? ""
        }
        return true
    }
    
}


// MARK: -Tableview second cell
class AddContactProviderCell: UITableViewCell,UITextFieldDelegate {

    //MARK:- Outlets
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    @IBOutlet weak var categoryview: BaseView!
    @IBOutlet weak var categoryviewheight: NSLayoutConstraint!
    @IBOutlet weak var dropview: BaseView!
    @IBOutlet weak var dropviewheight: NSLayoutConstraint!
    @IBOutlet weak var dropdownImg: UIImageView!
    
    @IBOutlet weak var categoryTV: UITableView!
    @IBOutlet weak var searchview: BaseView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchviewheight: NSLayoutConstraint!
    
    var search:Bool = false
    var catarray = [[String:Any]]()
    var catlist = [getcatDataModel]()
    var selectedArray = [String]()
    var delegate : categorydeletearraydelegate?
    
        
    override func awakeFromNib() {
        super.awakeFromNib()
       print("selectedArray")
        searchTextField.delegate = self
        SelectedcategoryID.removeAll()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
     }
    


//MARK: - Functions & @IBAction
  
        func textBorderSetup(){
            categoryview.layer.borderColor = UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1).cgColor
            categoryview.layer.borderWidth = 1
        }
        
    //
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool{
      
       let searchText  = textField.text! + string
        //add matching text to arrya
        searcharrayRes = searchText.isEmpty ? NewCatarray : NewCatarray.filter({(($0.name!).localizedCaseInsensitiveContains(searchText))})
       print("search count",searcharrayRes.count)
        if(searcharrayRes.count == 0){
            search = false
            NotificationCenter.default.post(name: Notification.Name("false"), object: nil)
        }else{
            search = true
            NotificationCenter.default.post(name: Notification.Name("true"), object: nil)
        }
       NotificationCenter.default.post(name: Notification.Name("reloadcat"), object: nil)
 
       
        
        return true
    }

    @IBAction func btnCloseTapped(_ sender: UIButton) {
   
    
   
            print(sender.tag)
            
            if selectedArray.count >= 1 {
                categoryviewheight.constant = CGFloat(selectedArray.count * 40)
                print("height",selectedArray.count)
            }
            else if selectedArray.count == 1{
                categoryviewheight.constant = CGFloat(selectedArray.count * 50)
            }
            else{
                print("height norml",selectedArray.count)
                categoryviewheight.constant = 50
            }
            
            categoryTV.reloadData()
            let indexPathToRefresh = IndexPath(row: 0, section: 0)
            self.delegate?.diddelete(index: sender.tag)
     
}
  }
  


extension AddContactProviderCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UITableViewDelegate, UITableViewDataSource {
    
    //MARK:- Tableview
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "categoryTVCell", for: indexPath) as! categoryTVCell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBTN.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    
    //MARK:- Collectionview
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProviderCategoryCollectionCell", for: indexPath) as! ProviderCategoryCollectionCell
        cell.categoryLbl.text = selectedArray[indexPath.row]
        cell.dismissBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = collectionView.frame.width
        let h:CGFloat = 32
        return CGSize(width: w, height: h)
    }
    
}


//MARK:- PopUpTableViewCell
class ProviderCategoryCollectionCell: UICollectionViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
}

class categoryTVCell: UITableViewCell{
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var dismissBTN: UIButton!
}

    
//MARK: - Second cell
class Secondcell: UITableViewCell,UITextViewDelegate,UITextFieldDelegate{
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var flagview: UIView!
    @IBOutlet weak var numberview: UIView!
    @IBOutlet weak var countrypickerview: CountryPickerView!
    @IBOutlet weak var countrycodeLbl: UILabel!
    @IBOutlet weak var feedbackTextView: UITextView!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var checkboxIMG: UIImageView!
    @IBOutlet weak var checkBTN: UIButton!
    
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var code = ""
    var phonenumber = ""
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        feedbackTextView.delegate = self
        feedbackTextView.text = "Enter your feedback for the services"
        feedbackTextView.textColor = UIColor.lightGray
        feedbackTextView.returnKeyType = .done
        mobileTextField.delegate = self
        setUpUI()
        saveBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    }
}

extension Secondcell{
    func setUpUI() {
        self.flagview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.flagview.layer.borderWidth = 1
        self.numberview.layer.borderWidth = 1
        self.numberview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.saveBtn.addCornerForView(cornerRadius: 10.0)
        self.flagview.addCornerForView(cornerRadius: 10.0)
        self.numberview.addCornerForView(cornerRadius: 10.0)
        
        addDoneButtonOnKeyboard()
        countrypickerview.showPhoneCodeInView = false
        countrypickerview.showCountryCodeInView = true
        countrypickerview.showCountryNameInView = false
        countrypickerview.font = .systemFont(ofSize: 15)
        countrypickerview.delegate = self
        countrypickerview.dataSource = self
        countrycodeLbl.text = countrypickerview.selectedCountry.phoneCode
    }
    
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if feedbackTextView.text == "Enter your feedback for the services" {
                    feedbackTextView.text = ""
                    feedbackTextView.textColor = UIColor.black
                    feedbackTextView.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if feedbackTextView.text == "" {
                    feedbackTextView.text = "Enter your feedback for the services"
                    feedbackTextView.textColor = UIColor.lightGray
                    feedbackTextView.font = UIFont(name: "jost", size: 13.0)
                }
                else{
                    Typedfeedback = feedbackTextView.text
                }
            }

    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        mobileTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        mobileTextField.resignFirstResponder()
    }
    
    //MARK:- Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileTextField
        {
            if countrycodeLbl.text == "+1" && mobileTextField.text == ""{
                mobileTextField.placeholder = "(123) 456-7890"
            }else if countrycodeLbl.text == "+91" && mobileTextField.text == ""{
                mobileTextField.placeholder = "1234567890"
            }
            
        }
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        if textField == mobileTextField{
            Typedmobilenum = mobileTextField.text!
            Typedcountry = countrycodeLbl.text!
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField == mobileTextField{
            Typedmobilenum = mobileTextField.text!
            Typedcountry = countrycodeLbl.text!
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)

               if textField == mobileTextField
               {
                   if countrycodeLbl.text == "+1"{
                       print("usa selected")
                       return Secondcell.checkEnglishPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }
                   else{
                       mobileTextField.placeholder = "1234567890"
                       return Secondcell.checkindianPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }

               }
               else
               {
                   Typedmobilenum = mobileTextField.text!
                   Typedcountry = countrycodeLbl.text!
                   return true
               }
    }
    
    
}

//MARK: - CountryPickerViewDelegate, CountryPickerViewDataSource
extension Secondcell: CountryPickerViewDelegate, CountryPickerViewDataSource {
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        code = country.phoneCode
        countrycodeLbl.text = code
       if code  == "+1"{
           mobileTextField.placeholder = "(123) 456-7890"
       }else if code == "+91"{
           mobileTextField.placeholder = "1234567890"
       }

   }
    
   static func checkEnglishPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {

       if string == ""{ //BackSpace

           return true

       }else if str!.count < 3{

           if str!.count == 1{

               phoneNumber.text = "("
           }

       }else if str!.count == 5{

           phoneNumber.text = phoneNumber.text! + ") "

       }else if str!.count == 10{

           phoneNumber.text = phoneNumber.text! + "-"

       }else if str!.count > 14{

           return false
       }

       return true
   }
    
   //
   static func checkindianPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {

       if string == ""{

           return true

       }else if str!.count > 10{

           return false
       }

       return true
    }
}
