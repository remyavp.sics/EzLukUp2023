//
//  ProviderWelcomeVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 10/10/22.
//

import UIKit

class ProviderWelcomeVC: UIViewController {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var getstartedBtn: UIButton!
        
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getstartedBtn.addCornerForView(cornerRadius: 14.0)
    }
    
    override func viewDidLayoutSubviews() {
        self.viewBg.roundCorners(corners: [.topLeft,.topRight], radius: 14)
        self.roundView.roundCorners(corners: [.topLeft, .topRight, .bottomRight, .bottomLeft], radius: 65.0)
        
    }
    
    @IBAction func btnGetstartedAction(_ sender: UIButton) {
        
    }
    
}
