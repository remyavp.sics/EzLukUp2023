//
//  CustomTabbarVC.swift
//  EzLukUp
//
//  Created by Srishti on 14/11/22.
//

import UIKit


@available(iOS 14.0, *)
class CustomTabbarVC: UIViewController {
    var viewsub : ContactsmainVC!
    @IBOutlet weak var homeimg: UIImageView!
    @IBOutlet weak var searchimg: UIImageView!
    @IBOutlet weak var menuimg: UIImageView!
    @IBOutlet weak var menuView: UIView!
    @IBOutlet weak var menuViewblur: UIView!
    @IBOutlet weak var tabbarView: UIView!
    @IBOutlet weak var searchcontainer: UIView! //search page container
    @IBOutlet weak var homecontainer: UIView!// Dashboard page container
    @IBOutlet weak var ContactlistvcContainer: UIView!// Contactlist page container
    
    @IBOutlet weak var settingsContainer: UIView!// Settings page container
    @IBOutlet weak var faqContainer: UIView!
    @IBOutlet weak var feedbackToEzContainer: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView.isHidden = true
        menuViewblur.isHidden = true
        homecontainer.isHidden = false
        searchcontainer.isHidden = true
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "containerSegue" {
               let navController = segue.destination as! UINavigationController
               let secondVC = navController.viewControllers.first as! SearchVC
               navController.setViewControllers([secondVC], animated: false)
           }
    }
    
    
//    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//         if segue.identifier == "containerSegue" {
//                let navController = segue.destination as! UINavigationController
//                let secondVC = navController.viewControllers.first as! SearchVC
//                navController.setViewControllers([secondVC], animated: false)
//            }
//        }
    
    @IBAction func homeBTN(_ sender: UIButton) {
        homecontainer.isHidden = false
        searchcontainer.isHidden = true
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
        menuView.isHidden = true
        menuViewblur.isHidden = true
       
 NotificationCenter.default.post(name: Notification.Name("hometab"), object: nil)
    }
    
    @IBAction func searchBTN(_ sender: UIButton) {
        homecontainer.isHidden = true
        searchcontainer.isHidden = false
        ContactlistvcContainer.isHidden = true
        settingsContainer.isHidden = true
        feedbackToEzContainer.isHidden = true
        faqContainer.isHidden = true
        menuView.isHidden = true
        menuViewblur.isHidden = true
        if let navController = presentingViewController as? UINavigationController {
            navController.popToRootViewController(animated: true)
        }
        NotificationCenter.default.post(name: Notification.Name("searchtab"), object: nil)
    }
    
    @IBAction func menuBTN(_ sender: UIButton) {
        
        
        if menuView.isHidden == true{
            menuView.isHidden = false
            menuViewblur.isHidden = false
            tabbarView.isHidden = false
        }
        else{
            menuView.isHidden = true
            menuViewblur.isHidden = true
            tabbarView.isHidden = false
        }
//        Menucontainer.isHidden = true
    }
    
    @IBAction func menucloseBTN(_ sender: UIButton) {
        menuView.isHidden = true
        menuViewblur.isHidden = true
        tabbarView.isHidden = false
    }
    
    
    
    @available(iOS 14.0, *)
    @IBAction func mBTN(_ sender: UIButton) {
        if sender.tag == 0{
            print(sender.tag)
        }else if sender.tag == 1{
            print(sender.tag)
//            feedbackToEzContainer.isHidden = false
//            menuView.isHidden = true
//            menuViewblur.isHidden = true
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "FeedbackToEzVC") as! FeedbackToEzVC
            self.navigationController?.pushViewController(vc, animated: true)
        }else if sender.tag == 2{
            print(sender.tag)
            settingsContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
//            let storyboard = UIStoryboard(name: "Settings", bundle: nil)
//            let vc = storyboard.instantiateViewController(identifier: "SettingsVC") as! SettingsVC
//            self.navigationController?.pushViewController(vc, animated: true)

        }else if sender.tag == 3 {
            print(sender.tag)
        }else if sender.tag == 4 {
            print(sender.tag)
            faqContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
        }else if sender.tag == 5 {
            print(sender.tag)
            ContactlistvcContainer.isHidden = false
            menuView.isHidden = true
            menuViewblur.isHidden = true
           // vcinnavstack()        
        }
    }
    
    
    
    func vcinnavstack(){
        if let viewControllers = self.navigationController?.viewControllers
        {
            print("***",viewControllers)
            if viewControllers.contains(where: {
                return $0 is ContactsmainVC
            })
            {
                print("u r here")
            }
        }
    }
    
   
    
}
//dashboard codes







// pop back in viewcontroller
extension ViewController{
    func popBack(_ nb: Int) {
        if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
            guard viewControllers.count < nb else {
                self.navigationController?.popToViewController(viewControllers[viewControllers.count - nb], animated: true)
                return
            }
        }
    }
}

