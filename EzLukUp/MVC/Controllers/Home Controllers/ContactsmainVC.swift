//
//  ContactsmainVC.swift
//  EzLukUp
//
//  Created by Srishti on 15/11/22.
//

import UIKit
import Contacts
import Alamofire
import Algorithms
import CCBottomRefreshControl
import Firebase
import FirebaseDynamicLinks
import IPImage

var searchtexter = ""
var headcount = 0
var selectedBTN  = 0

@available(iOS 14.0, *)
class ContactsmainVC: UIViewController {
    //MARK: - VARIABLES AND CONSTANTS
    var offsetpg = 0
    var providerpaginationremainingcount = 0
     // if 0 selected is generalbtn , 1- providerbtn selected
    var selectedType = "Personal"
    var selectedsection = -1
    let store = CNContactStore()
    var contactlist : getcontactDataModel?
    var contactlistpg : getcontactDataModel?
    var remainingcount =  0
    var Allcontactlist : getcontactDataModel?
    var providerlist : providerResponseModel?
    var providerlistpg : providerResponseModel?
    let refreshControl = UIRefreshControl()
    var getCountry = String()
    var getState = String()
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    let imageArray: [UIImage?] = [UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon"),UIImage(named: "profile_icon")]
    let generaltipstatus:Bool = UserDefaults.standard.value(forKey: "generalTipStatus") as? Bool ?? false
    let providertipstatus:Bool = UserDefaults.standard.value(forKey: "providerTipStatus") as? Bool ?? false
    
    
    //MARK: - OUTLETS
    @IBOutlet weak var ButtonStack: UIStackView!
    @IBOutlet weak var generalBTNview:UIView!
    @IBOutlet weak var providerBTNview:UIView!
    @IBOutlet weak var generalLBL: UILabel!
    @IBOutlet weak var providerLBL: UILabel!
    @IBOutlet weak var GeneralTipviewHeight: NSLayoutConstraint!
    @IBOutlet weak var ProviderTipviewHeight: NSLayoutConstraint!
    @IBOutlet weak var filterview: UIView!
    @IBOutlet weak var contactsTBL: UITableView!
    @IBOutlet weak var GeneralTipview: BaseView!
    @IBOutlet weak var providerTipview: BaseView!
    @IBOutlet weak var nodataLBL: UILabel!
    
    
    @IBOutlet weak var FilteroneLBL: UILabel!
    @IBOutlet weak var FiltertwoLBL: UILabel!
    @IBOutlet weak var FilterthreeLBL: UILabel!
    @IBOutlet weak var FilterfourLBL: UILabel!
    @IBOutlet weak var inreviewView: UIView!
    @IBOutlet weak var FilterfiveLBL: UILabel!
    @IBOutlet weak var filterseperatorline: UIView!
    @IBOutlet weak var inreviewBTNout: UIButton!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var activityView: UIActivityIndicatorView!
    
    var contactid = ""
    var fkey = ""
    var totalcountfrom3sections = 0
    var isloadinglist = false
    var providerlistpaginationcount = 0
    var providerlistpagecount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        api(filterkey: "all")
        setupUI()
        if #available(iOS 15, *) {
            contactsTBL.sectionHeaderTopPadding = 0
        }
        refreshControl.triggerVerticalOffset = 100.0
        
        refreshControl.tintColor = UIColor.blue
        tabselector()
        //contactsTBL.bottomRefreshControl = refreshControl
        //       refreshControl.addTarget(self, action: #selector(reloadList), for: .valueChanged)
        print("selectedButton == \(selectedBTN)")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("selectedButton in viewllappear== \(selectedBTN)")
        tabselector()
    }
    func tabselector(){
        if selectedBTN == 0{
            offsetpg = 0
            self.contactlist = nil
            api(filterkey: "all")
            providerBTNview.backgroundColor = UIColor.clear
            generalBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) //= UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1).cgColor
            generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        }else{
            providerlist = nil
            providerlistpaginationcount = 0
            providerlistAPI(searchkey: "", filterkey: "all")
            generalBTNview.backgroundColor = UIColor.clear
            providerBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
            generalLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
            providerLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        }
    }
    
    @objc func reloadList(){
        isloadinglist = true
        if self.contactlistpg?.gettotalcount() ?? 0 > totalcountfrom3sections{
            if isloadinglist == true {
                offsetpg += 10
                self.paginationApicaller()
                
            }
        }
    }
    
    func paginationApicaller(){
        self.isloadinglist = false
        self.api(filterkey: "")
        self.refreshControl.endRefreshing()
    }
    
    
    //MARK: - FUNCTIONS
    func setupUI(){
        selectedBTN = 0
        contactsTBL.decelerationRate = UIScrollView.DecelerationRate.fast
        ButtonStack.layer.cornerRadius = ButtonStack.frame.height / 2
        providerBTNview.backgroundColor = UIColor.clear
        generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.inreviewView.isHidden = true
        self.filterseperatorline.isHidden = true
      
        //show only general tip view in selectedbtn = 0
    
        //tip view open if api value is true
        
        if selectedBTN == 0{
            if generaltipstatus{
                print("hide tip")
                GeneralTipview.isHidden = true
//                GeneralTipviewHeight.constant = 0
            }
            else{
                print("show tip")
                GeneralTipview.isHidden = false
//                GeneralTipviewHeight.constant = 80
            }
        }
        else{
            if providertipstatus{
                print("hide tip")
                providerTipview.isHidden = true
//                GeneralTipviewHeight.constant = 0
            }
            else{
                print("show tip")
                providerTipview.isHidden = false
//                GeneralTipviewHeight.constant = 80
            }
        }

        
        filterview.isHidden = true
        filterview.layer.cornerRadius = 13
        filterview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
    }
    
    func dotBTNcalled(){
        //MARK: - UImenu for 3dot button
        
    }
    //MARK: - BUTTON ACTIONS
    //filterbutton actions
    @IBAction func FilterBTNone(_ sender: UIButton) {
        fkey = "all"
        self.contactlist = nil
        self.searchTF.text = ""
        self.contactsTBL.reloadData()
        if selectedBTN == 0 {
            
            offsetpg = 0
            api(filterkey: "all")
            //            { error in
            //                self.contactsTBL.reloadData()
            //            }
        }
        else{
            
//            api(filterkey: "all")
            providerlistAPI(searchkey: "", filterkey: "all")
        }
        filterview.isHidden = true
    }
    @IBAction func FilterBTNtwo(_ sender: UIButton) {
        
        self.contactlist = nil
        self.searchTF.text = ""
        self.contactsTBL.reloadData()
        if selectedBTN == 0 {
            fkey = "invite"
            offsetpg = 0
            api(filterkey: "invite")
            //            { error in
            //                self.contactsTBL.reloadData()
            //            }
        }
        else{
            fkey = "recomendbyme"
            providerlistAPI(searchkey: "", filterkey: "recomendbyme")
            //            { error in
            //                self.contactsTBL.reloadData()
            //            }
        }
        filterview.isHidden = true
    }
    @IBAction func FilterBTNthree(_ sender: UIButton) {
        
        self.contactlist = nil
        self.searchTF.text = ""
        self.contactsTBL.reloadData()
        if selectedBTN == 0 {
            fkey = "invited"
            offsetpg = 0
            api(filterkey: "invited")
            //            { error in
            //                self.contactsTBL.reloadData()
            //            }
        }
        else{
            fkey = "recomendbyothers"
            providerlistAPI(searchkey: "", filterkey: "recomendbyothers")
            //            { error in
            //                self.contactsTBL.reloadData()
            //            }
        }
        filterview.isHidden = true
    }
    @IBAction func FilterBTNfour(_ sender: UIButton) {
        
        self.contactlist = nil
        self.searchTF.text = ""
        self.contactsTBL.reloadData()
        if selectedBTN == 0 {
            fkey = "inez"
            offsetpg = 0
            api(filterkey: "inez")
            
        }
        else{
            fkey = "connected"
            providerlistAPI(searchkey: "", filterkey: "connected")
            
        }
        filterview.isHidden = true
    }
    
    @IBAction func FilterBTNfive(_ sender: UIButton) {
        if selectedBTN == 1{
            fkey = "connected"
            providerlistAPI(searchkey: "", filterkey: "needsreview")
        }
    }
    
    @IBAction func AddBTNtapped(_ sender: UIButton) {
        if selectedBTN == 0 {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddGeneralContactVC") as! AddGeneralContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddProviderContactVC") as! AddProviderContactVC
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func FilterBTNtapped(_ sender: UIButton) {
        if filterview.isHidden{
            filterview.isHidden = false
        }
        else{
            filterview.isHidden = true
        }
        if selectedBTN == 0 {
            FilteroneLBL.text = "All"
            FiltertwoLBL.text = "Invite"
            FilterthreeLBL.text = "invited"
            FilterfourLBL.text = "In ez"
            selectedType = "Personal"
        }
        else{
            FilteroneLBL.text = "All"
            FiltertwoLBL.text = "In ez - Recommended by Me"
            FilterthreeLBL.text = "In ez - Recommended by Others"
            FilterfourLBL.text = "In ez - Just Connected"
            FilterfiveLBL.text = "Needs review"
            selectedType = "Business"
        }
        
    }
    
    @IBAction func GeneralBTNtapped(_ sender: UIButton) {
        //tip condtion chcking
        if generaltipstatus{
            providerTipview.isHidden = true
        }
        else{
            GeneralTipview.isHidden = false
        }
       selectedBTN = 0
        selectedType = "Personal"
        offsetpg = 0
        self.contactlist = nil
        self.providerlist = nil
        api(filterkey: "all")
        providerBTNview.backgroundColor = UIColor.clear
        generalBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) //= UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1).cgColor
        generalLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        providerLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.inreviewView.isHidden = true
        self.filterseperatorline.isHidden = true
    }
    
    @IBAction func ProviderBTNtapped(_ sender: UIButton) {
        if providertipstatus{
            providerTipview.isHidden = true
        }
        else{
            GeneralTipview.isHidden = false
        }
        selectedBTN = 1
        providerlistpaginationcount = 0
        selectedType = "Business"
        fkey = "all"
        self.providerlist = nil
        providerlistAPI(searchkey: "", filterkey: "all")
        generalBTNview.backgroundColor = UIColor.clear
        providerBTNview.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        generalLBL.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        providerLBL.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.inreviewView.isHidden = false
        self.filterseperatorline.isHidden = false
    }
    @IBAction func searchBTNTapped(_sender : UIButton){
      
        
        
    }
    //MARK: - Tip Close Button functions
    @IBAction func TipcloseBTNtapped(_ sender: UIButton) {
        print("tag = \(sender.tag)")
        if selectedBTN == 0{
                    GeneralTipview.isHidden = true
                    UserDefaults.standard.set(true, forKey: "generalTipStatus")
                    tipcloseAPI(keyvalue: "tooltip.contactswipe")
        }else{

            providerTipview.isHidden = true
            UserDefaults.standard.set(true, forKey: "providerTipStatus")
            tipcloseAPI(keyvalue: "tooltip.contactswipeprovider")
        }
    }
    
    @IBAction func HeaderDropBTNtapped(_ sender: UIButton) {
        print("section :",sender.tag)
        selectedsection = sender.tag
        
        if selectedBTN == 0 {
            if sender.tag == 0 {
                self.contactlist?.taggedContact[0].sectionOpened = !(self.contactlist?.taggedContact[0].sectionOpened)!
            }
            else if sender.tag == 1 {
                self.contactlist?.possibleProviders[0].sectionOpened = !(self.contactlist?.possibleProviders[0].sectionOpened)!
            }else {
                self.contactlist?.normalList[sender.tag - 2].sectionOpened = !(self.contactlist?.normalList[sender.tag - 2].sectionOpened ?? true)
                
            }
            
        }else{
            self.providerlist?.Data[sender.tag].sectionOpened = !(self.providerlist?.Data[sender.tag].sectionOpened ?? true)
            
        }
        contactsTBL.reloadData()
        
    }
    
    @IBAction func dotBTNtapped(_ sender: UIButton) {
        
        print("indexpath :",sender.tag)
        
    }
    
    @IBAction func inviteBTNtapped(_ sender: UIButton) {
        
        print("indexpath :",sender.tag)
        print("")
        shareShow()
        
    }
    
    @IBAction func providerDotsBTNTapped(_ sender: UIButton) {
        print("indexpath :",sender.tag)
    }
    
}

//MARK: - EXtension (Tableview)
@available(iOS 14.0, *)
extension ContactsmainVC:UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if selectedBTN == 0{
            return (self.contactlist?.normalList.count ?? 0) + 2
        }else{
            return (self.providerlist?.Data.count ?? 0)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if selectedBTN == 0{
            if section == 0 {
                return self.contactlist?.taggedContact.count ?? 0
            }
            else if section == 1 {
                return self.contactlist?.possibleProviders.count ?? 0
            }else {
                return self.contactlist?.normalList[section - 2].contactsList.count ?? 0
            }
        }else{
            return self.providerlist?.Data[section].providercontacts.count ?? 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "headercell") as! headercell
        cell.dropBTN.tag = section
        
        if selectedBTN == 0{
            cell.nameLBL.text = section == 0 ? "Tagged by other users as Provider": (section == 1 ? "Identified by ezlukup as Possible Provider" : self.contactlist?.normalList[section - 2].Headername?.capitalized)
        }else{
            cell.nameLBL.text = self.providerlist?.Data[section].name
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! contactCell
        
        if selectedBTN == 0{
            if let contactList = self.contactlist{
                if indexPath.section == 0 {
                    //tagged user name
                    cell.nameLBL.text = contactList.taggedContact[indexPath.row].name ??  contactList.taggedContact[indexPath.row].twilioCallerName
                    //tagged user image
                    DispatchQueue.main.async {
                        let ipimage = IPImage(text: cell.nameLBL.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (contactList.taggedContact[indexPath.row].contactImage ?? "")),placeholder: ipimage.generateImage())
                    }
                    
                    //recomended thumb count
                    DispatchQueue.main.async {
                        if let count = contactList.taggedContact[indexPath.row].recommendedCount as? String{
                            
                            if count != "0"{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = count
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        else {
                            let count = contactList.taggedContact[indexPath.row].recommendedCount as? Int
                            if count != 0{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = "\(count ?? 0)"
                                
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        
                    }
                    //category id for tagged user
                    if contactList.taggedContact[indexPath.row].isUser ?? false {
                        print("user true")
                    }
                    else{
                        print("user false")
                        let taggedarraycount = contactList.taggedContact[indexPath.row].taggedCategoryIds.count
                        print("taggedcount ==",taggedarraycount)
                        let ddc = Dictionary(grouping: contactList.taggedContact[indexPath.row].taggedCategoryIds, by: { $0.name })
                        let ppc = ddc.max(by: {$0.value.count < $1.value.count})
                        print("******************",ppc ?? "")
                        cell.categorynameLBL.text = ppc?.key ?? ""
                    }
                   
//                    cell.categorynameLBL.text = contactList.taggedContact[indexPath.row].taggedCategoryIds.first?.name
                    cell.statusBTNtapped.tag = indexPath.row
                    //inez , invite status
                    // notin us
                    //contactUserId != null   ,    isUser = false,isReferred = true - invite
                    if contactList.taggedContact[indexPath.row].notInUs == true {
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusnameLBL.text = "Not in USA"
//                        cell.dotimg.isHidden = true
                        cell.dotBTN.isHidden = true
                        cell.statusView.backgroundColor = hexStringToUIColor(hex: "#828685")
                    }else if   contactList.taggedContact[indexPath.row].isUser == false &&  contactList.taggedContact[indexPath.row].isReferred == true{
                        cell.statusnameLBL.text = "invited"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#FBBC05")
                    }
                    else if  contactList.taggedContact[indexPath.row].isUser == false &&  contactList.taggedContact[indexPath.row].isReferred == false
                    {
                        cell.statusnameLBL.text = "invite"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = true
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#4285F4")
                        
                    }else if contactList.taggedContact[indexPath.row].contactUserId != nil && contactList.taggedContact[indexPath.row].isUser == true {
                        cell.statusnameLBL.text = "in ez"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#33CB98")
                    }
                    
                    
                    //
                }else if indexPath.section == 1 {
                    cell.categorynameLBL.text = ""
                    cell.nameLBL.text = contactList.possibleProviders[indexPath.row].name ??  contactList.possibleProviders[indexPath.row].twilioCallerName
                    // possible contact img
                    DispatchQueue.main.async {
                        let ipimage = IPImage(text: cell.nameLBL.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (contactList.possibleProviders[indexPath.row].contactImage ?? "")),placeholder: ipimage.generateImage())
                    }
//                    cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (contactList.possibleProviders[indexPath.row].contactImage ?? "")),placeholder: UIImage(named: "avatar"))
                    // recomended thumb
                    
                    
                    DispatchQueue.main.async {
                        if let count = contactList.possibleProviders[indexPath.row].recommendedCount as? String{
                            
                            if count != "0"{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = count
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        else {
                            let count = contactList.possibleProviders[indexPath.row].recommendedCount as? Int
                            if count != 0{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = "\(count ?? 0)"
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        
                    }
                    
                    //inez , invite status
                    if contactList.possibleProviders[indexPath.row].notInUs == true {
                        cell.statusnameLBL.text = "Not in USA"
//                        cell.dotimg.isHidden = true
                        cell.dotBTN.isHidden = true
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex: "#828685")
                    }else if  contactList.possibleProviders[indexPath.row].isUser == false &&  contactList.possibleProviders[indexPath.row].isReferred == true{
                        cell.statusnameLBL.text = "invited"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#FBBC05")
                    }
                    else if  contactList.possibleProviders[indexPath.row].isUser == false &&  contactList.possibleProviders[indexPath.row].isReferred == false
                    {
                        cell.statusnameLBL.text = "invite"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = true
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#4285F4")
                        
                    }else if contactList.possibleProviders[indexPath.row].contactUserId != nil && contactList.possibleProviders[indexPath.row].isUser == true {
                        cell.statusnameLBL.text = "in ez"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#33CB98")
                    }
                    //
                }else {
                    var cityl = ""
                    var statel = ""
                    cell.categorynameLBL.text = ""
                    cell.nameLBL.text = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].name ??  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].twilioCallerName
                    let city = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].city ?? ""
                    let state = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].stateShortCode ?? ""
                    if city != ""{
                        cityl = city
                    }
                    if state != "" {
                        statel = " , \(state)"
                    }
                    cell.categorynameLBL.text = "\(cityl)\(statel)"
                    
                    
                    
                    //nomrmal list ccontact img
                    DispatchQueue.main.async {
                        let ipimage = IPImage(text: cell.nameLBL.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].contactImage ?? "")),placeholder: ipimage.generateImage())
                    }
                    // recomended thumb
                    
                    DispatchQueue.main.async {
                        if let count = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].recommendedCount as? String{
                            
                            if count != "0"{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = count
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        else {
                            let count = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].recommendedCount as? Int
                            if count != 0{
                                cell.generalthumbIMG.isHidden = false
                                cell.generalthumbcount.text = "\(count ?? 0)"
                            }
                            else{
                                cell.generalthumbIMG.isHidden = true
                                cell.generalthumbcount.text = ""
                            }
                        }
                        
                    }
                    
                    //inez , invite status
                    if contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].notInUs == true {
                        cell.statusnameLBL.text = "Not in USA"
                        cell.statusBTNtapped.isUserInteractionEnabled = false
//                        cell.dotimg.isHidden = true
                        cell.dotBTN.isHidden = true
                        cell.statusView.backgroundColor = hexStringToUIColor(hex: "#828685")
                    }else if  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].isUser == false &&  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].isReferred == true{
                        cell.statusnameLBL.text = "invited"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#FBBC05")
                    }
                    else if  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].isUser == false &&  contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].isReferred == false
                    {
                        cell.statusnameLBL.text = "invite"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = true
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#4285F4")
                        
                    }else if contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].contactUserId != nil && contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].isUser == true {
                        cell.statusnameLBL.text = "in ez"
                        cell.dotimg.isHidden = false
                        cell.dotBTN.isHidden = false
                        cell.statusBTNtapped.isUserInteractionEnabled = false
                        cell.statusView.backgroundColor = hexStringToUIColor(hex:"#33CB98")
                        //city label for in ez A-Z list
                        cell.categorynameLBL.text = contactList.normalList[indexPath.section - 2].contactsList[indexPath.row].city
                    }
                    //
                }
            }
        }else{
            //Provider contact name
            var cityl = ""
            var statel = ""
            cell.providerNameLbl.text = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].name ??
            providerlist?.Data[indexPath.row].providercontacts[indexPath.row].twilioCallerName
            
            let city = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].city ?? ""
            let state = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].stateShortCode ?? ""
            if city != "" {
                cityl = city
            }
            if state != "" {
                statel = " , \(state)"
            }
            cell.proLocationLbl.text = "\(cityl)\(statel)"
            //provider contact image
            DispatchQueue.main.async {
                let ipimage = IPImage(text: cell.providerNameLbl.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                cell.providerProfileIMG.kf.setImage(with: URL(string: kImageUrl + (self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].contactImage ?? "")),placeholder: ipimage.generateImage())
            }
            //recomended like
            if self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].isRecommended == true{
                cell.providerthumbIMG.isHidden = false
            }
            else{
                cell.providerthumbIMG.isHidden = true
            }
            //head section
            let rec = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].recomended.count
            headcount = rec ?? 0
            print("headcount",rec)
            //
            cell.recommendedCVwidth.constant = CGFloat(20 * headcount)
            if headcount > 0 {
                cell.countLabel.text = "\(headcount)"
                cell.recommendedCV.reloadData()
            }
            else {
                cell.countLabel.text = ""
            }
            
        }
        
        // general,provider tab view show and hide
        if selectedBTN == 0 {
            cell.generalView.isHidden = false
            cell.ProviderView.isHidden = true
        }else{
            cell.ProviderView.isHidden = false
            cell.generalView.isHidden = true
        }
       
        
        cell.dotBTN.tag = indexPath.row
//        cell.
        cell.dotBTN.showsMenuAsPrimaryAction = true
        
        
        cell.providerDotsBTN.tag = indexPath.row
        cell.providerDotsBTN.showsMenuAsPrimaryAction = true
        //MARK: - popup menu
        if selectedBTN == 0 {
            //MARK: - Call
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                // Perform action
                var getmobile = ""
                if indexPath.section == 0 {
                    print("call fromtagged")
                    getmobile = self.contactlist?.taggedContact[indexPath.row].phoneNumber ?? ""
                }else if indexPath.section == 1{
                    print("call possible provider")
                    getmobile = self.contactlist?.possibleProviders[indexPath.row].phoneNumber ?? ""
                }
                else{
                    print("call normal list")
                    getmobile = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].phoneNumber ?? ""
                }
                
                let url:NSURL = NSURL(string: "telprompt:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                print("calling")
            }
            //MARK: - Text
            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                var getmobile = ""
                if indexPath.section == 0 {
                    print("call fromtagged")
                    getmobile = self.contactlist?.taggedContact[indexPath.row].phoneNumber ?? ""
                }else if indexPath.section == 1{
                    print("call possible provider")
                    getmobile = self.contactlist?.possibleProviders[indexPath.row].phoneNumber ?? ""
                }
                else{
                    print("call normal list")
                    getmobile = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].phoneNumber ?? ""
                }
                
                var url:NSURL = NSURL(string: "sms:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                print("texting")
            }
            
            //MARK: - Share
            let Share = UIAction(title: "Share",
                                 image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                if indexPath.section == 0 {
                    print("share fromtagged")
                    let getname = self.contactlist?.taggedContact[indexPath.row].name ?? ""
                    let getmobile = self.contactlist?.taggedContact[indexPath.row].phoneNumber ?? ""
                    
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }else if indexPath.section == 1{
                    print("share possible provider")
                    let getname = self.contactlist?.possibleProviders[indexPath.row].name ?? ""
                    let getmobile = self.contactlist?.possibleProviders[indexPath.row].phoneNumber ?? ""
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }
                else{
                    print("share normal list")
                    let getmobile = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].phoneNumber ?? ""
                    let getname = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].name ?? ""
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
                }
                func createContact(fname:String,lname:String,mob:String) -> CNContact {
                    // Creating a mutable object to add to the contact
                    let contact = CNMutableContact()
                    contact.imageData = NSData() as Data // The profile picture as a NSData object
                    contact.givenName = fname
                    contact.familyName = lname
                    contact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:mob))]
                    
                    return contact
                }
                // text to share
                let text = "This is some text that I want to share."
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                //                self.present(activityViewController, animated: true, completion: nil)
                print("sharing")
            }
            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "EditGeneralContactVC") as! EditGeneralContactVC
                if indexPath.section == 0{
                    vc.getcontactid = self.contactlist?.taggedContact[indexPath.row].id ?? ""
                    vc.getname = self.contactlist?.taggedContact[indexPath.row].name ?? ""
                    vc.getmobile = self.contactlist?.taggedContact[indexPath.row].phoneNumber ?? ""
                    vc.getcountry = self.contactlist?.taggedContact[indexPath.row].countryCode ?? ""
                }else if indexPath.section == 1{
                    vc.getcontactid = self.contactlist?.possibleProviders[indexPath.row].id ?? ""
                    vc.getname = self.contactlist?.possibleProviders[indexPath.row].name ?? ""
                    vc.getmobile = self.contactlist?.possibleProviders[indexPath.row].phoneNumber ?? ""
                    vc.getcountry = self.contactlist?.possibleProviders[indexPath.row].countryCode ?? ""
                }else{
                    vc.getcontactid = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id ?? ""
                    vc.getmobile = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].phoneNumber ?? ""
                    vc.getname = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].name ?? ""
                    vc.getcountry = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].countryCode ?? ""
                }
                
                self.navigationController?.pushViewController(vc, animated: true)
                print("opening")
            }
            
            cell.dotBTN.menu = UIMenu(title:"", children: [Call, Text, Share, Edit])
        }else{
            //MARK: - menu functions for provider tab
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                // Perform action
                let number = "" ?? ""
                var url:NSURL = NSURL(string: "telprompt:\(number)")!
                UIApplication.shared.openURL(url as URL)
                print("calling")
            }
            
            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                let number = 5555555555
                var url:NSURL = NSURL(string: "sms:\(number)")!
                UIApplication.shared.openURL(url as URL)
                print("texting")
            }
            
            
            let Share = UIAction(title: "Share",
                                 image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                func createContact() -> CNContact {
                    
                    // Creating a mutable object to add to the contact
                    let contact = CNMutableContact()
                    
                    contact.imageData = NSData() as Data // The profile picture as a NSData object
                    contact.givenName = "John"
                    contact.familyName = "Appleseed"
                    contact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:"(408) 555-0126"))]
                    
                    return contact
                }
                // text to share
                let text = "This is some text that I want to share."
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                self.present(activityViewController, animated: true, completion: nil)
                print("sharing")
            }
            
            let Feedback = UIAction(title: "Feedback",
                                    image: UIImage(named: "Feedback")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
                vc.getcontactid = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                print("typing")
            }
            
            let Report = UIAction(title: "Report",
                                  image: UIImage(named: "Report")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
                vc.getcontactid = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                print("reporting")
            }
            
            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "EditProviderVC") as! EditProviderVC
                vc.getcontactid = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id ?? ""
                vc.getmobile = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].phoneNumber ?? ""
                
                vc.getname = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].name ?? ""
                vc.getcountry = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].countryCode ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                print("opening")
            }
            
            cell.providerDotsBTN.menu = UIMenu(title:"", children: [Call, Text, Share, Feedback, Report, Edit])
        }
        
        return cell
    }
    //MARK: - image with name
    func imageWith(name: String?) -> UIImage? {
            let frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            let nameLabel = UILabel(frame: frame)
            nameLabel.textAlignment = .center
            nameLabel.backgroundColor = .lightGray
            nameLabel.textColor = .white
            nameLabel.font = UIFont.boldSystemFont(ofSize: 20)
            var initials = ""
            if let initialsArray = name?.components(separatedBy: " ") {
                if let firstWord = initialsArray.first {
                    if let firstLetter = firstWord.first {
                        initials += String(firstLetter).capitalized }
                }
                if initialsArray.count > 1, let lastWord = initialsArray.last {
                    if let lastLetter = lastWord.first { initials += String(lastLetter).capitalized
                    }
                }
            } else {
                return nil
            }
            nameLabel.text = initials
            UIGraphicsBeginImageContext(frame.size)
            if let currentContext = UIGraphicsGetCurrentContext() {
                nameLabel.layer.render(in: currentContext)
                let nameImage = UIGraphicsGetImageFromCurrentImageContext()
                return nameImage
            }
            return nil
        }
    //--------------------------------------------------------------------------------------------------------------------------------------------------
    
    // provider Swipe actions
    func tableView(_ tableView: UITableView,
                   leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let action = UIContextualAction(style: .normal,
                                        title: "Provider") { [weak self] (action, view, completionHandler) in
            self?.handleLeftaction()
            completionHandler(true)
            
            let contactList = self?.contactlist
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "AddCategoryVC") as! AddCategoryVC
            
            if indexPath.section == 0{
                vc.getcontactid = self?.contactlist?.taggedContact[indexPath.row].id ?? ""
                vc.getProfileName = contactList?.taggedContact[indexPath.row].name ?? ""
                 self?.navigationController?.pushViewController(vc, animated: true)
            }
            else if indexPath.section == 1{
                vc.getcontactid = self?.contactlist?.possibleProviders[indexPath.row].id ?? ""
                vc.getProfileName = contactList?.possibleProviders[indexPath.row].name ?? ""
                self?.navigationController?.pushViewController(vc, animated: true)
            }
            else{
                vc.getcontactid = self?.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id ?? ""
                vc.getProfileName = contactList?.normalList[indexPath.section - 2].contactsList[indexPath.row].name ?? ""
                 self?.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        action.backgroundColor = UIColor(red: 0.2, green: 0.8, blue: 0.6, alpha: 0.2)
        if selectedBTN == 0{
            if indexPath.section == 0{
                if self.contactlist?.taggedContact[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            else if indexPath.section == 1 {
                if self.contactlist?.possibleProviders[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            else{
                if self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].notInUs == true {
                    print("No swipe needed")
                }
                else{
                    return UISwipeActionsConfiguration(actions: [action])
                }
            }
            
            
        }
        return UISwipeActionsConfiguration()
    }
    
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if selectedBTN == 0{
            if indexPath.section == 0 || indexPath.section == 1 {
                
                if indexPath.section == 0{
                    if self.contactlist?.taggedContact[indexPath.row].notInUs == true {
                        print("No swipe needed")
                    }
                    else{
                        let rightaction = UIContextualAction(style: .normal,
                                                             title: "General") {(action, view, completionHandler) in
                            
                            print("call api for swipe")
                            let getcontactid = self.contactlist?.taggedContact[indexPath.row].id ?? ""
                            self.taggeneralapi(contactidfromlist: getcontactid)
                            completionHandler(true)
                        }
                        rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
                        return UISwipeActionsConfiguration(actions: [rightaction])
                    }
                    
                }
                else if indexPath.section == 1{
                    if self.contactlist?.possibleProviders[indexPath.row].notInUs == true {
                        print("No swipe needed")
                    }
                    else{
                        let rightaction = UIContextualAction(style: .normal,
                                                             title: "General") {(action, view, completionHandler) in
                            print("call api for swipe")
                            let getcontactid = self.contactlist?.possibleProviders[indexPath.row].id ?? ""
                            self.taggeneralapi(contactidfromlist: getcontactid)
                            completionHandler(true)
                        }
                        rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
                        return UISwipeActionsConfiguration(actions: [rightaction])
                    }
                    
                }
            }
        }
        else {
            let rightaction = UIContextualAction(style: .normal,
                                                 title: "General") { [weak self] (action, view, completionHandler) in
                let getcontactid = self?.providerlist?.Data[indexPath.row].providercontacts[indexPath.row].id ?? ""
                self?.changeproviderapi(contactidfromlist: getcontactid)
//                self?.handleRightaction()
                
                completionHandler(true)
                
            }
            
            rightaction.backgroundColor = UIColor(red: 0.984, green: 0.737, blue: 0.02, alpha: 0.5)
            return UISwipeActionsConfiguration(actions: [rightaction])
        }
        
        return UISwipeActionsConfiguration()
    }
    
    
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    //  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    //       return true
    //   }
    
    
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        //       return self.contactlist[section].contactsList.count == 0 ? 0 : 22
        
        if selectedBTN == 0 {
            if  section == 0 {
                return self.contactlist?.taggedContact.count ?? 0 != 0 ? 40 : 0
            }else if section == 1{
                return self.contactlist?.possibleProviders.count ?? 0 != 0 ? 40 : 0
            }
            else{
                return self.contactlist?.normalList[section - 2].contactsList.count ?? 0 != 0 ? 40 : 0
            }
        }else{
            return self.providerlist?.Data.count != 0 ? 40 : 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if selectedBTN == 0 {
            
            if  indexPath.section == 0 {
                return self.contactlist?.taggedContact.count == 0 ? 0 : (self.contactlist?.taggedContact[0].sectionOpened ?? false ? UITableView.automaticDimension : 0)
            }else if indexPath.section == 1{
                return self.contactlist?.possibleProviders.count == 0 ? 0 :  (self.contactlist?.possibleProviders[0].sectionOpened ?? false ? UITableView.automaticDimension : 0)
            }
            else{
                return self.contactlist?.normalList[indexPath.section - 2].contactsList.count == 0 ? 0 : (self.contactlist?.normalList[indexPath.section - 2].sectionOpened ?? true ? UITableView.automaticDimension : 0)
            }
        }else{
            return self.providerlist?.Data.count == 0 ? 0 : (self.providerlist?.Data[indexPath.section].sectionOpened ?? true)  ? UITableView.automaticDimension : 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = contactsTBL.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! contactCell
        
        if selectedBTN == 0{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "GeneralContactProfileVC") as! GeneralContactProfileVC
            if indexPath.section == 0 {
                UserDefaults.standard.setValue(self.contactlist?.taggedContact[indexPath.row].id, forKey: "Kcontactid")
                print(contactlist?.taggedContact[indexPath.row].id)
                vc.getname = self.contactlist?.taggedContact[indexPath.row].name ?? ""
                vc.selecteduserid = self.contactlist?.taggedContact[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            }else if indexPath.section == 1 {
                UserDefaults.standard.setValue(self.contactlist?.possibleProviders[indexPath.row].id, forKey: "Kcontactid")
                print(contactlist?.possibleProviders[indexPath.row].id)
                vc.getname = self.contactlist?.possibleProviders[indexPath.row].name ?? ""
                vc.selecteduserid = self.contactlist?.possibleProviders[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
            }else {
                UserDefaults.standard.setValue(self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id, forKey: "Kcontactid")
                print(contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id)
                vc.getname = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].name ?? ""
                vc.selecteduserid = self.contactlist?.normalList[indexPath.section - 2].contactsList[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                
            }
        }
        
        
        else {
            print("provider")
            print(self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].name ?? "")
            print(self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id ?? "")
            
            UserDefaults.standard.setValue(self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id, forKey: "Kcontactid")
            
            cell.ProviderView.isExclusiveTouch = true
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
            vc.selecteduserid = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].id ?? ""
            vc.getname = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].name ?? ""
            checkcollectionstatus = "fromlist"
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if selectedBTN == 1{ // only for provider tab
            let lastItem = (self.providerlist?.Data[indexPath.section].providercontacts.count ?? 0) - 1
                if indexPath.row == lastItem {
                    print("IndexRow\(indexPath.row)")
                    if self.providerlistpaginationcount <= providerlistpagecount && self.contactsTBL.isScrollEnabled{
                        self.contactsTBL.isScrollEnabled = false
                        providerlistpaginationcount += 1
                        self.providerlistAPI(searchkey: "", filterkey: "all")
                    }
                    else{
                        self.contactsTBL.isScrollEnabled = true
                        return
                    }
                }
        }

    }
    
    //MARK: - share contact card from 3dot menu
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
    //MARK: - SHARE INVTIE LINK WITH TYPE
    func shareShow(inviteto:String = "",type:String = "") {
       var shareText = "Hello, world!"
        var sh = ""
        var ln = ""
        //        let linkToShare = "https://mashoproduct.page.link/?pdtid=\(self.productID)&link=\(self.ProductDetailsData.productdetails.webshare!)"
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "eizlukup.page.link"
        components.path = "/invite"
        
        let queryItem1 = URLQueryItem(name: "invitername", value: "ashik")
        let queryItem2 = URLQueryItem(name: "type", value: "personal")
        components.queryItems = [queryItem1,queryItem2]
        
        guard let linkParameter = components.url else {return}
        print("sharing Link :\(linkParameter.absoluteString)")
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://eizlukup.page.link") else { return }
        // IOS PARAMETERS
        if let bundleID = Bundle.main.bundleIdentifier {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        }
        shareLink.iOSParameters?.appStoreID = "1663135116"
        // Android PARAMETERS
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.ezlukup")
        // Config MetaData
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters ()
//        shareLink.socialMetaTagParameters?.title = ""
        //        shareLink.socialMetaTagParameters?.descriptionText = self.ProductDetailsData.productdetails.product_description
//        if let imageString = "" , let imageURL = URL(string: imageString) {
//            shareLink.socialMetaTagParameters?.imageURL = imageURL
//        }
        
        guard let longURL = shareLink.url else { return }
        print("The long dynamcLink is :\(longURL)")
        ln = "\(longURL)"
        
        shareLink.shorten { (url, warnings, error) in
            if let error = error {
                print("Oh no! got an error :\(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings {
                for warning in warnings {
                    print("FDL warning :\(warning)")
                }
            }
            
            guard let url = url else { return }
            print("Short url :\(url.absoluteString)")
            sh = "\(url.absoluteString)"
            let vc = UIActivityViewController(activityItems: ["Invite \("") to ezlukup",url], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
        shareText = "\(sh)"
        print("long URL = \(ln)")
        print("short URL = \(sh)")
        

//            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
//             present(vc, animated: true, completion: nil)
//
    }
    
    
    
    //MARK: - Did scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if selectedBTN == 0 {
            if (fkey == "all" || fkey == "") && searchtexter == "" {
                let pos = contactsTBL.contentOffset.y
                if pos > contactsTBL.contentSize.height-100 - scrollView.frame.size.height{
                    guard !isloadinglist else{
                        return
                    }
                    let totalcount = self.contactlistpg?.gettotalcount() ?? 0
                    print("total count showing when pagination = \(totalcount)")
                    print("offset value before increment \(offsetpg)")
                    if totalcount > offsetpg{
                        print("call api")
                        offsetpg += 100
                        self.api()
                        print("offset value after apicall \(offsetpg)")
                    }
                    else{
                        return
                    }
                    
                    
                }
            }
        }
        else{
            let pos = contactsTBL.contentOffset.y
            if pos > contactsTBL.contentSize.height-100 - scrollView.frame.size.height{
                guard !isloadinglist else{
                    return
                }
                
                let totalcount = self.providerlist?.Data.count ?? 0
                print("total count showing when pagination = \(totalcount)")
                
                if self.providerlistpaginationcount <= providerlistpagecount && self.contactsTBL.isScrollEnabled{
                    self.contactsTBL.isScrollEnabled = false
                    providerlistpaginationcount += 1
                    self.providerlistAPI(searchkey: "", filterkey: "all")
                }
                else{
                    self.contactsTBL.isScrollEnabled = true
                    return
                }
            }
        }
        
        
    }
    
    //MARK: - Handlers
    private func handleLeftaction() {
        print("tag provider swipe success")
        
    }
    private func handleRightaction() {
        print(" general swipe success")
        
        
    }
    //MARK: - API FUNCTIONS-----------------------------------------------------------------------------------------------------------------------------
    //MARK: - OLD LISTING API FUNCTION WITH GENERAL TAB AND PROVIDER TAB
    func api(key:String = "",type:String = "",filterkey:String = "" /*, completion: @escaping(Error?) -> ()*/){
        showActivityIndicator()
        self.isloadinglist = false
        self.nodataLBL.isHidden = true
        contactsTBL.isScrollEnabled = false
        var endapi = ""
        var filter = ""
        var limit = 0
        print("dynamic userid = \(userid)")
        print("dynamic tocken = \(token)")
        // arya test userid = "63d75ac32e4db50d3a6e086d"
        // raji 63db53e00fca2c6d036dda86
        //Client "63e318d749e58565630abcdd"
        
        if selectedBTN == 0 {
            if filterkey == "" || filterkey == "all"{
                if key == ""{
                    limit = 100
                    print("pagination needed")
                }
                else{
                    limit = 0
                    print("pagination not needed")
                }
            }
            else{
                limit = 0
                print("limit = 0 , no pagination")
            }
        }else{
            limit = 0
            print("limit = 0 , no pagination")
        }
        var params : [String:Any] = [:]
        
        if selectedBTN == 0 {
            endapi = "getContactsSingleApi"
            filter = filterkey
            params = [
                                      "userId":userid ,
//                "userId":"63db53e00fca2c6d036dda86",
                "searchKey":key,
                "limit":limit,
                "offset" : offsetpg,
                "filter":filter,
            ] as [String : Any]

        let url = "http://13.234.177.61/api7/\(endapi)"
        print("parameters =",params ?? [] ,"url = ",url)
        
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
                
                switch (response.result) {
                case .success( let JSON):
                    if let responsedata =  JSON as? [String:Any]  {
                        //                        print("response data = \(responsedata)")
                        let contact = getcontactDataModel(from:responsedata)
                        self.contactlistpg = contact
                        var normalList :[getcontactDataListModel] = []
                        let contactx = (self.contactlistpg?.apinormalList.chunked(on: \.name?.first) ?? []).map({ (String($0.0!),Array($0.1)) }).sorted(by: {$0.0.compare($1.0, options: .caseInsensitive) == .orderedAscending })
                      
                        for contact in contactx {
                            normalList.append(getcontactDataListModel(headername: contact.0, contacts: contact.1))
                        }
                        self.contactlistpg?.normalList = normalList
                        if key != "" || filterkey != "" {
                            self.contactlist = getcontactDataModel(normalList: self.contactlistpg?.normalList ?? [], taggedContact: self.contactlistpg?.taggedContact ?? [], possibleProviders: self.contactlistpg?.possibleProviders ?? [])
                        } else {
                            if let taggedcontactpg = self.contactlist?.taggedContact, let possibleproviderpg = self.contactlist?.possibleProviders , let normallistpg = self.contactlist?.normalList{
                                self.contactlist?.taggedContact.append(contentsOf:self.contactlistpg?.taggedContact ?? [] )
                                self.contactlist?.possibleProviders.append(contentsOf: self.contactlistpg?.possibleProviders ?? [])
                                self.contactlist?.normalList.append(contentsOf: self.contactlistpg?.normalList ?? [])
                            }
                            else{
                                self.contactlist = getcontactDataModel(normalList: self.contactlistpg?.normalList ?? [], taggedContact: self.contactlistpg?.taggedContact ?? [], possibleProviders: self.contactlistpg?.possibleProviders ?? [])
                            }
                        }
                                                self.contactlist?.normalList.insert(getcontactDataListModel(headername: "#", contacts: []), at: 0)
                        self.contactlist?.normalList = self.contactlist?.normalList.sorted{$0.Headername?.compare($1.Headername ?? "", options: .caseInsensitive) == .orderedAscending } ?? []
                        let hashIndex = self.contactlist?.normalList.firstIndex(where: {$0.Headername == "#"}) ?? 0
                        for index in 0 ..< (self.contactlist?.normalList.count ?? 0) {
                            if index+1 <= (self.contactlist?.normalList.count ?? 0) - 1 {
                                if self.contactlist?.normalList[index].Headername?.compare(self.contactlist?.normalList[index+1].Headername ?? "", options: .caseInsensitive) == .orderedSame {
                                    self.contactlist?.normalList[index].contactsList.append(contentsOf: self.contactlist?.normalList[index+1].contactsList ?? [])
                                    self.contactlist?.normalList[index+1].contactsList.removeAll()
                                }
                            }
                            let allowedCharacters = CharacterSet(charactersIn: "0123456789+")
                            let characterSet = CharacterSet(charactersIn: self.contactlist?.normalList[index].Headername ?? "")
                            if allowedCharacters.isSuperset(of: characterSet){
                                self.contactlist?.normalList[hashIndex].contactsList.append(contentsOf: self.contactlist?.normalList[index].contactsList ?? [])
                                self.contactlist?.normalList[index].contactsList.removeAll()
                            }
                        }
                        self.contactlist?.taggedContact = Array(_immutableCocoaArray: NSOrderedSet(array: self.contactlist?.taggedContact ?? []))
                        self.contactlist?.possibleProviders = Array(_immutableCocoaArray: NSOrderedSet(array: self.contactlist?.possibleProviders ?? []))
                        self.contactlist?.normalList.forEach({
                            $0.contactsList = Array(_immutableCocoaArray: NSOrderedSet(array: $0.contactsList ))
                        })
                        self.nodataLBL.isHidden = self.contactlistpg?.gettotalcount() != 0
                        self.Allcontactlist = self.contactlist
                        
                        self.remainingcount = (self.contactlist?.taggedContact.count != 0 ? 1 : 0) +  (self.contactlist?.possibleProviders.count != 0 ? 1 : 0)
                    }
                    self.contactsTBL.reloadData()
                    self.hideActivityIndicator()
                    self.contactsTBL.isScrollEnabled = true
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            }
        }
    }
    //MARK: - PROVIDER LIST API
    func providerlistAPI(searchkey:String,filterkey:String){
        showActivityIndicator()
        print("dynamic userid = \(userid)")
        print("dynamic tocken = \(token)")
        print("search key = \(searchkey)")
        print("filterkey = \(filterkey)")
     let params = [
//                   "userId":"63d75ac32e4db50d3a6e086d",
//                   "userId":"63e33c6649e58565630acfbe",
                   "userId":userid,
                  "searchKey":searchkey,
                  "filter":filterkey,
                   "catgoryIndex":providerlistpaginationcount ] as? [String:Any]
        
        let url = kBaseUrl+"getProviderContacts"
        print("pagination count = \(providerlistpaginationcount)")
        let request =  AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print(response)
            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                        self.providerlistpg = providerResponseModel(fromData:responsedata)
                    self.providerlist?.count = self.providerlistpg?.count
                   
                    self.providerlist?.status = self.providerlistpg?.status
                   
                    if let _ = self.providerlist?.Data{
                        self.providerlist?.Data.append(contentsOf: self.providerlistpg?.Data ?? [])
                    }
                    else{
                        self.providerlist = providerResponseModel(fromData: [:])
                        self.providerlist?.Data = self.providerlistpg?.Data ?? []
                        
                    }
                        if self.providerlist?.Data.count == 0{
                            self.nodataLBL.isHidden = false
                        }
                    if self.providerlistpaginationcount == 0 {
                        self.providerlistpagecount = self.providerlistpg?.count ?? 0
                    }
                        self.contactsTBL.reloadData()
                }
                if selectedBTN == 1{
                    self.nodataLBL.isHidden = self.providerlist?.Data.count != 0
                    self.hideActivityIndicator()
                    
                    self.contactsTBL.isScrollEnabled = true
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    //MARK: - TIP CLOSE
    func tipcloseAPI(keyvalue:String = ""){
        
        let params: [String : Any] = [
            "userid": userid,
            "key": keyvalue,
            "value": true
        ]
        
        let url = kBaseUrl+"changeSettings"
        let request =  AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
            print(response)
        }
    }
    
    //MARK: - TAG API FUNCTION
    func taggeneralapi(contactidfromlist : String){
        //        showActivityIndicator()
        print("contactidfromlist = \(contactidfromlist)")
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        let params: [String : Any] = [
            "contactid": contactidfromlist]
        
        let url = "http://13.234.177.61/api7/markAsGeneralContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    //                        hideActivityIndicator()
                    var alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["data"] as? String ?? "" == "Updated sucessfully"{
                            selectedBTN = 0
                            self.tabselector()
                            self.contactsTBL.reloadData()
                        }else{
                           alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                            
                        }
                        
                    }))
                    self.present(alert, animated: true)
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
        
    }
    
    //MARK: - change provider contact to General contact
//    changeToGeneralContact
    func changeproviderapi(contactidfromlist : String){
        //        showActivityIndicator()
        print("contactidfromlist = \(contactidfromlist)")
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        
        let params: [String : Any] = [
            "contactid": contactidfromlist]
        AF.request(kBaseUrl+"changeToGeneralContact", method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    //                        hideActivityIndicator()
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Changed to general contact sucessfully"{
                            selectedBTN = 0
                            self.tabselector()
                            self.contactsTBL.reloadData()
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                            
                        }
                        
                    }
                                                  
                                                 ))
                    self.present(alert, animated: true)
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
        
    }
}


@available(iOS 14.0, *)
extension ContactsmainVC:UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if selectedBTN == 0 {
            searchtexter = textField.text ?? ""
            offsetpg = 0
            print(textField.text ?? "")
            let taggedContact = self.Allcontactlist?.taggedContact.filter({
                $0.name?.localizedCaseInsensitiveContains(searchtexter) ?? false || $0.twilioCallerName?.localizedCaseInsensitiveContains(searchtexter) ?? false
            }) ?? []
            let possibleProviders = self.Allcontactlist?.possibleProviders.filter({
                $0.name?.localizedCaseInsensitiveContains(searchtexter) ?? false || $0.twilioCallerName?.localizedCaseInsensitiveContains(searchtexter) ?? false
            }) ?? []
            var normallistpg = self.Allcontactlist?.normalList ?? []
            normallistpg.forEach({ contact in
                contact.contactsList = contact.contactsList.filter({ $0.name?.localizedCaseInsensitiveContains(searchtexter) ?? false || $0.twilioCallerName?.localizedCaseInsensitiveContains(searchtexter) ?? false })
            })
            normallistpg.removeAll(where: { $0.contactsList.count == 0 })
            self.api(key: textField.text ?? "",filterkey: fkey)
            
        }
        else{
            providerlistAPI(searchkey: textField.text ?? "", filterkey: fkey)
        }
       
    }
}


//MARK: - EXtension (RecommendedCollectionview)
@available(iOS 14.0, *)
extension contactCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "recommendedcollectioncell", for: indexPath) as! recommendedcollectioncell
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = 20
        let h:CGFloat = 20
        return CGSize(width: w, height: h)
    }
    
}


@available(iOS 14.0, *)
extension ContactsmainVC{
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    func showActivityIndicator() {
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        activityView?.stopAnimating()
    }
}

//MARK: - contacts cell
class contactCell:UITableViewCell{
    
    @IBOutlet weak var generalView: BaseView!
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var profileIMG:UIImageView!
    @IBOutlet weak var generalthumbIMG: UIImageView!
    @IBOutlet weak var generalthumbcount:UILabel!
    @IBOutlet weak var categorynameLBL:UILabel!
    @IBOutlet weak var statusnameLBL:UILabel!
    @IBOutlet weak var statusView:UIView!
    @IBOutlet weak var dotBTN:UIButton!
    @IBOutlet weak var dotimg:UIImageView!
    @IBOutlet weak var statusBTNtapped:UIButton!
    @IBOutlet weak var ProviderView: UIView!
    @IBOutlet weak var providerProfileIMG: UIImageView!
    @IBOutlet weak var providerthumbIMG: UIImageView!
    @IBOutlet weak var providerNameLbl: UILabel!
    @IBOutlet weak var proLocationLbl: UILabel!
    @IBOutlet weak var proDotsIMG: UIImageView!
    @IBOutlet weak var providerDotsBTN: UIButton!
    
    @IBOutlet weak var recommendedCV: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var recommendedCVwidth: NSLayoutConstraint!
    
}

class headercell:UITableViewCell{
    @IBOutlet weak var nameLBL:UILabel!
    @IBOutlet weak var dropIMG:UIImageView!
    @IBOutlet weak var dropBTN:UIButton!
}


class recommendedcollectioncell:UICollectionViewCell{
    @IBOutlet weak var recomIMG: UIImageView!
    @IBOutlet weak var viewIMGBg: BaseView!
    override func awakeFromNib() {
        self.recomIMG.layer.cornerRadius = 12
    }
    
}
extension UIColor {
    
    public convenience init?(hex: String) {
        let r, g, b, a: CGFloat
        
        if hex.hasPrefix("#") {
            let start = hex.index(hex.startIndex, offsetBy: 1)
            let hexColor = String(hex[start...])
            
            if hexColor.count == 8 {
                let scanner = Scanner(string: hexColor)
                var hexNumber: UInt64 = 0
                
                if scanner.scanHexInt64(&hexNumber) {
                    r = CGFloat((hexNumber & 0xff000000) >> 24) / 255
                    g = CGFloat((hexNumber & 0x00ff0000) >> 16) / 255
                    b = CGFloat((hexNumber & 0x0000ff00) >> 8) / 255
                    a = CGFloat(hexNumber & 0x000000ff) / 255
                    
                    self.init(red: r, green: g, blue: b, alpha: a)
                    return
                }
            }
        }
        
        return nil
    }
}
