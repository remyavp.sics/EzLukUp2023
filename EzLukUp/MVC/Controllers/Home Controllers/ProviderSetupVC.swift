//
//  ProviderSetupVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 25/10/22.
//

import UIKit
import CoreLocation
import Alamofire

var statedropstatus = false
var citydropstatus = false
var radiusdropstatus = false

var Providersignupname = ""
var providersignupmob = ""
var typedlocation = ""
var typedcompanyname = ""
var typedhoursoperation = ""
var typedabout = ""

var searchArrRes = [getcatDataModel]()
var searching = false
var selectedcategories = [String]()
var selectedcategoriesID = [String]()
var CatnameArray = [String]()
var newcatarray = [getcatDataModel]()
var categorylist : getCategoriesResponse?

class ProviderSetupVC: UIViewController, deletearraydelegate, UIImagePickerControllerDelegate & UINavigationControllerDelegate,providerdatadelegate, UITextFieldDelegate {
    func firtcell(cell: Firstcell, searchtext: String) {
        showResults(string: searchtext)
        pname = cell.nameTextField.text ?? ""
        psearchvlaue = searchtext
        cell.locationTextField.becomeFirstResponder()
    }
    func firtcell(cell:Firstcell,didselect item :String){
        pname = cell.nameTextField.text ?? ""
        psearchvlaue = item
        self.autocompleteResults.removeAll()
        setupTableView.reloadData()
    }
    func diddelete(index: Int) {
        let newindex = newcatarray.firstIndex{$0.name == selectedcategories[index]}
        newcatarray[newindex ?? 0].isselected = false
        selectedcategories.remove(at: index)
        categoryTableView.reloadData()
        setupTableView.reloadData()
        
    }
    //locationpop
    @IBOutlet var locationPopupView: UIView!
    @IBOutlet weak var poplocationTextfield:UITextField!
    @IBOutlet var locationTableview: UITableView!
    @IBOutlet var locationView: UIView!
    
    @IBOutlet weak var setupTableView: UITableView!
    @IBOutlet var categoryPopupView: UIView!
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var DoneBTnView: UIView!
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var chooseBuuton: UIButton!
    @IBOutlet weak var catSearchTF: UITextField!

    
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var dropstatus = false
    var isLocationHasValue = false
    var psearchvlaue = ""
    var pname = ""
    let locationManager = CLLocationManager()
    var getlocality = ""
    var getcountry = ""
    var fulllocation = ""
    var getlat = ""
    var getlong = ""
    var Uname = ""
    var mob = ""
    var pickedimage :UIImage?
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var selectedLocationFromPopup = ""
    
    
    var imagePicker = UIImagePickerController()
    var blurView : UIView!
    let kScreenWidth = UIScreen.main.bounds.width
    let kScreenHeight = UIScreen.main.bounds.height
    
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    var getmobNum : String = UserDefaults.standard.value(forKey: "mynumber") as? String ?? ""
    var getCode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as? String ?? ""
    var activityView: UIActivityIndicatorView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getCategoryAPI()
       // self.poplocationTextfield.delegate = self
       // locationView.isHidden = true
      //  getCurrentLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("reloadcat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ontrue(notification:)), name: Notification.Name("true"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfalse(notification:)), name: Notification.Name("false"), object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = categoryTableView.indexPathForSelectedRow {
            categoryTableView.deselectRow(at: indexPath, animated: true)
        }
    }
    
    
    @objc func onContinueClicked(notification: Notification) {
        categoryTableView.reloadData()
    }
    
    @objc func ontrue(notification: Notification) {
        searching = true
    }
    
    @objc func onfalse(notification: Notification) {
        searching = false
    }
    
    
    
    @IBAction func stateBTNTapped(_ sender: UIButton) {
        let tcell = setupTableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! Thirdcell
        tcell.stateHeightView.constant = 1000
    }
    
    
    
    
    //MARK: - Finish button action
    @IBAction func btnfinishAction(_ sender: UIButton) {
        print("Name ",Providersignupname)
        print("Full location = ",fulllocation)
        print("location from gps","\(self.getlocality) , \(self.getcountry)")
        print("Location from g-Api",selectedLocationFromPopup)
        print("categorys",newcatarray.filter({ $0.isselected }).map({ $0.id }))
        print("Mobile number",UserDefaults.standard.value(forKey: "Kphone") ?? "")
        print("company name",typedcompanyname)
        print("hours of operation",typedhoursoperation)
        print("About",typedabout)
        
                    if Providersignupname == ""{
                    
                        let tcell = setupTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! Firstcell
                      //  tcell.nameasterikImagview.isHidden = false
                       // tcell.locasterikImagview.isHidden = false
                        showDefaultAlert(viewController: self, title: "Message", msg: "Please enter Name")
                    }
        
//                    else if typedlocation == ""{
//
//                        let tcell = setupTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as! Firstcell
//                        tcell.locasterikImagview.isHidden = false
//                        showDefaultAlert(viewController: self, title: "Message", msg: "Please enter Location")
//                    }
                    
                    else if typedabout == ""{
                        
                        let cell = setupTableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! Thirdcell
                       // cell.aboutasterikImgvw.isHidden = false
                        showDefaultAlert(viewController: self, title: "Message", msg: "Please enter About")
                    }
                    
                    else{
                        welcomename = Providersignupname
                        SaveProviderApi()
//                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                        let vc = storyboard.instantiateViewController(identifier: "WelcomemessageVC") as! WelcomemessageVC
//                        self.navigationController?.pushViewController(vc, animated: true)

                    }
        
    }
    
    @IBAction func btnClicked() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
        
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage = info[UIImagePickerController.InfoKey.editedImage] as! UIImage
        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        uploadImage()
        setupTableView.reloadData()
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //
    override func viewDidLayoutSubviews() {
        self.categoryPopupView.roundCorners(corners: [.topLeft,.topRight, .bottomLeft, .bottomRight], radius: 15)
    }
    //MARK: - category api
        func getCategoryAPI() {
         showActivityIndicator()
            let url = "http://13.234.177.61/api7/getCategories"
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
           
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                 hideActivityIndicator()
                                categorylist = getCategoriesResponse(from: responsedata)

                                
                               newcatarray = categorylist?.Data ?? []
                                DispatchQueue.main.async {
                                    if let categories = UserDefaults.standard.value(forKey: "selectedIDs") as? [String]{
                                        newcatarray.forEach({ $0.isselected = categories.contains($0.id ?? "") })
                                        selectedcategories = newcatarray.filter({ $0.isselected }).map({ $0.name ?? "" })
                                        SelectedCategoryID = newcatarray.filter({ $0.isselected }).map({ $0.id ?? "" })
                                    }
                                    searchArrRes = newcatarray
                                    self.categoryTableView.reloadData()
                                    self.setupTableView.reloadSections([1], with: .none)
                                }
                               }
                   
               
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
            }
        }
    
//MARK: - privdersetupapi
    func SaveProviderApi(){
        
        if isNetworkReachable {
            showActivityIndicator()
            
            let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"
            let user = "632dab05aded9e72781ad266"
           
          let params = [
           // "userid":user,
                    "userid":user,
                    "name": Providersignupname,
                    "countrycode": getCode,
                    "phonenumber": getmobNum,
                    "categoryimage":"",
                    "country" : "us",
                    "state": "TX",
                    "city": "Abram",
                    "radiusOfService":"25",
//                    "state": getlocality,
//                    "city": getcountry,
                    "latitude": getlat,
                    "longitude": getlong,
                    "companyname": typedcompanyname,
                    "operationalhours": typedhoursoperation,
                    "aboutbusiness": typedabout,
                    "categoryids": SelectedCategoryID] as? [String : Any]
             
            let url = "http://13.234.177.61/api7/saveProvider"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":tokenn]).validate(statusCode: 200..<500) .responseJSON { response in
                print("Response from server",response)
                switch (response.result) {
                case .success( let JSON):
                    print("Json - ",JSON)
                    self.hideActivityIndicator()
                    loggedin = true
//                    let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                    let vc = storyboard.instantiateViewController(identifier: "WelcomemessageVC") as! WelcomemessageVC
//                    self.navigationController?.pushViewController(vc, animated: true)
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                            self.navigationController?.pushViewController(vc, animated: true)
                    

                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                    showDefaultAlert(viewController: self, msg: "something has happend, try again")
                }
              
            }
        }
    }
    
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
    //MARK: - PROFILE UPLOAD API CALL
         func uploadImage(){
             
             if selectedProfileImage != nil{
                 showActivityIndicator()
                 let uiImage : UIImage = self.selectedProfileImage
                 imageData = uiImage.jpegData(compressionQuality: 0.2)!
                 
                 imageStr = imageData.base64EncodedString()
                 fileName = "image"
                 mimetype = mimeType(for: imageData)
                 print("mime type = \(mimetype)")
             }else{
                 imageData = Data()
                 fileName = ""
             }
           
             ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
                 print(response as Any)
                 if success {
                     if response!["status"] as! Bool == true {
                         self.hideActivityIndicator()
                         print("image upload success")
                         
                     }else {
                         showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                     }
                 }else{
                     print(error?.localizedDescription as Any)
                     showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
                 }
             }
         }
    
    //
    func getCurrentLocation(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        DispatchQueue.global().async {
              if CLLocationManager.locationServicesEnabled() {
                  self.locationManager.delegate = self
                  self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                  self.locationManager.startUpdatingLocation()
              }
        }
        
    }
    @IBAction func btnDropdownAction(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.setupTableView.reloadSections([1], with: .none)
    }
    
    @IBAction func btnDropdownActionstate(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.setupTableView.reloadSections([1], with: .none)
    }
    @IBAction func btnDropdownActioncity(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.setupTableView.reloadSections([1], with: .none)
    }
    
    
    @IBAction func DoneSelectingCategoryBTN(_ sender: UIButton) {
     hideResult()
        didmissLocationPopUp()
        print("done clicked")
        setupTableView.reloadSections([0], with: .none)
    }
    //MARK: - Google place api codes
    
    @IBAction func locationBtnTapped(_ sender: UIButton) {
        print("location btn clicked",sender.tag)
        showLocationPopUp()
//        getCurrentLocation()
    }
    
    @IBAction func closelocationpopup(_ sender: UIButton) {
        didmissLocationPopUp()
    }
    
    
    
    func showResults(string:String){
      var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                 
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                   
                    print("loc table count",self.autocompleteResults.count)
                   
                    self.setupTableView.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    

    //
}

extension ProviderSetupVC: UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == setupTableView{
            return 3
        }
//        else if tableView == locationTableview {
//            return 1
//        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == setupTableView{
            if section == 0{
                return 1
            }
            if section == 1{
                return 1
            }
            else {
                return 1
            }
        }
//        else if tableView == locationTableview{
//            return autocompleteResults.count
//
//        }
        else {
            //check search text & original text
            if( searching == true){
                return searchArrRes.count
            }else{
                return newcatarray.count
            }

        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == setupTableView{
            if indexPath.section == 0{
                let cell = setupTableView.dequeueReusableCell(withIdentifier: "Firstcell", for: indexPath) as! Firstcell
//                cell.googlelocationBTN.tag = indexPath.row
                if pickedimage?.imageAsset == nil {
                    print("nothing")

                }
                else{
                    print("image is present")
                    cell.userImageview.image = pickedimage
                    
                }
                    cell.nameTextField.text = Providersignupname
               // fulllocation = selectedLocationFromPopup
                  //  cell.locationTextField.text = selectedLocationFromPopup
                    
                
//                if cell.locationTextField.text == "" && cell.nameTextField.text != "" {
//                    cell.locationTextField.becomeFirstResponder()
//                }
//               cell.locationTextField.text = "\(getlocality) , \(getcountry)"
                
//                if cell.locationTextField.text == "" && cell.nameTextField.text != "" {
//                    cell.locationTextField.becomeFirstResponder()
//                }
//                cell.delegate = self
//                cell.autocompleteResults = self.autocompleteResults
//                cell.nameTextField.text = pname
//                cell.locationTextField.text = psearchvlaue
//                cell.searchResultheight.constant = CGFloat(self.autocompleteResults.count * 30)
//                cell.loctable.reloadData()
                
                
                return cell
            }
            else if indexPath.section == 1 {
                let cell = setupTableView.dequeueReusableCell(withIdentifier: "SecondCategorycell", for: indexPath) as! SecondCategorycell
                cell.delegate = self
                cell.selectedArray = selectedcategories
                cell.CategoryTV.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    if self.dropstatus {
                       
                        cell.searchView.isHidden = false
                        cell.searchViewHeight.constant = 40
                        cell.dropviewHeight.constant = 500
                        cell.dropview.isHidden = false
                        cell.dropimg.image = UIImage(named: "dropup_icon")
                        cell.dropview.addSubview(self.categoryPopupView)
                       
                    }
                    else{
                        
                        cell.dropviewHeight.constant = 0
                        cell.searchViewHeight.constant = 0
                        cell.searchView.isHidden = true
                        cell.dropview.isHidden = true
                        cell.dropimg.image = UIImage(named: "dropdown_icon")
                       
                    }
                    if selectedcategories.count == 1{
                        if self.dropstatus {
                            cell.catgoryviewHeight.constant = 83

                        }else {
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 11 )
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11 )
                        }
                    }
                    else if selectedcategories.count > 1{
                        if self.dropstatus{
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 40 + 11)
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 40 + 11)
                        }
                        else{
//                            cell.catgoryviewHeight.constant = CGFloat((self.selectedcategories.count * 32) + 11)
                            cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11)
                        }
                    }
                        else{
                            
                            cell.catgoryviewHeight.constant = 52
                            
                        }
                    
                }
                )
                

                
                
                return cell
            }
            else{
                let cell = setupTableView.dequeueReusableCell(withIdentifier: "Thirdcell", for: indexPath) as! Thirdcell
                
                cell.callback = {
                    self.setupTableView.reloadSections([2], with: .none)
                    }
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    if statedropstatus {
                        
                        cell.stateHeightView.constant = 1000
                        //                    self.setupTableView.reloadData()
                    }
                    else{
                        //  self.setupTableView.reloadData()
                        cell.stateHeightView.constant = 0
                        
                    }
                })
                
                return cell
            }
        }
        
        //MARK: - category tableview cellforrowat
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryPopupCell") as! CategoryPopupCell
            print("status",searching)
            cell.catLbl.text = searchArrRes[indexPath.row].name ?? "" //
            cell.catLbl.textColor = searchArrRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
            return cell
        }
        
//        else{
//            let cell = tableView.dequeueReusableCell(withIdentifier: "locationtablecell") as! locationtablecell
//            cell.locLbl.text = autocompleteResults[indexPath.row].formattedAddress
//            return cell
//        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == setupTableView{
            if indexPath.section == 1{
                if selectedcategories.count == 1{
                    if self.dropstatus {
                        return 83 + 9 + 500

                    }else {
                       return CGFloat((selectedcategories.count * 32) + 11 + 11)
//                        return UITableView.automaticDimension
                    }
                }
                else if selectedcategories.count > 1{
                    if self.dropstatus{
                        return CGFloat((selectedcategories.count * 32) + 40 + 11 + 9 + 500)
                    }
                    else{
                        return CGFloat((selectedcategories.count * 32) + 11 + 11)
//                        return UITableView.automaticDimension
                    }
                }
                    else{
                        if self.dropstatus {
                            return 52 + 500
                        }
                        return 52

                    }
            }
            else if indexPath.section == 2{
//                return UITableView.automaticDimension
                if statedropstatus{
                    return 1000
                }else{
                    return UITableView.automaticDimension
                }
                
            }else{
                print("tim tim tim")
                return UITableView.automaticDimension
            }
          //  return UITableView.automaticDimension
        }else if tableView == locationTableview{
            return 35
        }
        else{
            //return UITableView.automaticDimension
            return 45
        }
        
        
        
    }
    //MARK: - category tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView{
            let cell = setupTableView.dequeueReusableCell(withIdentifier: "SecondCategorycell", for: indexPath) as! SecondCategorycell
            let ss = newcatarray.filter{$0.isselected}.map{$0.name ?? ""}
            
            if ss.count < 3 {
                searchArrRes[indexPath.row].isselected = !searchArrRes[indexPath.row].isselected
                let index = newcatarray.firstIndex(where: { $0.id ==  searchArrRes[indexPath.row].id }) ?? 0
                newcatarray[index].isselected = searchArrRes[indexPath.row].isselected
                selectedcategories = newcatarray.filter{$0.isselected}.map{$0.name ?? ""}
                SelectedCategoryID = newcatarray.filter{$0.isselected}.map{$0.id ?? ""}
                print("ss count ==",ss.count)
                print("selected cat count ==",selectedcategories.count)
            }
            else{
                print("category selection is restricted to max 3")
                print("selected cat count ==",selectedcategories.count)
                print("ss count ==",ss.count)
                dropstatus = false
            }
            categoryTableView.reloadData()
            setupTableView.reloadData()
            //
        }
//        else if tableView == locationTableview{
//            poplocationTextfield.text = autocompleteResults[indexPath.row].formattedAddress
//            poplocationTextfield.resignFirstResponder()
//            selectedLocationFromPopup = autocompleteResults[indexPath.row].formattedAddress
//            hideResult()
//            self.isLocationHasValue = true
//            didmissLocationPopUp()
//            self.setupTableView.reloadData()
//        }
        else{
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == categoryTableView{
            
        }
    }
    
}


extension ProviderSetupVC: CLLocationManagerDelegate {
    func showResult(string:String){
        var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                    self.locationView.isHidden = false
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                    self.locationTableview.reloadData()
                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    func hideResult(){
        self.locationView.isHidden = true
        poplocationTextfield.text = ""
        autocompleteResults.removeAll()
        locationTableview.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        hideResult()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == poplocationTextfield{
            let text = textField.text! as NSString
            let fullText = text.replacingCharacters(in: range, with: string)
            if fullText.count > 0 {
                showResult(string:fullText)
            }else{
                hideResult()
            }
        }
        else if textField == catSearchTF{
            
        }
        else{
            
        }
        
        return true
    }
    
    //show location popup
    @objc func showLocationPopUp(){
        blurView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height:  kScreenHeight))
        blurView.backgroundColor = .black.withAlphaComponent(0.2)
        view.addSubview(blurView)
        let h = autocompleteResults.count * 30
        self.locationPopupView.frame = CGRect(x: (kScreenWidth - 320) / 2, y: (kScreenHeight - 400) / 2, width: 320, height: 400)
        blurView.addSubview(self.locationPopupView)
        blurView.bringSubviewToFront(self.locationPopupView)
        locationPopupView.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 0.33, animations: {
            self.locationPopupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    //hide location popup
    func didmissLocationPopUp() {
        if blurView != nil {
            UIView.animate(withDuration: 0.33, animations: {
                self.blurView.alpha = 0
            }, completion: { (completed) in
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            }, completion: { (completed) in
                self.blurView.gestureRecognizers?.removeAll()
                self.blurView.removeFromSuperview()
                self.blurView = nil
            })
        }
    }
    
    
    
    
    
    
    //
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isLocationHasValue{
                    return
                }
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let userLocation:CLLocation = locations[0] as CLLocation
        
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }
            if let placemark = placemarks {
                if placemark.count>0 {
                    self.isLocationHasValue = true
                    let placemark = placemarks?.first
                    let loc = placemark?.locality ?? ""
                    let area = placemark?.administrativeArea ?? ""
                    let Country = placemark?.country ?? ""
                    
                    self.getlocality = loc
                    self.getcountry = Country
                    
                    self.getlocality = loc
                    self.getcountry = Country
                    self.getlat = (" \(userLocation.coordinate.latitude)")
                    self.getlong = ("\(userLocation.coordinate.longitude)")
                    self.selectedLocationFromPopup = "\(self.getlocality) , \(self.getcountry)"
                    
                }
            }
            self.setupTableView.reloadData()
        }
    }
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            print("location manager authorization status changed")
            
            switch status {
            case .authorizedAlways:
                print("user allow app to get location data when app is active or in background")
            case .authorizedWhenInUse:
                print("user allow app to get location data only when app is active")
            case .denied:
                print("user tap 'disallow' on the permission dialog, cant get location data")
            case .restricted:
                print("parental control setting disallow location data")
            case .notDetermined:
                print("the location permission dialog haven't shown before, user haven't tap allow/disallow")
            }
        }
}


protocol deletearraydelegate{
    func diddelete(index:Int)
}

extension ProviderSetupVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}

class cat
{
    var category = ""
    var isselected = false
    init(data:String)
    {
        self.category = data
    }
}
// locationtableview cell
class searchResultCell:UITableViewCell{
    @IBOutlet weak var locLbl: UILabel!
}
class locationtablecell:UITableViewCell{
    @IBOutlet weak var locLbl: UILabel!
}

class userdetails{
    var name = ""
    var searchtext = ""
    func saveuserdetail(name:String,searchtext:String){
        self.name = name
        self.searchtext = searchtext
    }
    func readuserdtail() -> (String,String){
        return (self.name,self.searchtext)
    }
}
