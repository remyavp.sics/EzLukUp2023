//
//  DashboardVC.swift
//  EzLukUp
//
//  Created by Srishti on 12/10/22.
//

import UIKit
import Alamofire

class DashboardVC: UIViewController {
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    var Dashboardmodel : DashboardDataModel?
    
    @IBOutlet weak var DashTableview: UITableView!
    
    @IBOutlet weak var HelloLbl: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.dashboardAPI()
        }
       
        let username = UserDefaults.standard.value(forKey: "Kname")
//        HelloLbl.text = "Hello \(username ?? "")!"
        
    }
    //MARK: - Dashboard API
    func dashboardAPI(){
        let params: [String : Any] = [
            "userId": "632dab05aded9e72781ad266"
        ]
        let urlp = kBaseUrl+"providerDashboardStatistics"
        let url = kBaseUrl+"dashboardStatistics"
        let request =  AF.request(urlp, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"]).validate(statusCode: 200..<510) .responseJSON { response in
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata :",responsedata)
                    self.Dashboardmodel = DashboardDataModel(from:responsedata)
                    print(self.Dashboardmodel?.Data?.recentFeedback.count ?? 0)
                    print("name = \(self.Dashboardmodel?.Data?.recentRecommendProviders[0].contacts?.name)")
                    DispatchQueue.main.async {
                        self.DashTableview.reloadData()
                    }
                   
                 
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    

}
extension DashboardVC{
    
}
extension DashboardVC:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            //status section
            return 1
        }
        if section == 1{
            //recent recomendation
            return 1
        } else if section == 2{
            //recent feedback
            return 1
        }
        else {
            //
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = DashTableview.dequeueReusableCell(withIdentifier: "CardviewCell", for: indexPath) as! CardviewCell
            cell.generalcontactscountLBL.text = Dashboardmodel?.Data?.genralContactsCount?.description
            cell.providercountLBL.text = Dashboardmodel?.Data?.serviceProviderContactsCount?.description
            cell.rewardcountLBL.text = "Total Rewards - \(Dashboardmodel?.Data?.rewardPoints?.description ?? "0") ezM"
            return cell
        }
        else if indexPath.section == 1 {
            let cell = DashTableview.dequeueReusableCell(withIdentifier: "Listcell", for: indexPath) as! Listcell
            cell.Dashboardmodel = Dashboardmodel
            cell.recomendationCV.reloadData()
            
            return cell
        }
        else if indexPath.section == 2 {
            let cell = DashTableview.dequeueReusableCell(withIdentifier: "Listcell2", for: indexPath) as! Listcell2
            cell.Dashboardmodel = Dashboardmodel
            cell.recomendationCV.reloadData()
            return cell
        }
        else {
            let cell = DashTableview.dequeueReusableCell(withIdentifier: "Listcell3", for: indexPath) as! Listcell3
            cell.Dashboardmodel = Dashboardmodel
            cell.recomendationCV.reloadData()
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableView.automaticDimension
        }
        else{
            return UITableView.automaticDimension
        }
    }
    
    
}





































