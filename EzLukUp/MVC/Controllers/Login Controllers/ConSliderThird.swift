//
//  ConSliderThird.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/10/22.
//

import UIKit

class ConSliderThird: UIViewController {
    
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    
    @IBOutlet weak var continueBtn: BaseButton!
    
   override func viewDidLoad() {
        super.viewDidLoad()
       label1.font = UIFont(name: "Jost-Regular", size: 22)
       label2.font = UIFont(name: "Jost-Regular", size: 17)
       label3.font = UIFont(name: "Jost-Regular", size: 17)
       continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
   }
   
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        NotificationCenter.default.post(name: Notification.Name("nextslide"), object: nil)
    }
    
    
 
}
