//
//  SetupVC.swift
//  EzLukUp
//
//  Created by Srishti on 11/10/22.
//

import UIKit
import CoreLocation
import Alamofire

class SetupVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate {
    
 //MARK: -Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var locationTextField: UITextField!
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var locationTable: UITableView!
    @IBOutlet weak var mView: UIView!
    
    @IBOutlet weak var mScrollView: UIScrollView!
    @IBOutlet weak var locationTBLheight: NSLayoutConstraint!
    @IBOutlet weak var continueBtn: UIButton!
    
    
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    let locationManager = CLLocationManager()
//    var getmobNum = String()
    var getName = String()
//    var getCode = String()
    var getlocality = ""
    var getcountry = ""
    var getlat = ""
    var getlong = ""
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var autocompleteResults :[GApiResponse.Autocomplete] = []
    var kstatus = 0
    var isLocationHasValue = false
    
    var token = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    var getmobNum : String = UserDefaults.standard.value(forKey: "mynumber") as? String ?? ""
    var getCode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as? String ?? ""
    var activityView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker.allowsEditing = true
        print("image ==== ",pickedimage as Any)
        locationTextField.delegate = self
        getCurrentLocation()
          
        NotificationCenter.default.addObserver(self,selector:#selector(keyboardWillShow),name:UIResponder.keyboardWillShowNotification, object: nil)
        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        
    }
}
 
//MARK: - functions & @IBActions
extension SetupVC{
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
//            if self.mView.frame.origin.y == 0 {
                kstatus = Int(keyboardSize.height)
//                self.mView.frame.origin.y -= keyboardSize.height
//                view.layoutSubviews()
           
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        if self.mView.frame.origin.y != 0 {
            self.mView.frame.origin.y = 0
        }
    }
    
    
    func getCurrentLocation(){
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.requestWhenInUseAuthorization()
        
        DispatchQueue.global().async {
            if CLLocationManager.locationServicesEnabled(){
                self.locationManager.delegate = self
                self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                self.locationManager.startUpdatingLocation()
            }
        }
        
    }
    
    @IBAction func ContinueBtn(_ sender: UIButton) {
        if pickedimage != nil{
            uploadImage()
        }
        saveUserDetails()
       
    }
    //MARK: - profile image picker =======================
    @IBAction func btnClicked() {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = true //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
        
    }
    //MARK: image picker delegate method
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.editedImage] as! UIImage

        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        profileIMG.image = pickedimage
        print("image ==== ",pickedimage as Any)

        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
 //MARK: - PROFILE UPLOAD API CALL
      func uploadImage(){
          if selectedProfileImage != nil{
              showActivityIndicator()
              let uiImage : UIImage = self.selectedProfileImage
              imageData = uiImage.jpegData(compressionQuality: 0.2)!
              
              imageStr = imageData.base64EncodedString()
              fileName = "image"
              mimetype = mimeType(for: imageData)
              print("mime type = \(mimetype)")
          }else{
              imageData = Data()
              fileName = ""
          }
        
          ServiceManager.sharedInstance.uploadSingleDataa("saveProfileImage", headerf: token, parameters: ["userId":userid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
              print(response as Any)
              if success {
                  if response!["status"] as! Bool == true {
                      self.hideActivityIndicator()
                      print("image upload success")
                      
                  }else {
                      showDefaultAlert(viewController: self, title: "", msg: "image upload unsuccessfull")
                  }
              }else{
                  print(error?.localizedDescription as Any)
                  showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
              }
          }
      }
    
}

extension SetupVC: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isLocationHasValue{
                               return
                           }
        self.isLocationHasValue = true
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("locations = \(locValue.latitude) \(locValue.longitude)")
        
        let userLocation:CLLocation = locations[0] as CLLocation
     
        print("user latitude = \(userLocation.coordinate.latitude)")
        print("user longitude = \(userLocation.coordinate.longitude)")
        
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(userLocation) { (placemarks, error) in
            if (error != nil){
                print("error in reverseGeocode")
            }

            if let placemark = placemarks {
                if placemark.count>0 {
                    let placemark = placemarks?.first
                    let loc = placemark?.locality ?? ""
                    let area = placemark?.administrativeArea ?? ""
                    let Country = placemark?.country ?? ""
                    self.locationTextField.text = "\(loc) , \(Country)"
                    //  self.getcountryDelegate.getCountryName(name: self.countryName)
                    // return
                    
                    self.getlocality = loc
                    self.getcountry = Country
                    self.getlat = (" \(userLocation.coordinate.latitude)")
                    self.getlong = ("\(userLocation.coordinate.longitude)")
                   
                }
            }
        }
    }
}

extension SetupVC{
  //MARK: - SAVE API Call
    func saveUserDetails(){
        showActivityIndicator()
    let name = nameTextField.text ?? ""
        
    let params: [String : Any] = [
              "userId": userid,
              "name": name,
              "countryCode": getCode,
              "phoneNumber": getmobNum,
              "categoryImage": "",
              "state": getcountry,
              "city": getlocality,
              "latitude": getlat,
              "longitude": getlong]
    let url = "http://13.234.177.61/api7/saveUser"
    print("params\(params)")
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
        
        switch (response.result) {
        case .success( let JSON):
            if let responsedata =  JSON as? [String:Any]  {
              print("responsedata :",responsedata)
                hideActivityIndicator()
              
                UserDefaults.standard.setValue("true", forKey: "loggedin")
                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
                let nav = UINavigationController(rootViewController: homeViewController)
                nav.navigationBar.isHidden = true
                appdelegate.window!.rootViewController = nav
                        navigationController?.popToRootViewController(animated: true)
                        loggedin = true
                        alreadyloggedin = true
                
//
//                let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
//                        self.navigationController?.pushViewController(vc, animated: true)
            }
            
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
        }
     }
   }
}

extension SetupVC:UITableViewDelegate,UITableViewDataSource{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == locationTextField{#imageLiteral(resourceName: "profile icon")
            let point = CGPoint(x: 0, y: (self.view.frame.height) / 4) // 200 or any value you like.
            self.mScrollView.contentOffset = point
        }
        
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == locationTextField{
            let text = textField.text! as NSString
            let fullText = text.replacingCharacters(in: range, with: string)
            if fullText.count > 0 {

                showResults(string:fullText)
            }else{
                hideResults()
            }
            
        }
        return true
    }
    
    func showResults(string:String){
      var input = GInput()
        input.keyword = string
        GoogleApi.shared.callApi(input: input) { (response) in
            if response.isValidFor(.autocomplete) {
                DispatchQueue.main.async {
                 
                    self.autocompleteResults = response.data as! [GApiResponse.Autocomplete]
                   
                    self.locationTBLheight.constant = CGFloat(self.autocompleteResults.count * 30)
                    print("loc table count",self.autocompleteResults.count)
                    
                    self.locationTable.reloadData()
                    
//                    if self.kstatus == 0  {
                    print("Mview origin is - ",self.mView.frame.origin.y)
                        
//                    }

                }
            } else { print(response.error ?? "ERROR") }
        }
    }
    func hideResults(){
        locationTBLheight.constant = 0
        autocompleteResults.removeAll()
        locationTable.reloadData()
        print("Mview origin is - ",self.mView.frame.origin.y)
//            if self.mView.frame.origin.y != 0 {
//                self.kstatus = 0
//                self.mView.frame.origin.y = 0
//                self.mView.layoutSubviews()
//                self.mView.layoutIfNeeded()
//        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return autocompleteResults.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "locationcell") as! locationcell
        
        cell.locLBL.text = autocompleteResults[indexPath.row].formattedAddress
      
       
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        locationTextField.text = autocompleteResults[indexPath.row].formattedAddress
        locationTextField.resignFirstResponder()
        hideResults()
    }
    
    
}

extension SetupVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}


//location tableview cell
class locationcell:UITableViewCell{
    @IBOutlet weak var locLBL:UILabel!
}

