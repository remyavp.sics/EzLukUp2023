//
//  WelcomemessageVC.swift
//  EzLukUp
//
//  Created by Srishti on 11/10/22.
//

import UIKit
var welcomename = ""
class WelcomemessageVC: UIViewController {
    
   
    
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var WelcomenameLBl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(welcomename)
        WelcomenameLBl.text = "Welcome \(welcomename)"
        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        
    }
    

    @IBAction func ContinueBtn(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactSyncedMessageVC") as! ContactSyncedMessageVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
}
