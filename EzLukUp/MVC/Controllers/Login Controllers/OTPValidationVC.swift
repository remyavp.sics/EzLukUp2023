//
//  OTPValidationVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 10/10/22.
//

import UIKit
import Alamofire
import PhoneNumberKit

    class PhoneVerificationVC: UIViewController {

        @IBOutlet weak var txt1: UITextField!
        @IBOutlet weak var txt2: UITextField!
        @IBOutlet weak var txt3: UITextField!
        @IBOutlet weak var txt4: UITextField!
        @IBOutlet weak var lblNumber: UILabel!
        @IBOutlet weak var view1: UIView!
        @IBOutlet weak var view2: UIView!
        @IBOutlet weak var view3: UIView!
        @IBOutlet weak var view4: UIView!
        
        
        var getphNo = String()
        var getcode = String()
        var fromdisabled : Bool = false
        var message = String()
        var getMobileNumber = String()
        var otpinfo =  Dictionary<String, AnyObject>()
        var otpVerifyinfo =  Dictionary<String, AnyObject>()
        @IBOutlet weak var lblTime: UILabel!
        @IBOutlet weak var btnResendOtp: UIButton!
        
        var countTimer:Timer!
        var counter = 30
        
        var OTP = String()
        var activityView: UIActivityIndicatorView?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            
            initializeHideKeyboard()
         print(getMobileNumber)
            
            self.navigationController?.isNavigationBarHidden = true
//            self.lblNumber.text = getMobileNumber
//            ZUtility.addAlertView("OTP", message, kLocalization.kOk,self)
            self.intialSetup(txt1: txt1, txt2: txt2, txt3: txt3, txt4: txt4)
            otpinfo["mobile"] = getMobileNumber as AnyObject
            otpinfo["user_type"] = "APP_USER" as AnyObject
            //self.callResendOTPApi()
            otpTimerStart()
            textBorderSetup()
            
        }
        
        func textBorderSetup(){
            txt1.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            txt1.layer.borderWidth = 1
            txt1.addCornerForView(cornerRadius: 14.0)
            txt2.layer.borderWidth = 1
            txt2.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            txt2.addCornerForView(cornerRadius: 14.0)
            txt3.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            txt3.layer.borderWidth = 1
            txt3.addCornerForView(cornerRadius: 14.0)
            txt4.layer.borderWidth = 1
            txt4.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
            txt4.addCornerForView(cornerRadius: 14.0)
            
        }
        
        @IBAction func btnBackAction(_ sender: UIButton) {
            self.navigationController?.popViewController(animated: true)
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }

        func otpTimerStart(){
            
            counter = 30
            guard countTimer == nil else { return }
            self.countTimer = Timer.scheduledTimer(timeInterval: 1 ,
            target: self,
            selector: #selector(self.changeTitle),
            userInfo: nil,
            repeats: true)
        }
        
        func stopTimer() {
            
            guard countTimer != nil else { return }
            countTimer?.invalidate()
            countTimer = nil
        }
        
        @objc func changeTitle()
        {
             if counter != 0
             {
                // self.lblTime.text = "The Code is valid for 5 minutes \n\nResend code in \(counter) sec"
                 self.btnResendOtp.isEnabled = false
                 self.btnResendOtp.alpha = 0.5
                 counter -= 1
             }
             else
             {
                  countTimer.invalidate()

                 // self.lblTime.text = "The Code is valid for 5 minutes \n\nResend code in \(counter) sec"
                  self.btnResendOtp.isEnabled = true
                  self.btnResendOtp.alpha = 1.0
             }
        }
        
       
        
        
        @IBAction func resendAction(_ sender: UIButton) {
            
            otpinfo["mobile"] = getMobileNumber as AnyObject
            otpinfo["user_type"] = "APP_USER" as AnyObject
            self.callResendOTP()
            otpTimerStart()
            
        }
            

        
    }

    extension PhoneVerificationVC: UITextFieldDelegate {
        
        func intialSetup(txt1: UITextField, txt2: UITextField, txt3: UITextField, txt4: UITextField){
            
            txt1.delegate = self
            txt2.delegate = self
            txt3.delegate = self
            txt4.delegate = self
            
            txt1.becomeFirstResponder()

            txt1.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
            txt2.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
            txt3.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
            txt4.addTarget(self, action: #selector(handleOtp(_:)), for: UIControl.Event.editingChanged)
        
        }
        
        override var preferredStatusBarStyle: UIStatusBarStyle {
            return .lightContent
        }
        
        @objc private func handleOtp(_ textField: UITextField) {
            
            let text = textField.text
            if (text?.utf16.count)! >= 1
            {
                switch textField {
                    case txt1: self.txt2.becomeFirstResponder()
                    case txt2: self.txt3.becomeFirstResponder()
                    case txt3: self.txt4.becomeFirstResponder()
                    case txt4: self.txt4.resignFirstResponder()
                    
                    do {
                        txt4.resignFirstResponder()
                        
                        let otp = "\(txt1.text!)\(txt2.text!)\(txt3.text!)\(txt4.text!)"
                        print("-----Call Api-----OTP : \(otp)")
                        
                        otpVerifyinfo["mobile"] = getMobileNumber as AnyObject
                        otpVerifyinfo["otp"] = otp as AnyObject
                        otpVerifyinfo["user_type"] = "APP_USER" as AnyObject
                        
                        callVerifyOTPApi()
//                        pushNavController(navName: "ProviderSetupVC", storyboardName: "Home")
//                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
//                        let vc = storyboard?.instantiateViewController(identifier: "SetupVC") as! SetupVC
//                    self.navigationController?.pushViewController(vc, animated: true)
                       
                       
                    }
                    
                    default: txt4.resignFirstResponder()
                }
            }
            else {
                
                switch textField {
                case txt4:
                    
                    txt4.text!.isEmpty ? self.txt3.becomeFirstResponder() : self.txt4.becomeFirstResponder()
                    
                case txt3:
                        
                    txt3.text!.isEmpty ? self.txt2.becomeFirstResponder() : self.txt4.becomeFirstResponder()
                    
                case txt2:
                        
                    txt2.text!.isEmpty ? self.txt1.becomeFirstResponder() : self.txt3.becomeFirstResponder()
                    
                case txt1:
                    
                    txt1.text!.isEmpty ? self.txt1.becomeFirstResponder() : self.txt2.becomeFirstResponder()
                    
                    default: break
                }
            }
        }
        
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            if string.rangeOfCharacter(from: NSCharacterSet.decimalDigits) != nil {
                
               let newLength = (textField.text?.count)! + string.count - range.length
                
                if newLength == 2 {
                    
                    switch textField {
                    case txt4:
                        
                        txt4.text!.isEmpty ? self.txt3.becomeFirstResponder() : self.txt4.becomeFirstResponder()
                        
                    case txt3:
                            
                        txt3.text!.isEmpty ? self.txt2.becomeFirstResponder() : self.txt4.becomeFirstResponder()
                        
                    case txt2:
                            
                        txt2.text!.isEmpty ? self.txt1.becomeFirstResponder() : self.txt3.becomeFirstResponder()
                        
                    case txt1:
                        
                        txt1.text!.isEmpty ? self.txt1.becomeFirstResponder() : self.txt2.becomeFirstResponder()
                        
                        default: break
                    }
                }
                
               return newLength <= 1
                
            } else {
               return true
            }

        }
        
        func clearTextFields(){
            txt1.text = ""
            txt2.text = ""
            txt3.text = ""
            txt4.text = ""
        }
    }


extension PhoneVerificationVC{
//MARK: - API Call
func callVerifyOTPApi(){
    showActivityIndicator()
//    let country : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String ?? ""
    let otp1 = txt1.text ?? ""
    let otp2 = txt2.text ?? ""
    let otp3 = txt3.text ?? ""
    let otp4 = txt4.text ?? ""
        OTP = otp1+otp2+otp3+otp4

    let params: [String : Any] = [
              "phoneNumber": getMobileNumber,
              "countryCode": getcode,
              "code": OTP]
    let url = "http://13.234.177.61/api7/validateOTP"
    
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: nil).validate(statusCode: 200..<510) .responseJSON { [self] response in
        
        switch (response.result) {
        case .success( let JSON):
            if let responsedata =  JSON as? [String:Any]  {
              print("responsedata :",responsedata)
                hideActivityIndicator()
//
                if alreadyloggedin {
                    if fromdisabled{
                        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: "AccountSettingsVC") as! AccountSettingsVC
                        vc.fromdisabled = true
                        self.navigationController?.pushViewController(vc, animated: true)
                    }else{
                        let storyboard = UIStoryboard(name: "Home", bundle: nil)
                        let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                        self.navigationController?.pushViewController(vc, animated: true)
                    }

                }
                else{
                    print("BLA BLA BLA")
                    let storyboard = UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "ContactSyncedMessageVC") as! ContactSyncedMessageVC
                    self.navigationController?.pushViewController(vc, animated: true)

////
////                    var type : String = UserDefaults.standard.value(forKey: "Ktype") as! String
////                    if type == "Personal"{
////                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
////                                       let vc = storyboard.instantiateViewController(identifier: "SetupVC") as! SetupVC
////                                           vc.getmobNum = self.getphNo
////                                           vc.getCode = self.getcode
////                                           self.navigationController?.pushViewController(vc, animated: true)
////                   }else {
////                       let storyboard = UIStoryboard(name: "Home", bundle: nil)
////                                      let vc = storyboard.instantiateViewController(identifier: "ProviderSetupVC") as! ProviderSetupVC
////                                          vc.getmobNum = self.getphNo
////                                          vc.getCode = self.getcode
////                                          self.navigationController?.pushViewController(vc, animated: true)
////                   }
                }
 
//
            }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
        }
 }
}
    func callResendOTP(){
        showActivityIndicator()
        let country : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
                  "phoneNumber": getMobileNumber,
                  "countryCode": country]
        let url = "http://13.234.177.61/api7/varifyPhoneNumber"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: nil).validate(statusCode: 200..<510) .responseJSON { [self] response in
            print(response)
            hideActivityIndicator()
        }
    }
}

extension PhoneVerificationVC{
    func initializeHideKeyboard(){
     //Declare a Tap Gesture Recognizer which will trigger our dismissMyKeyboard() function
     let tap: UITapGestureRecognizer = UITapGestureRecognizer(
     target: self,
     action: #selector(dismissMyKeyboard))
     //Add this tap gesture recognizer to the parent view
     view.addGestureRecognizer(tap)
     }
     @objc func dismissMyKeyboard(){
     //endEditing causes the view (or one of its embedded text fields) to resign the first responder status.
     //In short- Dismiss the active keyboard.
     view.endEditing(true)
     }
    
}
extension UIViewController{
    func pushNavController (navName : String, storyboardName: String){
        
        let storyBoard : UIStoryboard = UIStoryboard(name: storyboardName, bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: navName)
        navigationController?.pushViewController(nextViewController, animated: true)
    }
}


extension PhoneVerificationVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}
