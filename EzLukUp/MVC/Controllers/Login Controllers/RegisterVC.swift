//
//  RegisterVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/10/22.
//

import UIKit
import CountryPickerView
import Alamofire
import PhoneNumberKit

var loggedin = false
var alreadyloggedin = false

class RegisterVC: UIViewController, UITextFieldDelegate {
    
    // @IBOutlet weak var countrydropBtn: UIButton!
    @IBOutlet weak var continueBtn: UIButton!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var flagView: UIView!
    @IBOutlet weak var countryCodeLbl: UILabel!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var countryPickerView: CountryPickerView!
    @IBOutlet weak var privacyLbl: UILabel!
    
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var code = ""
    var phonenumber = ""
    var getchecknumberdetails : getchecknumberResponse?
    var getuserdetails : loginapiResponseModel?
    var activityView: UIActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.flagView.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.flagView.layer.borderWidth = 1
        self.numberView.layer.borderWidth = 1
        self.numberView.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.continueBtn.addCornerForView(cornerRadius: 14.0)
        self.flagView.addCornerForView(cornerRadius: 14.0)
        self.numberView.addCornerForView(cornerRadius: 14.0)
        phoneNumberTextField.delegate = self
        setUpUI()
        
        privacyLbl.font = UIFont(name: "Jost-Regular", size: 17)
        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        
    }
    
    func setUpUI(){
        addDoneButtonOnKeyboard()
        countryPickerView.showPhoneCodeInView = false
        countryPickerView.showCountryCodeInView = true
        countryPickerView.showCountryNameInView = false
        countryPickerView.font = .systemFont(ofSize: 15)
        countryPickerView.delegate = self
        countryPickerView.dataSource = self
        countryPickerView.hostViewController = self
        //setting spectific country and code in picker view
//        countryPickerView.setCountryByName("Nigeria")
        countryPickerView.setCountryByCode("US")
//        countryPickerView.setCountryByPhoneCode("+1")
        countryCodeLbl.text = countryPickerView.selectedCountry.phoneCode
    }
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        phoneNumberTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        phoneNumberTextField.resignFirstResponder()
    }
    
    //MARK:- Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == phoneNumberTextField
        {
            if countryCodeLbl.text == "+1" && phoneNumberTextField.text == ""{
                phoneNumberTextField.placeholder = "(123) 456-7890"
            }else if countryCodeLbl.text == "+91" && phoneNumberTextField.text == ""{
                phoneNumberTextField.placeholder = "1234567890"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        
        if textField == phoneNumberTextField
        {
            if countryCodeLbl.text == "+1"{
                code = "+1"
                print("usa selected")
                
                return RegisterVC.checkEnglishPhoneNumberFormat(string: string, str: str, phoneNumber: phoneNumberTextField)
            }
            else{
                phoneNumberTextField.placeholder = "1234567890"
                return RegisterVC.checkindianPhoneNumberFormat(string: string, str: str, phoneNumber: phoneNumberTextField)
            }
            
            
        }
        else
        {
            return true
        }
    }
    
    
    @IBAction func btnContinueAction(_ sender: UIButton) {
        
        if phoneNumberTextField.text?.count == 0 {
            print("Enter a number")
            showDefaultAlert(viewController: self, title: "Message", msg: "Please enter phone number")
        }
        else{
            if countryCodeLbl.text == "+91"{
                let finalnumber = "\(countryCodeLbl.text ?? "")\(phoneNumberTextField.text ?? "")"
                print("final number is-",finalnumber)
                
                if finalnumber.count == 13
                {
                    print(finalnumber,"indian number is valid ")
                    checkphonenumber()
                    // loginapi()
                    
                }
                else{
                    print(finalnumber,"indian number is invalid ")
                    showDefaultAlert(viewController: self, title: "Message", msg: "Phone number is invalid")
                }
            }
            else{
                checkphonenumber()
                //  loginapi()
            }
            
            
        }
        
    }
    
    func navigator(){
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "PhoneVerificationVC") as! PhoneVerificationVC
        vc.getMobileNumber = code+phoneNumberTextField.text!
        vc.getcode = code
        vc.getphNo = phoneNumberTextField.text!
        providersignupmob = phoneNumberTextField.text!
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}


extension RegisterVC:CountryPickerViewDelegate,CountryPickerViewDataSource{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        code = country.phoneCode
        countryCodeLbl.text = code
        if code  == "+1"{
            phoneNumberTextField.placeholder = "(123) 456-7890"
        }else if code == "+91"{
            phoneNumberTextField.placeholder = "1234567890"
        }
        
    }
    static func checkEnglishPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
    {
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.count < 3{
            
            if str!.count == 1{
                
                phoneNumber.text = "("
            }
            
        }else if str!.count == 5{
            
            phoneNumber.text = phoneNumber.text! + ") "
            
        }else if str!.count == 10{
            
            phoneNumber.text = phoneNumber.text! + "-"
            
        }else if str!.count > 14{
            
            return false
        }
        
        return true
    }
    //
    static func checkindianPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
    {
        
        if string == ""{ //BackSpace
            
            return true
            
        }else if str!.count > 10{
            
            return false
        }
        
        return true
    }
}

extension RegisterVC{
    //MARK: - API Call
    // checkphonenumberapi api
    func checkphonenumber(){
        showActivityIndicator()
        if isNetworkReachable {
            let phoneNumber = phoneNumberTextField.text ?? ""
            // print("mob number =",phoneNumber)
            
            let params = ["phoneNumber":phoneNumber,"countryCode":code] as [String : Any]
            print("Params = \(params)")
            let url = "http://13.234.177.61/api7/checkPhoneNumber"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: nil).validate(statusCode: 200..<500) .responseJSON { response in
                print("RESPONSE FROM SERVER CHECKPHONE NUMBER API -",response)
                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        
                        self.hideActivityIndicator()
                        if responsedata["message"] as! String != "Verification is sent" {
                            if responsedata["message"] as! String == "User already exist.Please signin" {
                                self.loginapi()
                            }
                            else{
                                print("responsedata from checkphonenumber api = ",responsedata["message"] ?? "")
                                //navigation to error msg vc
                                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                let vc = storyboard.instantiateViewController(identifier: "ErrorMsgVC") as! ErrorMsgVC
                                self.navigationController?.pushViewController(vc, animated: true)
                                
                            }
                            
                            
                        }
                        else{
                            self.getchecknumberdetails = getchecknumberResponse(from:responsedata)
                            print("Tocken = ",self.getchecknumberdetails?.token ?? "")
                            print("status = ",self.getchecknumberdetails?.status ?? false)
                            print("fullname = ",self.getchecknumberdetails?.Data?.fullName ?? "")
                            print("type = ",self.getchecknumberdetails?.Data?.type ?? "")
                            print("country code",self.getchecknumberdetails?.Data?.refCountryCode ?? "")
                            print("referer name = \(self.getchecknumberdetails?.Referer?.userId?.fullName ?? "")")
                            print("tip status of general page = \(self.getchecknumberdetails?.Data?.tipstatus?.contactswipe ?? false)")
                            print("tip status of provider page = \(self.getchecknumberdetails?.Data?.tipstatus?.contactswipeprovider ?? false)")
                            
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.tipstatus?.contactswipe ?? false, forKey: "generalTipStatus")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.tipstatus?.contactswipeprovider ?? false, forKey: "providerTipStatus")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Referer?.userId?.fullName ?? "", forKey: "RefererName")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.token, forKey: "Ktoken")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.id, forKey: "Kuserid")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.fullName, forKey: "Kname")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.type, forKey: "Ktype")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.refCountryCode ?? "", forKey: "Kcountrycode")
                            UserDefaults.standard.setValue(self.getchecknumberdetails?.Data?.categoryIds.map({ $0.id }), forKey: "selectedIDs")
                            UserDefaults.standard.setValue(self.phoneNumberTextField.text!, forKey: "mynumber")
                            
                            
//                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
//                            alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                                
                                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                let vc = storyboard.instantiateViewController(identifier: "PhoneVerificationVC") as! PhoneVerificationVC
                                vc.getMobileNumber = self.code+self.phoneNumberTextField.text!
                                vc.getcode = self.code
                                vc.getphNo = self.phoneNumberTextField.text!
                                providersignupmob = self.phoneNumberTextField.text!
                                self.navigationController?.pushViewController(vc, animated: true)
//                            }))
//                            self.present(alert, animated: true)
                            
                            
                            
                            //showDefaultAlert(viewController: self, msg: "\(responsedata["message"] ?? "")")
                            //    self.showToast(message: "\(responsedata["message"] ?? "")", font: .systemFont(ofSize: 13.0))
                            
                            
                            
                        }
                        
                    }
                    
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                    showDefaultAlert(viewController: self, msg: "something has happend, try again")
                }
                
                
            }
            
        }
    }
    
    
    // login api
    func loginapi(){
        if isNetworkReachable {
            let phoneNumber = phoneNumberTextField.text ?? ""
            print("mob number =",phoneNumber)
            
            let params = ["phoneNumber": phoneNumber,"countryCode" :code] as [String : Any]
            
            let url = "http://13.234.177.61/api7/login"
            AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: nil).validate(statusCode: 200..<510) .responseJSON { response in
                
                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata from login api -",responsedata)
                        if responsedata["status"] as! Bool == true {
                            print("responsedata from login api when status true = ",responsedata["message"] ?? "")
                            if responsedata["token"] as! String != "" {
                                UserDefaults.standard.setValue("true", forKey: "loggedin")
                                loggedin = true
                                self.getuserdetails = loginapiResponseModel(from:responsedata)
                                //
                                
                                print("tip status of general page = \(self.getuserdetails?.Data?.tipstatus?.contactswipe ?? false)")
                                print("tip status of provider page = \(self.getuserdetails?.Data?.tipstatus?.contactswipeprovider ?? false)")
                                
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.tipstatus?.contactswipe ?? false, forKey: "generalTipStatus")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.tipstatus?.contactswipeprovider ?? false, forKey: "providerTipStatus")
                                
                                UserDefaults.standard.setValue(self.getuserdetails?.Tocken, forKey: "Ktoken")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.id, forKey: "Kuserid")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.fullName, forKey: "Kname")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.type, forKey: "Ktype")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.profilePic, forKey: "Kprofilepic")
                                UserDefaults.standard.setValue(self.phoneNumberTextField.text!, forKey: "mynumber")
                                
                                        let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                        let vc = storyboard.instantiateViewController(identifier: "PhoneVerificationVC") as! PhoneVerificationVC
                                        vc.getMobileNumber = self.code+self.phoneNumberTextField.text!
                                        vc.getcode = self.code
                                        vc.getphNo = self.phoneNumberTextField.text!
                                        providersignupmob = self.phoneNumberTextField.text!
                                        alreadyloggedin = true
                                        self.navigationController?.pushViewController(vc, animated: true)
                           
                            }
                            else{
                                showDefaultAlert(viewController: self, msg: "\(responsedata["message"] ?? "")")
                                print("responsedata from login api",responsedata["message"] ?? "")
                            }
                            
                        }else if responsedata["status"] as! Bool == false {
                            if responsedata["message"] as? String ?? ""  == "Disabled user"{
                                self.getuserdetails = loginapiResponseModel(from:responsedata)
                                //
                                print("tip status of general page = \(self.getuserdetails?.Data?.tipstatus?.contactswipe ?? false)")
                                print("tip status of provider page = \(self.getuserdetails?.Data?.tipstatus?.contactswipeprovider ?? false)")
                                
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.tipstatus?.contactswipe ?? false, forKey: "generalTipStatus")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.tipstatus?.contactswipeprovider ?? false, forKey: "providerTipStatus")
                                
                                UserDefaults.standard.setValue(self.getuserdetails?.Tocken, forKey: "Ktoken")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.id, forKey: "Kuserid")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.fullName, forKey: "Kname")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.type, forKey: "Ktype")
                                UserDefaults.standard.setValue(self.getuserdetails?.Data?.profilePic, forKey: "Kprofilepic")
                                UserDefaults.standard.setValue(self.phoneNumberTextField.text!, forKey: "mynumber")
                                //
                                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                let vc = storyboard.instantiateViewController(identifier: "PhoneVerificationVC") as! PhoneVerificationVC
                                vc.getMobileNumber = self.code+self.phoneNumberTextField.text!
                                vc.getcode = self.code
                                vc.fromdisabled = true
                                vc.getphNo = self.phoneNumberTextField.text!
                                providersignupmob = self.phoneNumberTextField.text!
                                alreadyloggedin = true
                                loggedin = true
                                self.navigationController?.pushViewController(vc, animated: true)
                            }
                        }
                        else{
                            print("responsedata from login api = ",responsedata["message"] ?? "")
                            //navigation to error msg vc
                            let storyboard = UIStoryboard(name: "Login", bundle: nil)
                            let vc = storyboard.instantiateViewController(identifier: "ErrorMsgVC") as! ErrorMsgVC
                            self.navigationController?.pushViewController(vc, animated: true)
                            //
                        }
                        
                    }
                    
                    if self.getuserdetails?.Tocken != nil{
                        print("Tocken = ",self.getuserdetails?.Tocken ?? "")
                        print("status = ",self.getuserdetails?.Status ?? false)
                        print("fullname = ",self.getuserdetails?.Data?.fullName ?? "")
                        print("type = ",self.getuserdetails?.Data?.type ?? "")
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                    showDefaultAlert(viewController: self, msg: "something has happend, try again")
                }
                
                
            }
            
        }
    }
}


extension RegisterVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}
