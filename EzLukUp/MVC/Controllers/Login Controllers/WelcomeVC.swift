//
//  WelcomeVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/10/22.
//

import UIKit

class WelcomeVC: UIViewController {

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var roundView: UIView!
    @IBOutlet weak var startBtn: UIButton!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var Lsrollview: UIScrollView!
    @IBOutlet weak var versionLBL: UILabel!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        versionLBL.text = "v \(Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "")"

     //   setupUI()
        startBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        titleLbl.textColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
      }
    
    func setupUI(){
        let attributestring1 = NSMutableAttributedString(string: "connecting the dots with contacts you ")
        var attributestring2 = NSMutableAttributedString(string: " trust")
        attributestring2 = NSMutableAttributedString(string: "trust", attributes: [NSAttributedString.Key.foregroundColor: #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)])
        attributestring1.append(attributestring2)
        textLabel.attributedText = attributestring1
        textLabel.font = UIFont(name: "Jost-Regular", size: 17)
    }
    
    override func viewDidLayoutSubviews() {
        self.roundView.layer.cornerRadius = 55
        self.viewBg.roundcornerselected(corners: [.topLeft, .topRight], radius: 16)
    }
    
    //MARK- Action
    @IBAction func btnStartedTapped(_ sender: UIButton) {

            let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "RegisterVC") as! RegisterVC
//        PageViewController - old navigation
            self.navigationController?.pushViewController(vc, animated: true)
  
    }
}
extension UIView {

    func roundcornerselected(corners:UIRectCorner, radius: CGFloat) {

        DispatchQueue.main.async {
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: corners,
                                    cornerRadii: CGSize(width: radius, height: radius))
            let maskLayer = CAShapeLayer()
            maskLayer.frame = self.bounds
            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
        }
    }
}
