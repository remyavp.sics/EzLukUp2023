//
//  PageViewController.swift
//  EzLukUp
//
//  Created by REMYA V P on 04/11/22.
//

import UIKit

class PageViewController: UIPageViewController,UIPageViewControllerDelegate,UIPageViewControllerDataSource{
    
    
    lazy var NewViewControllers: [UIViewController] = {
        return [self.newVC(viewController: "ConSliderFirst"),
                self.newVC(viewController: "ConSliderSecond"),
                self.newVC(viewController: "ConSliderThird"),
                self.newVC(viewController: "ConSliderFourth")]
    }()
    
    var index = 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.dataSource = self

         if let firstViewController = NewViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("nextslide"), object: nil)
        
    }
    
    @objc func onContinueClicked(notification: Notification) {
        print(index)
        self.index = index+1
          
        if index >= 4{
            startlogin()
        }else{
            let ViewController = NewViewControllers[index]
            self.setViewControllers([ViewController], direction: .forward, animated: false, completion: nil)
            
        }
    }

    func  startlogin(){
//        let storyboard = UIStoryboard(name: "Login", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "RegisterVC") as! RegisterVC
//        self.navigationController?.pushViewController(vc, animated: true)
        
        //
                            var type : String = UserDefaults.standard.value(forKey: "Ktype") as! String
                            if type == "Personal"{
                                let storyboard = UIStoryboard(name: "Login", bundle: nil)
                                               let vc = storyboard.instantiateViewController(identifier: "SetupVC") as! SetupVC
//                                                   vc.getmobNum = self.getphNo
//                                                   vc.getCode = self.getcode
                                                   self.navigationController?.pushViewController(vc, animated: true)
                           }else {
                               let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                              let vc = storyboard.instantiateViewController(identifier: "ProviderSetupVC") as! ProviderSetupVC
//                                                  vc.getmobNum = self.getphNo
//                                                  vc.getCode = self.getcode
                                                  self.navigationController?.pushViewController(vc, animated: true)
                           }
        //
    }
    
    
    func newVC(viewController:String) -> UIViewController{
        return UIStoryboard(name: "Login", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = NewViewControllers.index(of: viewController)
        
        else {
            return nil
        }
       
        let previousIndex = viewControllerIndex - 1
        index = viewControllerIndex
        guard previousIndex >= 0 else {
            return NewViewControllers.last
        }
        
        guard NewViewControllers.count > previousIndex else{
            return nil
        }
        
        return NewViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = NewViewControllers.index(of: viewController) else {
            return nil
        }
        index = viewControllerIndex
        let nextIndex = viewControllerIndex + 1
        
        guard NewViewControllers.count != nextIndex else{
//            return NewViewControllers.first
            return nil
        }
        
        guard NewViewControllers.count > nextIndex else{
            return nil
        }
        
        return NewViewControllers[nextIndex]
    }
    

}



