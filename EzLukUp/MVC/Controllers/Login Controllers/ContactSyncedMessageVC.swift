//
//  ContactSyncedMessageVC.swift
//  EzLukUp
//
//  Created by Srishti on 12/10/22.
//

import UIKit
import Contacts
import ContactsUI
import Alamofire
import SVProgressHUD

class ContactSyncedMessageVC: UIViewController {
    
    @IBOutlet weak var viewOfCongrats: UIView!
    @IBOutlet var contactSyncViewPopup: UIView!
    @IBOutlet weak var btnCancel: UIButton!
    @IBOutlet weak var btnOkay: UIButton!
    @IBOutlet weak var messageLbl: UILabel!
    @IBOutlet weak var continueBtn: BaseButton!
    @IBOutlet weak var congrats: UILabel!
    @IBOutlet weak var refernameLBL: UILabel!
    
    var refererName = UserDefaults.standard.value(forKey: "RefererName") ?? ""
    var blurView : UIView!
    let kScreenWidth = UIScreen.main.bounds.width
    let kScreenHeight = UIScreen.main.bounds.height
    let store = CNContactStore()
    var syncContact : Bool = false
    var contacDictionary = [[String:Any]]()
    var tempArr : [[String:Any]] = []
    let uploadCount = 1500
    var contactParam = [[String:Any]]()
    var uploadIsFaild = 3
    var activityView: UIActivityIndicatorView?
    var contactsCount = 0
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refernameLBL.text = "You have been invited by \(self.refererName) to this platform so that we all can collaborate to find a Service Providers via Simple, Efficient, Reliable and Trusted mechanism."
        self.viewOfCongrats.isHidden = false
       
        congrats.isHidden = true
        
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    func getContactList(){
        self.contacDictionary.removeAll()
        let predicate = CNContact.predicateForContactsInContainer(withIdentifier:store.defaultContainerIdentifier())
        let contactz = try! store.unifiedContacts(matching: predicate, keysToFetch: [CNContactGivenNameKey as
                                                                                     CNKeyDescriptor,CNContactFamilyNameKey as CNKeyDescriptor ,CNContactPhoneNumbersKey as CNKeyDescriptor])
        contactsCount = contactz.count
        
        for contact in contactz {
            let name = "\(contact.givenName) \(contact.familyName)"
            var numbers: [String] = []
            for ph in contact.phoneNumbers{
                numbers.append(ph.value.stringValue)
            }
            let dictionaryObject: [String : Any] = [ "name": name, "phoneNumber": numbers]
            self.contacDictionary.append(dictionaryObject)
        }
       // print("before sorting",contacDictionary)
        let sortedArray = contacDictionary.sorted { ($0["name"] as! String) < ($1["name"] as! String) }
        //print("after sorting",sortedArray)
        DispatchQueue.main.async {
            self.viewOfCongrats.isHidden = false
        }
        self.tempArr = sortedArray
//        DispatchQueue.global(qos: .background).async {
            self.syncAPIParameter()
//            DispatchQueue.main.async {
//                // Update the UI here
//            }
//        }
        
//        DispatchQueue.main.async {
//            print("Total of ",contactz.count," contacts found in your Phone list")
//            self.messageLbl.text = "Please wait while we finish syncing \(contactz.count) contacts from your phone book"
//        }
    }
    
    
    func syncAPIParameter() {
        self.uploadIsFaild = 3
        self.contactParam.removeAll()
        
        if self.tempArr.isEmpty { return }
        
        if self.tempArr.count <= self.uploadCount {
            self.contactParam = self.tempArr
            self.tempArr.removeAll()
            //  SVProgressHUD.dismiss()
            
            //                    DispatchQueue.main.async {
            //                        print("Total of ",self.contacDictionary.count,"has been synced")
            //                        self.messageLbl.text = "We have synced \(self.contacDictionary.count) contacts from your phone book"
            //                    }
        } else {
            self.contactParam = Array(self.tempArr.prefix(self.uploadCount))
            self.tempArr = Array(self.tempArr.dropFirst(self.uploadCount))
        }
        self.syncAPI(contacDictionary: self.contactParam)
    }
    
    //MARK: - Api call
    func syncAPI(contacDictionary: [[String:Any]] ){
        
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
        let countrycode : String = UserDefaults.standard.value(forKey: "Kcountrycode") as! String
        let params: [String : Any] = [
            "userId": userid,
            "countryCode": countrycode,
            "syncdata": contacDictionary
        ]
        print("params",params)
//        print("contactParam:",contactParam)
        let url = "http://13.234.177.61/api7/syncContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            //             print("params:\(params)")
            //             print("response:\(response)")
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    syncContact = true
                    // print("syncContact :",syncContact)
                    DispatchQueue.main.async {
                        self.syncAPIParameter()
                    }
                    
                    
                    print("responsedata :",responsedata)
                    if responsedata["message"] as? String == "Contact sync sucessfully"{
                        //                        hideActivityIndicator()
                        //                        congrats.isHidden = false
                        //                        continueBtn.isUserInteractionEnabled = true
                        //                        continueBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
                        //                        DispatchQueue.main.async {
                        //                            self.messageLbl.text = "We have synced your \(contactsCount) contacts from your phone book"
                        //                        }
                        //                        showDefaultAlert(viewController: self, msg: responsedata["message"] as? String ?? "")
                    }
                    else{
                        hideActivityIndicator()
//                        showDefaultAlert(viewController: self, msg: responsedata["message"] as? String ?? "")
                    }
                }
            case .failure(let error):
                if self.uploadIsFaild > 0 {
                    self.syncAPI(contacDictionary: self.contactParam)
                }
                self.uploadIsFaild -= 1
                showDefaultAlert(viewController: self, msg: "Request error: \(error.localizedDescription)")
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    override func viewDidLayoutSubviews() {
        self.contactSyncViewPopup.roundCorners(corners: [.topLeft, .topRight, .bottomRight, .bottomLeft], radius: 14)
        
    }
    
    @available(iOS 14.0, *)
    @objc func autopush(){
        timer.invalidate()
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "PageViewController") as! PageViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @available(iOS 14.0, *)
    @IBAction func btnContinueAction(_ sender: UIButton) {
        //
        DispatchQueue.main.async {
            self.requestAccess { accessGranted in
                print("contact request --",accessGranted)
                if accessGranted{
                    DispatchQueue.global(qos: .background).async {
                        self.getContactList()
                        DispatchQueue.main.async {
                            self.timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.autopush), userInfo: nil, repeats: false)
                        }
                    }
                    
                    
                }
                else{
                    let storyboard = UIStoryboard(name: "Login", bundle: nil)
                    let vc = storyboard.instantiateViewController(identifier: "ContactAlertVC") as! ContactAlertVC
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
        }
        //
        let storyboard = UIStoryboard(name: "Home", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
        //        self.navigationController?.pushViewController(vc, animated: true)
    }
    //MARK: - contact authorization
    func requestAccess(completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        switch CNContactStore.authorizationStatus(for: .contacts) {
        case .authorized:
            completionHandler(true)
        case .denied:
            self.showSettingsAlert(completionHandler)
        case .restricted, .notDetermined:
            self.store.requestAccess(for: .contacts) { granted, error in
                if granted {
                    completionHandler(true)
                } else {
                    DispatchQueue.main.async {
                        self.showSettingsAlert(completionHandler)
                    }
                }
            }
        }
    }
    //MARK: - private alert for granting access again from settings
    private func showSettingsAlert(_ completionHandler: @escaping (_ accessGranted: Bool) -> Void) {
        let alert = UIAlertController(title: nil, message: "This app requires access to Contacts to proceed. Go to Settings to grant access.", preferredStyle: .alert)
        if
            let settings = URL(string: UIApplication.openSettingsURLString),
            UIApplication.shared.canOpenURL(settings) {
            alert.addAction(UIAlertAction(title: "Open Settings", style: .default) { action in
                completionHandler(false)
                UIApplication.shared.open(settings)
            })
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            completionHandler(false)
        })
        present(alert, animated: true)
    }
    
    
}

//MARK: - @IBOutlet Actions
extension ContactSyncedMessageVC {
    
    @IBAction func btnCancelTap(_ sender: UIButton) {
        // didmissContactsyncPopUp()
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ContactAlertVC") as! ContactAlertVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnOkayTap(_ sender: UIButton) {
        didmissContactsyncPopUp()
        self.viewOfCongrats.isHidden = false
    }
    
}

//MARK: - ContactSyncPopUpSettings
extension ContactSyncedMessageVC {
    
    @objc func showContactSyncPopUp(){
        blurView = UIView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height:  kScreenHeight))
        blurView.backgroundColor = .black.withAlphaComponent(0.2)
        self.view.addSubview(blurView)
        self.contactSyncViewPopup.frame = CGRect(x: (kScreenWidth - 272) / 2, y: (kScreenHeight - 200) / 2, width: 273, height: 172)
        self.blurView.addSubview(self.contactSyncViewPopup)
        self.blurView.bringSubviewToFront(self.contactSyncViewPopup)
        self.contactSyncViewPopup.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        UIView.animate(withDuration: 0.33, animations: {
            self.contactSyncViewPopup.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    
    func didmissContactsyncPopUp() {
        if blurView != nil {
            UIView.animate(withDuration: 0.33, animations: {
                self.blurView.alpha = 0
            }, completion: { (completed) in
            })
            UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            }, completion: { (completed) in
                self.blurView.gestureRecognizers?.removeAll()
                self.blurView.removeFromSuperview()
                self.blurView = nil
            })
        }
    }
}

extension ContactSyncedMessageVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}
