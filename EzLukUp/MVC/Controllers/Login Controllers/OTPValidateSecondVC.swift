//
//  OTPValidateSecondVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 10/10/22.
//

import UIKit

class OTPValidateSecondVC: UIViewController {

    @IBOutlet weak var firstdigit: UITextField!
    @IBOutlet weak var seconddigit: UITextField!
    @IBOutlet weak var thirddigit: UITextField!
    @IBOutlet weak var fourthdigit: UITextField!
    @IBOutlet weak var btnReceiveCode: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.firstdigit.addCornerForView(cornerRadius: 14.0)
        self.seconddigit.addCornerForView(cornerRadius: 14.0)
        self.thirddigit.addCornerForView(cornerRadius: 14.0)
        self.fourthdigit.addCornerForView(cornerRadius: 14.0)
       
    }
    
    @IBAction func btnReceiveCodeAction(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Login", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "SetupVC") as! SetupVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    

}
