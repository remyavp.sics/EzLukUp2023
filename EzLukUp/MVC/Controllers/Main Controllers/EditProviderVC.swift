//
//  EditProviderVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 28/11/22.
//

import UIKit
import Alamofire
import SwiftUI

var Search = false
var SelectedCategoryID = [String]()
var SearcharrayRes = [getcatDataModel]()
var selectedCategories = [String]()
var catnamearray = [String]()
var NewCatArray = [getcatDataModel]()
var Editcategorylist : getCategoriesResponse?
//var typedfeedback = ""
var typedname = ""
var typedAbout = ""
var typedcountry = ""
var typedmobile = ""
//var getmobile = ""
//var getcountry = ""
//var getname = ""

protocol Catdeletearraydelegate {
    func diddelete(index:Int)
}

class EditProviderVC: UIViewController, Catdeletearraydelegate {
    func diddelete(index: Int) {
        let newindex = NewCatArray.firstIndex{$0.name == selectedCategories[index]}
        NewCatArray[newindex ?? 0].isselected = false
        selectedCategories.remove(at: index)
                   categoryPopupTV.reloadData()
                   editProviderTV.reloadData()
    }
    
    
//MARK:- Outlets
    @IBOutlet weak var categoryPopupTV: UITableView!
    @IBOutlet weak var editProviderTV: UITableView!
    @IBOutlet var categoryPopupView: BaseView!
    
    
    var editproviderlist : editProviderResponse?
    var editlist : EditProviderContactDetailsResponse?
    var items = [String]()
    var checkedbtnstatus : Bool = false
    var dropstatus = false
    var activityView: UIActivityIndicatorView?
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
   
    
    var getmobile = ""
    var getcountry = ""
    var getname = ""
    var getcontactid = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getcategoryapi()
        self.getcontactsapi()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("reloadcat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ontrue(notification:)), name: Notification.Name("true"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfalse(notification:)), name: Notification.Name("false"), object: nil)
       
    }
  
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = categoryPopupTV.indexPathForSelectedRow {
            categoryPopupTV.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.categoryPopupView.roundCorners(corners: [.topLeft,.topRight, .bottomLeft, .bottomRight], radius: 15)
    }
    
    
    @objc func onContinueClicked(notification: Notification) {
        categoryPopupTV.reloadData()
    }
    
    @objc func ontrue(notification: Notification) {
        Search = true
    }
    
    @objc func onfalse(notification: Notification) {
        Search = false
    }
    
    @IBAction func dropdownBTNTapped(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.editProviderTV.reloadSections([1], with: .none)
    }
}


//MARK: - @IBActions
extension EditProviderVC {
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)

    }
    
   
    
      @IBAction func checkmarkBTNTapped(_ sender: UIButton) {
          let tcell = editProviderTV.cellForRow(at: IndexPath(row: 0, section: 2)) as! EditThirdCell
         
          if checkedbtnstatus == false {
              checkedbtnstatus = true
              tcell.checkboxIMG.image = UIImage(named: "selectcheckbox")
          }else{
              checkedbtnstatus = false
              tcell.checkboxIMG.image = UIImage(named: "unselectcheckbox")
          }
      }
      
      @IBAction func saveBTNTapped(_ sender: UIButton) {
          editprovidercontactapi()
      }
      
      @IBAction func deleteBTNTapped(_ sender: UIButton) {
          deleteprovidercontactapi()
      }
      
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension EditProviderVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == editProviderTV{
            return 3
        }
        else{
            return 1
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == editProviderTV{
            if section == 0{
                return 1
            }else if section == 1{
                return 1
            }else if section == 2{
                return 1
            }else{
                return 0
            }
        }else{
            if( Search == true){
                return SearcharrayRes.count
            }else{
                return NewCatArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == editProviderTV{
            if indexPath.section == 0{
                let cell = editProviderTV.dequeueReusableCell(withIdentifier: "EditFirstCell", for: indexPath) as! EditFirstCell
                cell.nameTextField.text = self.editlist?.Data?.name
                return cell
            }
           else if indexPath.section == 1{
                let cell = editProviderTV.dequeueReusableCell(withIdentifier: "EditProviderTableCell", for: indexPath) as! EditProviderTableCell
                cell.delegate = self
               print("cat id count = ",self.editlist?.Data?.categoryIds.count ?? 0)
               print("cat ids = ",self.editlist?.Data?.categoryIds)
//               let tagcount = self.editlist?.Data?.categoryIds.count ?? 0
//               for i in 0...tagcount{
//                   print("cat name = ",self.editlist?.Data?.categoryIds[i].name ?? "")
//               }
//               let taggedcat = self.editlist?.Data?.categoryIds[0].name
               print("selected count ===",selectedCategories.count)
                cell.selectedArray = selectedCategories
                cell.categorytableview.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    if self.dropstatus {
                       
                        cell.searchview.isHidden = false
                        cell.searchviewheight.constant = 40
                        cell.dropviewheight.constant = 500
                        cell.dropdownview.isHidden = false
                        cell.dropdownIMG.image = UIImage(named: "dropup_icon")
                        cell.dropdownview.addSubview(self.categoryPopupView)
                       
                    }
                    else{
                        
                        cell.dropviewheight.constant = 0
                        cell.searchviewheight.constant = 0
                        cell.searchview.isHidden = true
                        cell.dropdownview.isHidden = true
                        cell.dropdownIMG.image = UIImage(named: "dropdown_icon")
                       
                    }
                    if selectedCategories.count == 1{
                        if self.dropstatus {
                            cell.categoryviewheight.constant = 85

                        }else {
                            cell.categoryviewheight.constant = CGFloat(cell.categorytableview.contentSize.height + 11 )
                        }
                    }
                    else if selectedCategories.count > 1{
                        if self.dropstatus{
                            cell.categoryviewheight.constant = CGFloat(cell.categorytableview.contentSize.height + 40 + 11)
                        }
                        else{
                            cell.categoryviewheight.constant = CGFloat(cell.categorytableview.contentSize.height + 11)
                        }
                    }
                        else{
                            
                            cell.categoryviewheight.constant = 52
                            
                        }
                    
                }
                )
                return cell
            }else{
            let cell = editProviderTV.dequeueReusableCell(withIdentifier: "EditThirdCell", for: indexPath) as! EditThirdCell
                cell.mobileTextField.text = editlist?.Data?.phoneNumber ?? ""
                cell.countrycodeLbl.text = editlist?.Data?.countryCode ?? ""
                cell.aboutTextView.text = editlist?.Feedback?.feedback ?? ""
            return cell
        }

    }
        
    //MARK: - popup category tableview cellforrowat
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditProviderCatPopUpTVCell") as! EditProviderCatPopUpTVCell
        print("status",Search)
        cell.categoryLbl.text = SearcharrayRes[indexPath.row].name
            cell.categoryLbl.textColor = SearcharrayRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == editProviderTV{
            if indexPath.section == 1{
                if selectedCategories.count == 1{
                                   if self.dropstatus {
                                       return 90 + 9 + 500
               
                                   }else {
//                                      return CGFloat((selectedCategories.count * 32) + 11 + 11)
                                       return UITableView.automaticDimension
                                   }
                               }
                               else if selectedCategories.count > 1{
                                   if self.dropstatus{
                                       return CGFloat((selectedCategories.count * 32) + 40 + 11 + 9 + 500)
                                   }
                                   else{
                                       return CGFloat((selectedCategories.count * 32) + 11 + 11)
               //                        return UITableView.automaticDimension
                                   }
                               }
                                   else{
                                       if self.dropstatus {
                                           return 52 + 500
                                       }
                                       return 52
               
                                   }
                           
            }
            return UITableView.automaticDimension
        }else{
            return 45
        }
    }
        
    
 //MARK: - category tableview didSelect & didDeselect
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categoryPopupTV{
            if let index = SearcharrayRes.firstIndex(where: { $0.isselected  }) {
                SearcharrayRes[index].isselected = false
            }
            SearcharrayRes[indexPath.row].isselected = !SearcharrayRes[indexPath.row].isselected
            let index = NewCatArray.firstIndex(where: { $0.id ==  SearcharrayRes[indexPath.row].id }) ?? 0
            NewCatArray[index].isselected = SearcharrayRes[indexPath.row].isselected
            selectedCategories = NewCatArray.filter{$0.isselected}.map{$0.name ?? ""}
            SelectedCategoryID = NewCatArray.filter{$0.isselected}.map{$0.id ?? ""}
            categoryPopupTV.reloadData()
            editProviderTV.reloadData()
           
        }
        else{
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == categoryPopupTV{
            
        }
    }
    
}


extension EditProviderVC{
    //MARK: - getcontactsapi
    func getcontactsapi() {
    
        let params: [String : Any] = ["contactid": getcontactid]
        
        let url = "http://13.234.177.61/api7/getContactDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.editlist = EditProviderContactDetailsResponse(from:responsedata)
                        UserDefaults.standard.setValue(self.editlist?.Data?.categoryIds.map({ $0.id }), forKey: "selectedIDsInedit")
                        self.editProviderTV.reloadData()
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
           }
     }
    
    //MARK: - category api
        func getcategoryapi() {
         showActivityIndicator()

            let url = "http://13.234.177.61/api7/getCategories"
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                hideActivityIndicator()
                                Editcategorylist = getCategoriesResponse(from: responsedata)
                               
                                NewCatArray = Editcategorylist?.Data ?? []
                                DispatchQueue.main.async {
                                   if let categories = UserDefaults.standard.value(forKey: "selectedIDsInedit") as? [String]{
                                       NewCatArray.forEach({ $0.isselected = categories.contains($0.id ?? "") })
                                    selectedCategories = NewCatArray.filter({ $0.isselected }).map({ $0.name ?? "" })
                                       print("cat to be highlated =",selectedCategories)
                                }
                                SearcharrayRes = NewCatArray
                                    print(NewCatArray)
                                    self.categoryPopupTV.reloadData()
//                                    self.editProviderTV.reloadData()
                                    self.editProviderTV.reloadSections([1], with: .none)
                                }
                               }
                   
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
                 }
          }
    
    
//MARK: - API Call
func editprovidercontactapi(){
   
    let params: [String : Any] = [
                  "contactid": getcontactid,
                  "name": typedname,
                  "recommend": checkedbtnstatus,
                  "feedback": typedabout,
                  "categoryids": SelectedCategoryID,
                  "phonenumber": typedmobile,
                  "countrycode": typedcountry
     ]
    print(params)
    
     let url = "http://13.234.177.61/api7/editProviderContact"
     AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
         switch (response.result) {
         case .success( let JSON):
             if let responsedata =  JSON as? [String:Any]  {
               print("responsedata :",responsedata)

                 let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                 alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                     if responsedata["message"] as? String == "Updated sucessfully"{

                         self.navigationController?.popViewController(animated: true)
                         
                       
                     }else{
                         let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                     }
                     }
                                               ))
                         self.present(alert, animated: true)
                 
             }
             case .failure(let error):
                 print("Request error: \(error.localizedDescription)")
         }
     }
}
    
 //MARK: - DELETE API Call
    func deleteprovidercontactapi(){
        
      //  let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let params: [String : Any] = [
                      "contactid": getcontactid ]
        
         let url = "http://13.234.177.61/api7/deleteContact"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             switch (response.result) {
                case .success( let JSON):
                    if let responsedata =  JSON as? [String:Any]  {
                      print("responsedata :",responsedata)
                        let alert = UIAlertController(title: "", message: "\(responsedata["status"])", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                            if responsedata["status"] as? String == "Record removed sucessfully"{
                                
//                                self.navigationController?.popViewController(animated: true)
                                if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
                                        guard viewControllers.count < 3 else {
                                            self.navigationController?.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                                            return
                                        }
                                    guard viewControllers.count < 2 else{
                                        self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                                        return
                                    }
                                    }
                                
                                
                                
                                
                                
                              
                            }else{
                                let alert = UIAlertController(title: "", message: "\(responsedata["status"])", preferredStyle: .alert)
                            }
                            }
                                                      ))
                                self.present(alert, animated: true)

                    }
                    case .failure(let error):
                        print("Request error: \(error.localizedDescription)")
                }
         }
    }
}

extension EditProviderVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}


class Category
{
    var category = ""
    var isselected = false
    init(data:String)
    {
        self.category = data
    }
}

class edituserDetails{
    var name = ""
    var searchtext = ""
    func saveuserdetail(name:String,searchtext:String){
        self.name = name
        self.searchtext = searchtext
    }
    func readuserdtail() -> (String,String){
        return (self.name,self.searchtext)
    }
}


class EditProviderCatPopUpTVCell: UITableViewCell {
    @IBOutlet weak var categoryLbl: UILabel!
}

