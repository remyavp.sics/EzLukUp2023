//
//  FeedbackProviderProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import Alamofire
import Kingfisher

class FeedbackProviderProfileVC: UIViewController {
    
    @IBOutlet weak var feedbacktableview: UITableView!
    @IBOutlet weak var nodataLBL: UILabel!
    
    var getfeedbackdetails : getProviderProfileResponse?
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    var isoformatmodel = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nodataLBL.isHidden = true
       feedbackapi()
    }
    
}


//MARK: - INFO UITableViewDelegate,UITableViewDataSource
extension FeedbackProviderProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.getfeedbackdetails?.Feedback.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getfeedbackdetails?.Feedback.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = feedbacktableview.dequeueReusableCell(withIdentifier: "FeedbackTVCell", for: indexPath) as! FeedbackTVCell
        cell.feedbackLbl.text = self.getfeedbackdetails?.Feedback[indexPath.row].feedback
       
        cell.nameLbl.text = self.getfeedbackdetails?.Feedback[indexPath.row].userId?.fullName
        cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.getfeedbackdetails?.Feedback[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profile icon"))
        
        self.isoformatmodel = self.getfeedbackdetails?.Feedback[indexPath.row].createdAt ?? ""
      //  let daysAgoString = convertDateStringToDaysAgo(dateString: isoformatmodel)
      //  print(daysAgoString)
      //  cell.dayLbl.text = daysAgoString
        let daysagoString = convertisoformat(dateString: isoformatmodel)
          print(daysagoString)
          cell.dayLbl.text = daysagoString
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
   
    func convertisoformat(dateString: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let date = dateFormatter.date(from: isoformatmodel)!
        let timeInterval = date.timeIntervalSinceNow
        
        let secondsAgo = Int(abs(timeInterval))
        let minutesAgo = secondsAgo / 60
        let hoursAgo = minutesAgo / 60
        let daysAgo = hoursAgo / 24
        let weeksAgo = daysAgo / 7
        let monthsAgo = weeksAgo / 4
        let yearsAgo = monthsAgo / 12
        
        if secondsAgo < 60 {
            return "\(secondsAgo) seconds ago"
        } else if minutesAgo < 60 {
            return "\(minutesAgo) minutes ago"
        } else if hoursAgo < 24 {
            return "\(hoursAgo) hours ago"
        } else if daysAgo < 7 {
            return "\(daysAgo) days ago"
        } else if weeksAgo < 4 {
            return "\(weeksAgo) weeks ago"
        } else if monthsAgo < 12 {
            return "\(monthsAgo) months ago"
        } else {
            return "\(yearsAgo) years ago"
        }
    }
    
    
    
    //MARK:- Converting date to daysago
        func convertDateStringToDaysAgo(dateString: String) -> String {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter.locale = Locale(identifier: "en_US_POSIX")
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            
            guard let date = dateFormatter.date(from: dateString) else {
                return ""
            }
            
            let calendar = Calendar.current
            let currentDate = Date()
            let components = calendar.dateComponents([.day], from: date, to: currentDate)
            
            if let days = components.day {
                // today
                if days == 0 {
                    let timeDiffSeconds = Int(currentDate.timeIntervalSince(date))
                    let timeDiffMinutes = timeDiffSeconds / 60

                    // format the output based on the time difference
                    if timeDiffSeconds < 60 {
                        print("\(timeDiffMinutes) minutes ago")
                        return "\(timeDiffSeconds) seconds ago"
                    } else if timeDiffMinutes < 60 {
                        print("\(timeDiffMinutes) minutes ago")
                        return "\(days) days ago"
                    } else {
                        print("more than 1 hour ago")
                        return "1 hour ago"
                    }
                    
                }else {
                    print(days)
                    return "\(days) days ago"
                }
            } else {
                return ""
                
            }
            
        }
    }


extension FeedbackProviderProfileVC{
//MARK: - API Call
func feedbackapi(){
    let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
    
    let params = ["contactid": contactid] as [String : Any]
    
    let url = "http://13.234.177.61/api7/getProviderContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":tokenn]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.getfeedbackdetails = getProviderProfileResponse(from:responsedata)
                    if self.getfeedbackdetails?.Feedback.count == 0{
                        self.nodataLBL.isHidden = false
                    }else{
                        self.nodataLBL.isHidden = true
                    }
                    
                    
                    self.feedbacktableview.reloadData()
                   // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}


//MARK: - TableViewCell
class FeedbackTVCell: UITableViewCell {
    
    @IBOutlet weak var feedbackview: BaseView!
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var feedbackLbl: UILabel!
}
