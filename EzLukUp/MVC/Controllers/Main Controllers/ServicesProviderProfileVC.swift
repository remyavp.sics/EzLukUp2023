//
//  ServicesProviderProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import Alamofire
import SwiftUI
import Kingfisher

class ServicesProviderProfileVC: UIViewController {

    @IBOutlet weak var servicestableview: UITableView!
    
    var getservivedetails : getProviderProfileResponse?
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       servicedetailsapi()
    }
    

}



//MARK: - UITableViewDelegate, UITableViewDataSource
extension ServicesProviderProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.getservivedetails?.Data?.contactUserId?.serviceDetails.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.getservivedetails?.Data?.contactUserId?.serviceDetails.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = servicestableview.dequeueReusableCell(withIdentifier: "ServicesTableCell", for: indexPath) as! ServicesTableCell
        cell.cellLbl1.text = self.getservivedetails?.Data?.contactUserId?.serviceDetails[indexPath.row].description
        cell.cellLbl2.text = self.getservivedetails?.Data?.contactUserId?.serviceDetails[indexPath.row].title
        cell.cellIMG.kf.setImage(with: URL(string: kImageUrl + (self.getservivedetails?.Data?.contactUserId?.serviceDetails[indexPath.row].url ?? "")))
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
}




//MARK: - TableCell
class ServicesTableCell: UITableViewCell {
    
    @IBOutlet weak var cellLbl1: UILabel!
    @IBOutlet weak var cellIMG: UIImageView!
    @IBOutlet weak var cellLbl2: UILabel!
   
}

extension ServicesProviderProfileVC{
//MARK: - API Call
func servicedetailsapi(){
   
    let params = ["contactid": contactid] as [String : Any]
    
    let url = "http://13.234.177.61/api7/getProviderContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.getservivedetails = getProviderProfileResponse(from:responsedata)
//                    var count = getservivedetails?.Data
                    self.servicestableview.reloadData()
                   // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}
