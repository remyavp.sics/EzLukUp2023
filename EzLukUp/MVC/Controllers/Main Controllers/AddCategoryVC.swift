//
//  AddCategoryVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 19/11/22.
//

import UIKit
import Alamofire
//import RegexBuilder

var Searching = false
var selectedcategoryID = [String]()
var searchArrayRes = [getcatDataModel]()
var Selectedcategories = [String]()
var catnameArray = [String]()
var newCatarray = [getcatDataModel]()
var Categorylist : getCategoriesResponse?
var Typedabout = ""

class AddCategoryVC: UIViewController, Categorydeletearraydelegate{
    func diddelete(index: Int) {
        let newindex = newCatarray.firstIndex{$0.name == Selectedcategories[index]}
        newCatarray[newindex ?? 0].isselected = false
        Selectedcategories.remove(at: index)
               categorytableview.reloadData()
               addCategoryTableView.reloadData()
    }
    

 //MARK:- Outlets
    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var categoryLbl: UILabel!
    
    @IBOutlet weak var addCategoryTableView: UITableView!
    @IBOutlet var categorypopupview: BaseView!
    @IBOutlet weak var categorytableview: UITableView!
    
    
    var tagproviderlist : tagProviderResponse?
    var items = [String]()
    var checkedbtnstatus : Bool = false
    var dropstatus = false
    var activityView: UIActivityIndicatorView?
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    var getcontactid = String()
    var getProfileName = String()
    var phNo = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.getcategoryapi()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("reloadcat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ontrue(notification:)), name: Notification.Name("true"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfalse(notification:)), name: Notification.Name("false"), object: nil)
        self.nameLbl.text = getProfileName
        print("selected catid ")
       // self.addCategoryTableView.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = categorytableview.indexPathForSelectedRow {
            categorytableview.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.categorypopupview.roundCorners(corners: [.topLeft,.topRight, .bottomLeft, .bottomRight], radius: 15)
    }
    
    
    @objc func onContinueClicked(notification: Notification) {
        categorytableview.reloadData()
    }
    
    @objc func ontrue(notification: Notification) {
        Searching = true
    }
    
    @objc func onfalse(notification: Notification) {
        Searching = false
    }
  
}

//MARK: - @IBActions
extension AddCategoryVC{
    
    @available(iOS 14.0, *)
    @IBAction func backBTNAction(_ sender: UIButton) {
        selectedcategoryID.removeAll()
        self.getProfileName = ""
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func checkmarkBTNTapped(_ sender: UIButton) {
        let tcell = addCategoryTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as! AddSecondCell
       
       if checkedbtnstatus == false {
           checkedbtnstatus = true
           tcell.checkboxIMG.image = UIImage(named: "selectcheckbox")
       }else{
           checkedbtnstatus = false
           tcell.checkboxIMG.image = UIImage(named: "unselectcheckbox")
       }
    }
    
    @IBAction func dropBTNTapped(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.addCategoryTableView.reloadSections([0], with: .none)
    }

    
    @available(iOS 14.0, *)
    @IBAction func saveBTNTapped(_ sender: UIButton) {
        if selectedcategoryID.count == 0 {
            let alert = UIAlertController(title: "", message: "Please select a category to continue", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "ok", style: .default))
            self.present(alert, animated: true)
            if Typedfeedback == "" {
                let alert = UIAlertController(title: "", message: "Please enter feedback to continue", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "ok", style: .default))
                self.present(alert, animated: true)
            }
           
        }
        else{
            
            tagproviderapi()
//            let text = "This is some text that I want to share."
//
//            // set up activity view controller
//            let textToShare = [ text ]
//            let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
//            activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
//
//            // exclude some activity types from the list (optional)
//            activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
//
//            // present the view controller
//                            self.present(activityViewController, animated: true, completion: nil)
        }
        
    }
    
    @available(iOS 14.0, *)
    @IBAction func topCloseBTNTapped(_ sender: UIButton) {
        selectedcategoryID.removeAll()
        self.getProfileName = ""
        self.navigationController?.popViewController(animated: true)
    }
    
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension AddCategoryVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == addCategoryTableView{
            return 2
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == addCategoryTableView{
            if section == 0{
                return 1
            }else if section == 1{
                return 1
            }else{
                return 0
            }
            
        }else{
            //check search text & original text
//            if searchArrayRes.count == 0{
//
//            }
            if( Searching == true){
                return searchArrayRes.count
            }else if Searching == false && searchArrayRes.count == 0 {
                return 1
            }
            else{
                return newCatarray.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == addCategoryTableView{
            if indexPath.section == 0{
                let cell = addCategoryTableView.dequeueReusableCell(withIdentifier: "AddCategoryTVCell", for: indexPath) as! AddCategoryTVCell
                cell.delegate = self
                cell.selectedArray = Selectedcategories
                cell.categoryTV.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    
                    if self.dropstatus {
                        
                        cell.searchview.isHidden = false
                        cell.searchviewheight.constant = 40
                        cell.dropdownHeight.constant = 500
                        cell.dropdownview.isHidden = false
                        cell.dropdownimg.image = UIImage(named: "dropup_icon")
                        cell.dropdownview.addSubview(self.categorypopupview)
                       
                    }
                    else{
                        cell.searchTextField.text = ""
                        cell.dropdownHeight.constant = 0
                        cell.searchviewheight.constant = 0
                        cell.searchview.isHidden = true
                        cell.dropdownview.isHidden = true
                        cell.dropdownimg.image = UIImage(named: "dropdown_icon")
                       
                    }
                    
                    if Selectedcategories.count == 1{
                        if self.dropstatus {
                            print("drop open h = \( 83 + 8)")
                            cell.categoryviewheight.constant = 83 + 8 //cell.categoryTV.contentSize.height // added 9 to fix the issue with overlapping search view to selected category

                        }else {
                            print("drop close h= \( CGFloat(cell.categoryTV.contentSize.height + 11 ))")
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 11 )
                        }
                    }
                    else if Selectedcategories.count > 1{
                        if self.dropstatus{
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 40 + 11)
                        }
                        else{
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 11)
                        }
                    }
                        else{
                            
                            cell.categoryviewheight.constant = 52
                            
                        }
                    
                }
                )
                return cell
            }else{
                let cell = addCategoryTableView.dequeueReusableCell(withIdentifier: "AddSecondCell", for: indexPath) as! AddSecondCell
                return cell
            }
        }
           
        //MARK: - category tableview cellforrowat
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddCategoryPopupTVCell") as! AddCategoryPopupTVCell
            print("status",Searching)
            if searchArrayRes.count == 0 {
                print("search list contains , \(searchArrayRes.count)")
                print("no of rows in sections = = \(searchArrayRes.count)")
                cell.categorypopupLbl.text = "No category found"
                cell.isExclusiveTouch = false
            }else{
                cell.categorypopupLbl.text = searchArrayRes[indexPath.row].name ?? ""
                cell.categorypopupLbl.textColor = searchArrayRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
            }
            
                
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == addCategoryTableView{
//            if indexPath.section == 0{
//                if Selectedcategories.count == 1{
//                                   if self.dropstatus {
//                                       return 83 + 36 + 500
//
//                                   }else {
//                                      return CGFloat((Selectedcategories.count * 32) + 11 + 11)
////                                       return UITableView.automaticDimension
//                                   }
//                               }
//                               else if Selectedcategories.count > 1{
//                                   if self.dropstatus{
//                                       return CGFloat((Selectedcategories.count * 32) + 40 + 11 + 9 + 500)
//                                   }
//                                   else{
//                                       return CGFloat((Selectedcategories.count * 32) + 11 + 11)
//               //                        return UITableView.automaticDimension
//                                   }
//                               }
//                                   else{
//                                       if self.dropstatus {
//                                           return 52 + 500
//                                       }
//                                       return 52
return UITableView.automaticDimension
//                                   }
            
//            }
//            else{
//                return UITableView.automaticDimension
//            }
               
           
        }else{
            return 45
        }
        
    }
    
    //MARK: - category tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categorytableview{
            if let index = searchArrayRes.firstIndex(where: { $0.isselected  }) {
                searchArrayRes[index].isselected = false 
            }
            
            if searchArrayRes.count == 0{
                
            }else{
                searchArrayRes[indexPath.row].isselected = !searchArrayRes[indexPath.row].isselected
                let index = newCatarray.firstIndex(where: { $0.id ==  searchArrayRes[indexPath.row].id }) ?? 0
                newCatarray[index].isselected = searchArrayRes[indexPath.row].isselected
                Selectedcategories = newCatarray.filter{$0.isselected}.map{$0.name ?? ""}
                selectedcategoryID = newCatarray.filter{$0.isselected}.map{$0.id ?? ""}
//                searchtexter = ""
//                searching = false
//                dropstatus = false
            }
            
            
            
            categorytableview.reloadData()
            addCategoryTableView.reloadData()
        }
        else{
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == categorytableview{
            
        }
    }
}

extension AddCategoryVC{
    
    //MARK: - category api
        func getcategoryapi() {
         showActivityIndicator()
            
//            let ttoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjNiYjllMTU5MDY2YjA1YzQyZGY2YjQzIiwiaWF0IjoxNjczMjQwMTM4fQ.J589WhSiU-dY9heyw-AfaAQi4abDbWADg1C3Hk3RLSc"
            
            
            let url = "http://13.234.177.61/api7/getCategories"
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                hideActivityIndicator()
                                Categorylist = getCategoriesResponse(from: responsedata)
                               
                                newCatarray = Categorylist?.Data ?? []
                                DispatchQueue.main.async {
//                                   if let categories = UserDefaults.standard.value(forKey: "selectedIDs") as? [String]{
//                                    newCatarray.forEach({ $0.isselected = categories.contains($0.id ?? "") })
//                                    Selectedcategories = newcatarray.filter({ $0.isselected }).map({ $0.name ?? "" })
                              //  }
                                searchArrayRes = newCatarray
                                    print(newCatarray)
                                    self.addCategoryTableView.reloadData()
                                }
                               }
                   
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
                 }
          }
    
    
//MARK: - tag provider API Call
    @available(iOS 14.0, *)
    func tagproviderapi(){
    showActivityIndicator()
    let params: [String : Any] = [
                  "id": getcontactid,
                  "isrecommended": checkedbtnstatus ,
                  "feedback": Typedabout,
                  "categoryIds": selectedcategoryID

     ]
    print(params)
 
     let url = "http://13.234.177.61/api7/tagContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    hideActivityIndicator()
                    let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["data"] as? String ?? "" == "Updated sucessfully"{
//                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                                let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
                           selectedBTN = 1
//                                self.navigationController?.pushViewController(vc, animated: true)
                            self.navigationController?.popViewController(animated: true)
                           // self.present(alert, animated: true)
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["data"] ?? "")", preferredStyle: .alert)
                            
                        }
                        
                        }
               
                                                  ))
                    self.present(alert, animated: true)
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
     }
}
}

extension AddCategoryVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}



protocol Categorydeletearraydelegate{
    func diddelete(index:Int)
}

class AddCategoryPopupTVCell: UITableViewCell {
    
    @IBOutlet weak var categorypopupLbl: UILabel!

}

class Cate
{
    var category = ""
    var isselected = false
    init(data:String)
    {
        self.category = data
    }
}

class userDetails{
    var name = ""
    var searchtext = ""
    func saveuserdetail(name:String,searchtext:String){
        self.name = name
        self.searchtext = searchtext
    }
    func readuserdtail() -> (String,String){
        return (self.name,self.searchtext)
    }
}

