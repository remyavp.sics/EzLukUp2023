//
//  AddGeneralContactVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/11/22.
//

import UIKit
import CountryPickerView
import Alamofire
import PhoneNumberKit


class AddGeneralContactVC: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {
    
 //MARK:- Outlets
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var profileIMG: UIImageView!
    
    @IBOutlet weak var flagview: UIView!
    @IBOutlet weak var numberview: UIView!
    @IBOutlet weak var countrypickerview: CountryPickerView!
    @IBOutlet weak var countrypickerLbl: UILabel!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var saveBtn: BaseButton!
    
    var uploadimage : uploadSingleImageResponse?
    var addlist : addGeneralResponse?
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var code = ""
    var phonenumber = ""
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var imageupload = ""
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        countrysetup()
    }
    
    func setUpUI(){
        self.flagview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.flagview.layer.borderWidth = 1
        self.numberview.layer.borderWidth = 1
        self.numberview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.saveBtn.addCornerForView(cornerRadius: 14.0)
        self.flagview.addCornerForView(cornerRadius: 14.0)
        self.numberview.addCornerForView(cornerRadius: 14.0)
        mobileTextField.delegate = self
        saveBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    }
    
    func countrysetup(){
        addDoneButtonOnKeyboard()
        countrypickerview.showPhoneCodeInView = false
        countrypickerview.showCountryCodeInView = true
        countrypickerview.showCountryNameInView = false
        countrypickerview.font = .systemFont(ofSize: 15)
        countrypickerview.delegate = self
        countrypickerview.dataSource = self
        countrypickerLbl.text = countrypickerview.selectedCountry.phoneCode
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        mobileTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        mobileTextField.resignFirstResponder()
    }
    
    //MARK:- Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileTextField
        {
            if countrypickerLbl.text == "+1" && mobileTextField.text == ""{
                mobileTextField.placeholder = "(123) 456-7890"
            }else if countrypickerLbl.text == "+91" && mobileTextField.text == ""{
                mobileTextField.placeholder = "(123) 456-7890"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
               
               if textField == mobileTextField
               {
                   if countrypickerLbl.text == "+1"{
                       print("usa selected")
                       return AddGeneralContactVC.checkEnglishPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }
                   else{
                       mobileTextField.placeholder = "1234567890"
                       return AddGeneralContactVC.checkindianPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }
                   
               }
               else
               {
                   return true
               }
    }
    
   
}


//MARK: - @IBActions
extension AddGeneralContactVC {
    
    @available(iOS 14.0, *)
    @IBAction func backBTNAction(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
//        self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @available(iOS 14.0, *)
    @IBAction func closeBTNTapped(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
//        self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popViewController(animated: true)

    }
    
    
    @available(iOS 14.0, *)
    @IBAction func saveBTNTapped(_ sender: UIButton) {
        addgeneralcontactapi()
    }
    
    @IBAction func profileBTNTapped(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        
        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        profileIMG.image = pickedimage
        uploadsingleImage()
        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
   
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
 //MARK: - PROFILE UPLOAD API CALL
      func uploadsingleImage(){
          if selectedProfileImage != nil{
              
              let uiImage : UIImage = self.selectedProfileImage
              imageData = uiImage.jpegData(compressionQuality: 0.2)!
              
              imageStr = imageData.base64EncodedString()
              fileName = "image"
              mimetype = mimeType(for: imageData)
              print("mime type = \(mimetype)")
          }else{
              imageData = Data()
              fileName = ""
          }
        
          
          ServiceManager.sharedInstance.uploadSingleDataa("saveSingleImage", headerf: token, parameters: nil, image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
              print(response as Any)
              if success {
                  if response!["status"] as! Bool == true {
                      self.imageupload = response!["filename"] as! String
                      
                      print("image upload success",self.imageupload)
                      
                  }else {
                      showDefaultAlert(viewController: self, title: "", msg: "Successfully uploaded file")
                  }
              }else{
                  print(error?.localizedDescription as Any)
                  showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
              }
          }
      }
}


extension AddGeneralContactVC: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        code = country.phoneCode
       countrypickerLbl.text = code
       if code  == "+1"{
           mobileTextField.placeholder = "(123) 456-7890"
       }else if code == "+91"{
           mobileTextField.placeholder = "1234567890"
       }
       
   }
   static func checkEnglishPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {
       
       if string == ""{ //BackSpace
           
           return true
           
       }else if str!.count < 3{
           
           if str!.count == 1{
               
               phoneNumber.text = "("
           }
           
       }else if str!.count == 5{
           
           phoneNumber.text = phoneNumber.text! + ") "
           
       }else if str!.count == 10{
           
           phoneNumber.text = phoneNumber.text! + "-"
           
       }else if str!.count > 14{
           
           return false
       }
       
       return true
   }
   //
   static func checkindianPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {
       
       if string == ""{ //BackSpace
           
           return true
           
       }else if str!.count > 10{
           
           return false
       }
       
       return true
    }
    
    
}


extension AddGeneralContactVC{
//MARK: - API Call
    @available(iOS 14.0, *)
    func addgeneralcontactapi(){
   
//    let ttoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjNiYjllMTU5MDY2YjA1YzQyZGY2YjQzIiwiaWF0IjoxNjczMjQwMTM4fQ.J589WhSiU-dY9heyw-AfaAQi4abDbWADg1C3Hk3RLSc"
//    let uuserid = "63bb9e159066b05c42df6b43"
    
  //  token = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    let countrycode = countrypickerLbl.text ?? ""
    let contactname = nameTextField.text ?? ""
    let mobnumber = mobileTextField.text ?? ""
    
    let params: [String : Any] = [
                  "userId": userid,
                  "countryCode": countrycode,
                  "name": contactname,
                  "contactimage": imageupload,
                  "phonenumber": mobnumber,
     ]
    print(params)
        
     let url = "http://13.234.177.61/api7/addGeneralContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"])", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Contact saved sucessfully"{
                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
                                let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
                                self.navigationController?.pushViewController(vc, animated: true)
                           // self.present(alert, animated: true)
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"])", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
        }
    }
}
