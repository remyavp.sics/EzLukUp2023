//
//  EditGeneralContactVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 28/11/22.
//

import UIKit
import Alamofire
import CountryPickerView
import PhoneNumberKit

class EditGeneralContactVC: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {

    @IBOutlet weak var profileIMG: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var flagview: UIView!
    @IBOutlet weak var numberview: UIView!
    @IBOutlet weak var countrypickerview: CountryPickerView!
    @IBOutlet weak var countrycodeLbl: UILabel!
    @IBOutlet weak var mobileTextField: UITextField!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var deleteBtn: BaseButton!
    
    var editlist : editGeneralResponse?
    var pickedimage :UIImage?
    var imagePicker = UIImagePickerController()
    var selectedProfileImage : UIImage!
    var imageData = Data()
    var imageStr = ""
    var fileName = ""
    var mimetype = ""
    var uploadedImageStr = ""
    var imageupload = ""
    var getname = ""
    var getmobile = ""
    var getcountry = ""
    var getcontactid = ""
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var activityView: UIActivityIndicatorView?
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var code = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
        countrysetup()
    }
    
    func setUpUI(){
        self.nameTextField.text = self.getname
        self.mobileTextField.text = self.getmobile
        self.countrycodeLbl.text = self.getcountry
        self.flagview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.flagview.layer.borderWidth = 1
        self.numberview.layer.borderWidth = 1
        self.numberview.layer.borderColor = #colorLiteral(red: 0.2509803922, green: 0.2509803922, blue: 0.2509803922, alpha: 1)
        self.saveBtn.addCornerForView(cornerRadius: 14.0)
        self.flagview.addCornerForView(cornerRadius: 14.0)
        self.numberview.addCornerForView(cornerRadius: 14.0)
        mobileTextField.delegate = self
        saveBtn.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
        deleteBtn.backgroundColor = #colorLiteral(red: 0.9176470588, green: 0.262745098, blue: 0.2078431373, alpha: 1)
    }
    
    func countrysetup(){
        addDoneButtonOnKeyboard()
        countrypickerview.showPhoneCodeInView = false
        countrypickerview.showCountryCodeInView = true
        countrypickerview.showCountryNameInView = false
        countrypickerview.font = .systemFont(ofSize: 15)
        countrypickerview.delegate = self
        countrypickerview.dataSource = self
        countrypickerview.hostViewController = self

        countrypickerview.setCountryByPhoneCode(getcountry)
        
        countrycodeLbl.text = countrypickerview.selectedCountry.phoneCode
    }
    
    func addDoneButtonOnKeyboard(){
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        mobileTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction(){
        mobileTextField.resignFirstResponder()
    }
    
    //MARK:- Delegates
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == mobileTextField
        {
            if countrycodeLbl.text == "+1" && mobileTextField.text == ""{
                mobileTextField.placeholder = "(123) 456-7890"
            }else if countrycodeLbl.text == "+91" && mobileTextField.text == ""{
                mobileTextField.placeholder = "(123) 456-7890"
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let str = (textField.text! as NSString).replacingCharacters(in: range, with: string)
               
               if textField == mobileTextField
               {
                   if countrycodeLbl.text == "+1"{
                       print("usa selected")
                       return EditGeneralContactVC.checkEnglishPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }
                   else{
                       mobileTextField.placeholder = "1234567890"
                       return EditGeneralContactVC.checkindianPhoneNumberFormat(string: string, str: str, phoneNumber: mobileTextField)
                   }
                   
               }
               else
               {
                   return true
               }
          }
     }


//MARK: - @IBActions
extension EditGeneralContactVC {
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
       
        self.getname = ""
        self.getmobile = ""
        self.getcountry = ""
    }
    
    @available(iOS 14.0, *)
    @IBAction func saveBTNTapped(_ sender: UIButton) {
        editgeneralcontactapi()
        self.getname = ""
        self.getmobile = ""
        self.getcountry = ""
    }
    
    @IBAction func deleteBTNTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Alert", message: "Do you want to delete?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.deletegeneralcontactapi()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
 //MARK: - profile image picker
    @IBAction func profileBTNTapped(_ sender: UIButton) {
        let imagePickerController = UIImagePickerController()
        imagePickerController.allowsEditing = false //If you want edit option set "true"
        imagePickerController.sourceType = .photoLibrary
        imagePickerController.delegate = self
        present(imagePickerController, animated: true, completion: nil)
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let tempImage:UIImage = info[.originalImage] as! UIImage
        
        self.selectedProfileImage = tempImage
        pickedimage  = tempImage
        profileIMG.image = pickedimage
        uploadContactImage()
        picker.dismiss(animated: true) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //mime types
      func mimeType(for data: Data) -> String {
          
          var b: UInt8 = 0
          data.copyBytes(to: &b, count: 1)

          switch b {
          case 0xFF:
              return "i.jpeg"
          case 0x89:
              return ".png"
          case 0x47:
              return ".gif"
          case 0x4D, 0x49:
              return ".tiff"
          case 0x25:
              return "application/pdf"
          case 0xD0:
              return "application/vnd"
          case 0x46:
              return "text/plain"
          default:
              return "application/octet-stream"
          }
      }
    
 //MARK: - PROFILE UPLOAD API CALL
      func uploadContactImage(){
          if selectedProfileImage != nil{
              showActivityIndicator()
              let uiImage : UIImage = self.selectedProfileImage
              imageData = uiImage.jpegData(compressionQuality: 0.2)!
              
              imageStr = imageData.base64EncodedString()
              fileName = "image"
              mimetype = mimeType(for: imageData)
              print("mime type = \(mimetype)")
          }else{
              imageData = Data()
              fileName = ""
          }

          ServiceManager.sharedInstance.uploadSingleDataa("saveContactImage", headerf: token, parameters: ["contactid":getcontactid], image: imageData, filename: fileName, mimetype: mimetype, withHud: false) { (success, response, error) in
              print(response as Any)
              if success {
                  if response!["status"] as! Bool == true {
                      self.hideActivityIndicator()
                      print("image upload success",self.imageupload)
                      
                  }else {
                      showDefaultAlert(viewController: self, title: "", msg: "Successfully uploaded file")
                  }
              }else{
                  print(error?.localizedDescription as Any)
                  showDefaultAlert(viewController: self, title: "", msg: error?.localizedDescription ?? "API Failed...!")
              }
          }
      }
}


//MARK: - CountryPickerViewDelegate
extension EditGeneralContactVC: CountryPickerViewDelegate, CountryPickerViewDataSource {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        code = country.phoneCode
       countrycodeLbl.text = code
       if code  == "+1"{
           mobileTextField.placeholder = "(123) 456-7890"
       }else if code == "+91"{
           mobileTextField.placeholder = "1234567890"
       }
       
   }
   static func checkEnglishPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {
       
       if string == ""{ //BackSpace
           
           return true
           
       }else if str!.count < 3{
           
           if str!.count == 1{
               
               phoneNumber.text = "("
           }
           
       }else if str!.count == 5{
           
           phoneNumber.text = phoneNumber.text! + ") "
           
       }else if str!.count == 10{
           
           phoneNumber.text = phoneNumber.text! + "-"
           
       }else if str!.count > 14{
           
           return false
       }
       
       return true
   }
   //
   static func checkindianPhoneNumberFormat(string: String?, str: String?,phoneNumber : UITextField) -> Bool
   {
       
       if string == ""{ //BackSpace
           
           return true
           
       }else if str!.count > 10{
           
           return false
       }
       
       return true
    }
    
    
}
extension EditGeneralContactVC{
//MARK: - SAVE API Call
    @available(iOS 14.0, *)
    func editgeneralcontactapi(){
   
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let params: [String : Any] = [
                  "contactid": getcontactid,
                  "countrycode": self.countrycodeLbl.text ?? "",
                  "name": self.nameTextField.text ?? "",
                  "phonenumber": self.mobileTextField.text ?? ""]
     
    
     let url = "http://13.234.177.61/api7/editGeneralContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)

                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String == "Updated sucessfully"{

                            self.navigationController?.popViewController(animated: true)
                            
                          
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)
                    
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
     }
}

//MARK: - DELETE API Call
func deletegeneralcontactapi(){
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let params: [String : Any] = [
                  "contactid": getcontactid ]
    
     let url = "http://13.234.177.61/api7/deleteContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String == "Record removed sucessfully"{
                            
//                            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//                                let vc = storyboard.instantiateViewController(identifier: "ContactsmainVC") as! ContactsmainVC
//                                self.navigationController?.pushViewController(vc, animated: true)
                            if let viewControllers: [UIViewController] = self.navigationController?.viewControllers {
                                    guard viewControllers.count < 3 else {
                                        self.navigationController?.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
                                        return
                                    }
                                guard viewControllers.count < 2 else{
                                    self.navigationController?.popToViewController(viewControllers[viewControllers.count - 2], animated: true)
                                    return
                                }
                                }
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)

                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
     }
}
}

extension EditGeneralContactVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}
