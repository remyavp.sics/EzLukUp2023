//
//  ReportVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 20/02/23.
//

import UIKit
import Alamofire

class ReportVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var closeBTN: BaseButton!
    @IBOutlet weak var postBTN: BaseButton!
    @IBOutlet weak var reportTextView: UITextView!
      
    var getcontactid = ""
    var typedreport = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        reportTextView.delegate = self
        reportTextView.text = "Report the issue you would like to inform the users who have recommended this Service Provider."
        reportTextView.textColor = UIColor.lightGray
        reportTextView.returnKeyType = .done
        postBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    }
    
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if reportTextView.text == "Report the issue you would like to inform the users who have recommended this Service Provider." {
                    reportTextView.text = ""
                    reportTextView.textColor = UIColor.black
                    reportTextView.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if reportTextView.text == "" {
                    reportTextView.text = "Report the issue you would like to inform the users who have recommended this Service Provider."
                    reportTextView.textColor = UIColor.lightGray
                    reportTextView.font = UIFont(name: "jost", size: 15.0)
                }else{
                    typedreport = reportTextView.text
                }
                
            }
   
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.reportTextView.text?.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postBTNTapped(_ sender: UIButton) {
        if typedreport == ""{
            showDefaultAlert(viewController: self, title: "Message", msg: "Please enter report")
        }else{
            reportapi()
        }
    }
}


extension ReportVC{
    
    //MARK: - add report
       func reportapi(){

//           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
           
       let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
       let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
       let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
       
       let report = reportTextView.text ?? ""
       let params: [String : Any] = [
                     "providerid": getcontactid,
                     "userid":userid,
                     //"userid": "63d7bae986e8dfe1c06e732b",
                     "issue": report]
       
       print(params)
           
        let url = "http://13.234.177.61/api7/saveReportFeedback"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "Report saved sucessfully"{
                              
                               self.navigationController?.popViewController(animated: true)
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
}
