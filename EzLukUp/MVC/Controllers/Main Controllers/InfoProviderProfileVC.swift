//
//  InfoProviderProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/12/22.
//

import UIKit
import Alamofire

class InfoProviderProfileVC: UIViewController {

    @IBOutlet weak var infotableview: UITableView!
    
    var getinfolist : getProviderProfileResponse?
    var locz = ""
    var InfoImgsarray = ["location_icon","mobile_icon","calendar_icon"]
    var InfoCellarray1 = ["Area of Service","Mobile","Hours of Operation"]
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    var phonenum = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
          infoapi()
       
    }
}


//MARK: - INFO UITableViewDelegate,UITableViewDataSource
extension InfoProviderProfileVC : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            let city = self.getinfolist?.Data?.areaOfService.first?.city ?? ""
            let stateshortcode = self.getinfolist?.Data?.areaOfService.first?.stateShortCode ?? ""
            if city == "" && stateshortcode == ""{
                  return 0
              }
              else{
                 return 1
              }
        }else if section == 1{
            if self.getinfolist?.Data?.isUser == true{
                phonenum = self.getinfolist?.Data?.contactUserId?.phoneNumber ?? ""
            }else{
                phonenum = self.getinfolist?.Data?.phoneNumber ?? ""
            }
            
            
            if phonenum == ""{
                  return 0
              }
              else{
                 return 1
              }
        }else{
            let hours = self.getinfolist?.Data?.operationalHours ?? ""
            if hours == ""{
                  return 0
              }
              else{
                 return 1
              }
        }
        
       
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = infotableview.dequeueReusableCell(withIdentifier: "InfoTVCell", for: indexPath) as! InfoTVCell
        cell.cellIMG.image = UIImage(named: InfoImgsarray[indexPath.row])
        cell.cellLbl1.text = InfoCellarray1[indexPath.row]
        
        let city = self.getinfolist?.Data?.areaOfService.first?.city ?? ""
        let stateshortcode = self.getinfolist?.Data?.areaOfService.first?.stateShortCode ?? ""
        let joincitystate = "\(city) , \(stateshortcode)"
       // let phonenum = self.getinfolist?.Data?.phoneNumber ?? ""
        let hours = self.getinfolist?.Data?.operationalHours ?? ""
       
        if self.getinfolist?.Data?.isUser == true{
            phonenum = self.getinfolist?.Data?.contactUserId?.phoneNumber ?? ""
        }else{
            phonenum = self.getinfolist?.Data?.phoneNumber ?? ""
        }
        
        
        if indexPath.section == 0{
          if city == "" && stateshortcode == ""{
                cell.cellLbl2.text = ""
            }
            else{
                var samplecitystate = ""
                if city != "" && stateshortcode != ""{
                    samplecitystate = "\(city) , \(stateshortcode)"
                }else{
                    if city == ""{
                        samplecitystate = stateshortcode
                    }else{
                        samplecitystate = city
                    }
                }
                cell.cellLbl2.text = samplecitystate
                cell.cellIMG.image = UIImage(named: "location_icon")
                cell.cellLbl1.text = "Area of Service"
            }
        }
        else if indexPath.section == 1{
            cell.cellLbl2.text = phonenum
            cell.cellIMG.image = UIImage(named: "mobile_icon")
            cell.cellLbl1.text = "Mobile"
        }
        else{
            cell.cellLbl2.text = hours
            cell.cellIMG.image = UIImage(named: "calendar_icon")
            cell.cellLbl1.text = "Hours of Operation"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // let cell = tableView.cellForRow(at: indexPath) as? InfoTVCell
        if indexPath.section == 0{
            let city = self.getinfolist?.Data?.areaOfService.first?.city ?? ""
            let stateshortcode = self.getinfolist?.Data?.areaOfService.first?.stateShortCode ?? ""
            if city == "" && stateshortcode == ""{
                  return 0
              }
              else{
                  return UITableView.automaticDimension
              }
        }else if indexPath.section == 1{
            if self.getinfolist?.Data?.isUser == true{
                phonenum = self.getinfolist?.Data?.contactUserId?.phoneNumber ?? ""
            }else{
                phonenum = self.getinfolist?.Data?.phoneNumber ?? ""
            }
            
            
            if phonenum == ""{
                  return 0
              }
              else{
                  return UITableView.automaticDimension
              }
        }else{
            let hours = self.getinfolist?.Data?.operationalHours ?? ""
            if hours == ""{
                  return 0
              }
              else{
                  return UITableView.automaticDimension
              }
        }
        
        
     //   return cell?.cellLbl2.text?.isEmpty ?? true ? 0 : 50
    }
}


extension InfoProviderProfileVC{
//MARK: - API Call
func infoapi(){
    
    let params = ["contactid": contactid] as [String : Any]
    
    let url = "http://13.234.177.61/api7/getProviderContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.getinfolist = getProviderProfileResponse(from:responsedata)
                    self.infotableview.reloadData()
                    self.locz = self.getinfolist?.Data?.city ?? "" + (self.getinfolist?.Data?.stateShortCode ?? "")
                    print("locationfrom ",self.locz)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}


//MARK: - TableViewCell
class InfoTVCell: UITableViewCell {
    
    @IBOutlet weak var cellIMG: UIImageView!
    @IBOutlet weak var cellLbl1: UILabel!
    @IBOutlet weak var cellLbl2: UILabel!
    
}

