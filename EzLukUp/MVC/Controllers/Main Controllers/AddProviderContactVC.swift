//
//  AddProviderContactVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/11/22.
//

import UIKit
import Alamofire

var search = false
var SelectedcategoryID = [String]()
var searcharrayRes = [getcatDataModel]()
var SelectedCategories = [String]()
var CatNameArray = [String]()
var NewCatarray = [getcatDataModel]()
var CategoryList : getCategoriesResponse?
var Typedfeedback = ""
var Typedmobilenum = ""
var Typedcountry = ""
var Typedname = ""

protocol categorydeletearraydelegate{
    func diddelete(index:Int)
}

class AddProviderContactVC: UIViewController,categorydeletearraydelegate{
    
    func diddelete(index: Int) {
        let newindex = NewCatarray.firstIndex{$0.name == SelectedCategories[index]}
        NewCatarray[newindex ?? 0].isselected = false
        SelectedCategories.remove(at: index)
               categorypopuptbview.reloadData()
               contactTableView.reloadData()
    }
   
    
 //MARK:- Outlets
    @IBOutlet weak var contactTableView: UITableView!
    @IBOutlet weak var categorypopuptbview: UITableView!
    @IBOutlet var categorypopupview: BaseView!
    
    
    var addproviderlist : addProviderResponse?
    var items = [String]()
    var checkedbtnstatus : Bool = false
    var dropstatus = false
    var activityView: UIActivityIndicatorView?
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getcategoryapi()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("reloadcat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ontrue(notification:)), name: Notification.Name("true"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfalse(notification:)), name: Notification.Name("false"), object: nil)
        
    }
   
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = categorypopuptbview.indexPathForSelectedRow {
            categorypopuptbview.deselectRow(at: indexPath, animated: true)
        }
    }
    
    override func viewDidLayoutSubviews() {
        self.categorypopupview.roundCorners(corners: [.topLeft,.topRight, .bottomLeft, .bottomRight], radius: 15)
    }
   
    
    @objc func onContinueClicked(notification: Notification) {
        categorypopuptbview.reloadData()
    }
    
    @objc func ontrue(notification: Notification) {
        search = true
    }
    
    @objc func onfalse(notification: Notification) {
        search = false
    }
   
}



//MARK: - @IBActions
@available(iOS 14.0, *)
extension AddProviderContactVC{
    
    @IBAction func backBTNAction(_ sender: UIButton) {
        SelectedcategoryID.removeAll()
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        SelectedcategoryID.removeAll()
        self.navigationController?.popViewController(animated: true)

    }
    
    @IBAction func btnDropDownTapped(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.contactTableView.reloadSections([1], with: .none)
    }
    
    @IBAction func btnFinishAction(_ sender: UIButton) {
        if SelectedcategoryID.count != 0{
            
        }
        addprovidercontactapi()
        
    }
    
    @IBAction func btnCheckmarkAction(_ sender: UIButton) {
         let tcell = contactTableView.cellForRow(at: IndexPath(row: 0, section: 2)) as! Secondcell
        
        if checkedbtnstatus == false {
            checkedbtnstatus = true
            tcell.checkboxIMG.image = UIImage(named: "selectcheckbox")
        }else{
            checkedbtnstatus = false
            tcell.checkboxIMG.image = UIImage(named: "unselectcheckbox")
        }
            
    }
    
}


//MARK: - UITableViewDelegate & UITableViewDataSource
extension AddProviderContactVC:UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == contactTableView{
            return 3
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == contactTableView{
            if section == 0{
                return 1
            }else if section == 1{
                return 1
            }else if section == 2{
                return 1
            }else{
                return 0
            }
        }else{
            if( search == true){
                return searcharrayRes.count
            }else{
                return NewCatarray.count
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == contactTableView{
            if indexPath.section == 0{
                let cell = contactTableView.dequeueReusableCell(withIdentifier: "FirstCell", for: indexPath) as! FirstCell
                return cell
            }
           else if indexPath.section == 1{
                let cell = contactTableView.dequeueReusableCell(withIdentifier: "AddContactProviderCell", for: indexPath) as! AddContactProviderCell
                cell.delegate = self
                cell.selectedArray = SelectedCategories
                cell.categoryTV.reloadData()
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                    if self.dropstatus {
                       
                        cell.searchview.isHidden = false
                        cell.searchviewheight.constant = 40
                        cell.dropviewheight.constant = 500
                        cell.dropview.isHidden = false
                        cell.dropdownImg.image = UIImage(named: "dropup_icon")
                        cell.dropview.addSubview(self.categorypopupview)
                       
                    }
                    else{
                        
                        cell.dropviewheight.constant = 0
                        cell.searchviewheight.constant = 0
                        cell.searchview.isHidden = true
                        cell.dropview.isHidden = true
                        cell.dropdownImg.image = UIImage(named: "dropdown_icon")
                       
                    }
                    if SelectedCategories.count == 1{
                        if self.dropstatus {
                            cell.categoryviewheight.constant = 83

                        }else {
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 11 )
                        }
                    }
                    else if SelectedCategories.count > 1{
                        if self.dropstatus{
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 40 + 11)
                        }
                        else{
                            cell.categoryviewheight.constant = CGFloat(cell.categoryTV.contentSize.height + 11)
                        }
                    }
                        else{
                            
                            cell.categoryviewheight.constant = 52
                            
                        }
                    
                }
                )
                return cell
            }else{
            let cell = contactTableView.dequeueReusableCell(withIdentifier: "Secondcell", for: indexPath) as! Secondcell
            return cell
        }
        }
        //MARK: - popup category tableview cellforrowat
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryPopupTVCell") as! CategoryPopupTVCell
            print("status",search)
            cell.categorypopupLbl.text = searcharrayRes[indexPath.row].name
                cell.categorypopupLbl.textColor = searcharrayRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
              
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == contactTableView{
            if indexPath.section == 1{
                if SelectedCategories.count == 1{
                                   if self.dropstatus {
                                       return 83 + 9 + 500
               
                                   }else {
                                      return CGFloat((SelectedCategories.count * 32) + 11 + 11)
                                   }
                               }
                               else if SelectedCategories.count > 1{
                                   if self.dropstatus{
                                       return CGFloat((SelectedCategories.count * 32) + 40 + 11 + 9 + 500)
                                   }
                                   else{
                                       return CGFloat((SelectedCategories.count * 32) + 11 + 11)
                                   }
                               }
                                   else{
                                       if self.dropstatus {
                                           return 52 + 500
                                       }
                                       return 52
               
                                   }
                           
            }
            return UITableView.automaticDimension
        }else{
            return 45
        }
        
    }
    
    //MARK: - category tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == categorypopuptbview{
            if let index = searcharrayRes.firstIndex(where: { $0.isselected  }) {
                searcharrayRes[index].isselected = false
            }
            
            searcharrayRes[indexPath.row].isselected = !searcharrayRes[indexPath.row].isselected
            let index = NewCatarray.firstIndex(where: { $0.id ==  searcharrayRes[indexPath.row].id }) ?? 0
            NewCatarray[index].isselected = searcharrayRes[indexPath.row].isselected
            SelectedCategories = NewCatarray.filter{$0.isselected}.map{$0.name ?? ""}
            SelectedcategoryID = NewCatarray.filter{$0.isselected}.map{$0.id ?? ""}
            categorypopuptbview.reloadData()
            contactTableView.reloadData()
           
        }
        else{
            
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == categorypopuptbview{
            
        }
    }
    
}


extension AddProviderContactVC{
    
    //MARK: - category api
        func getcategoryapi() {
         showActivityIndicator()
            
            let ttoken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjNiYjllMTU5MDY2YjA1YzQyZGY2YjQzIiwiaWF0IjoxNjczMjQwMTM4fQ.J589WhSiU-dY9heyw-AfaAQi4abDbWADg1C3Hk3RLSc"
            
            
            let url = "http://13.234.177.61/api7/getCategories"
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                hideActivityIndicator()
                                CategoryList = getCategoriesResponse(from: responsedata)
                               
                                NewCatarray = CategoryList?.Data ?? []
                                DispatchQueue.main.async {
                                searcharrayRes = NewCatarray
                                    print(NewCatarray)
                                    self.contactTableView.reloadData()
                                }
                               }
                   
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
                 }
          }
    
    
//MARK: - API Call
    @available(iOS 14.0, *)
    func addprovidercontactapi(){

   
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    let params: [String : Any] = [
                  "userid": userid,
                  "name": Typedname,
                  "phonenumber": Typedmobilenum,
                  "countryCode": Typedcountry,
                  "recommend": checkedbtnstatus,
                  "feedback": Typedfeedback,
                  "categoryIds": SelectedcategoryID
                  ]
    print(params)
    
     let url = "http://13.234.177.61/api7/addProviderContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Contact saved sucessfully"{
                            self.navigationController?.popViewController(animated: true)
                          
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)

                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
     }
   }
}

extension AddProviderContactVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}


class Cat
{
    var category = ""
    var isselected = false
    init(data:String)
    {
        self.category = data
    }
}

class UserDetails{
    var name = ""
    var searchtext = ""
    func saveuserdetail(name:String,searchtext:String){
        self.name = name
        self.searchtext = searchtext
    }
    func readuserdtail() -> (String,String){
        return (self.name,self.searchtext)
    }
}
