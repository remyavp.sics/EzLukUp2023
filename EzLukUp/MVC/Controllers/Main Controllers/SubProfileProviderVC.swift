//
//  SubProfileProviderVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/11/22.
//

import UIKit
import Alamofire


class SubProfileProviderVC: UIViewController {
    
    @IBOutlet weak var providerTableview: UITableView!
    @IBOutlet weak var nodataLBL: UILabel!
    
    var providerlist : getGeneralProfileResponse?
    var selectid = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nodataLBL.isHidden = true
        DispatchQueue.main.async {
            self.api()
        }
      
      
    }
    
}

//MARK: - @IBActions
extension SubProfileProviderVC{
    
    @IBAction func dropdownBTNTapped(_ sender: UIButton) {
        
        self.providerlist?.RecommendedProviders[sender.tag].sectionOpened = !(self.providerlist?.RecommendedProviders[sender.tag].sectionOpened ?? true)
        providerTableview.reloadData()
    }
    
    @IBAction func dotsBTNTapped(_ sender: UIButton) {
        print("indexpath :",sender.tag)
    }
}


//MARK: - UITableViewDelegate,UITableViewDataSource
@available(iOS 14.0, *)
extension SubProfileProviderVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return providerlist?.RecommendedProviders.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.providerlist?.RecommendedProviders[section].contacts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = providerTableview.dequeueReusableCell(withIdentifier: "Headercell") as! Headercell
        cell.headerNameLbl.text = self.providerlist?.RecommendedProviders[section].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = providerTableview.dequeueReusableCell(withIdentifier: "ProviderContactsCell", for: indexPath) as! ProviderContactsCell
//       cell.nameLbl.text = self.providerlist?.Data[indexPath.section].providercontacts[indexPath.row].name ??         providerlist?.Data[indexPath.row].providercontacts[indexPath.row].twilioCallerName
        
        cell.nameLbl.text = self.providerlist?.RecommendedProviders[indexPath.section].contacts[indexPath.row].name //providerlist?.RecommendedProviders[indexPath.row].contacts[indexPath.row].
        
        let rec = self.providerlist?.RecommendedProviders[indexPath.section].contacts[indexPath.row].recomended.count
        headcount = rec ?? 0
        print("headcount",rec)
        //
        cell.reccomendCVWidth.constant = CGFloat(20 * headcount)
        if headcount > 0 {
            cell.countLabel.text = "\(headcount)"
        }
        else {
            cell.countLabel.text = ""
        }
        
        cell.providerDotsBTN.tag = indexPath.row
        cell.providerDotsBTN.showsMenuAsPrimaryAction = true
        
      
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                    // Perform action
                //let number = self.contactlist?.normalList[indexPath.section].contactsList[indexPath.row].phoneNumber ?? ""
                let number = self.providerlist?.RecommendedProviders[indexPath.section].contacts[indexPath.row].phoneNumber ?? ""
                
                var url:NSURL = NSURL(string: "telprompt:\(number)")!
                UIApplication.shared.openURL(url as URL)
                print("calling")
                  }

            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                    // Perform action
                let number = 5555555555
                var url:NSURL = NSURL(string: "sms:\(number)")!
                UIApplication.shared.openURL(url as URL)
                      print("texting")
                  }


                  let Share = UIAction(title: "Share",
                                       image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                     // Perform action
                      // text to share
                              let text = "This is some text that I want to share."

                              // set up activity view controller
                              let textToShare = [ text ]
                              let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                              activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

                              // exclude some activity types from the list (optional)
                              activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]

                              // present the view controller
                              self.present(activityViewController, animated: true, completion: nil)
                      print("sharing")
                   }

            let Feedback = UIAction(title: "Feedback",
                                image: UIImage(named: "Feedback")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
                                 ) { action in
                    // Perform action
                    print("typing")
             }

            let Report = UIAction(title: "Report",
                                image: UIImage(named: "Report")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
                                 ) { action in
                    // Perform action
                    print("reporting")
             }

            let Edit = UIAction(title: "Edit",
                                image: UIImage(named: "Editcontact")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
                                 ) { action in
                    // Perform action
                    print("opening")
             }

            cell.providerDotsBTN.menu = UIMenu(title:"", children: [Edit, Report, Feedback, Share, Text, Call])
       
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = 0.0
        } else {
          
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
       // return UITableView.automaticDimension
        return self.providerlist?.RecommendedProviders.count != 0 && (self.providerlist?.RecommendedProviders[indexPath.section].sectionOpened)! ? UITableView.automaticDimension : 0
    }
    func tableView(_ tableView: UITableView,
                   editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell =  providerTableview.dequeueReusableCell(withIdentifier: "ProviderContactsCell", for: indexPath) as! ProviderContactsCell
        cell.providerView.isExclusiveTouch = true
        print("hfgcg")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
      //  self.navigationController?.pushViewController(vc, animated: true)
    }
}

//MARK: - API call
extension SubProfileProviderVC{
    func api(){
        let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
        let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
         
        let params = ["contactid": contactid] as [String : Any]

        let url = "http://13.234.177.61/api7/getGeneralContactProfileDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            print("params: \(params)")
            print(response)
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    self.providerlist = getGeneralProfileResponse(from:responsedata)
                    print("providercount",self.providerlist?.RecommendedProviders.count ?? 0)
                    if self.providerlist?.RecommendedProviders.count == 0 {
                        self.nodataLBL.isHidden = false
                    }else{
                        self.nodataLBL.isHidden = true
                    }
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
            DispatchQueue.main.async {
                self.providerTableview.reloadData()
            }
            
    }
        
    }
}



//MARK: - Provider Profile contacts cell
class ProviderContactsCell: UITableViewCell {
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var dotsIMG: UIImageView!
    @IBOutlet weak var providerDotsBTN: UIButton!
    @IBOutlet weak var providerView: BaseView!
    
    @IBOutlet weak var reccomndCV: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var reccomendCVWidth: NSLayoutConstraint!
}


class Headercell: UITableViewCell {
    @IBOutlet weak var dropdownIMG: UIImageView!
    @IBOutlet weak var headerNameLbl: UILabel!
}
class RecomendedCollectionCell: UICollectionViewCell{
    @IBOutlet weak var viewIMGBg: BaseView!
  
    @IBOutlet weak var recomIMG: UIImageView!
    
}
// MARK: -  recommended collectionview
extension ProviderContactsCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return headcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendedCollectionCell", for: indexPath) as? RecomendedCollectionCell
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = 18
        let h:CGFloat = 18
        return CGSize(width: w, height: h)
    }
}
