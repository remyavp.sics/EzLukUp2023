//
//  SubProfileInfoVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 23/11/22.
//

import UIKit
import Alamofire
import SwiftUI

class SubProfileInfoVC: UIViewController {

    @IBOutlet weak var infotableview: UITableView!
    
    var infolist : getGeneralProfileResponse?
    var InfoImgsarray = ["mobile_icon","location_icon"]
    var InfoTitletarray = ["Mobile","Location"]
  //  var InfoNumLocarray = ["(843) 334-0483","Austin, TX"]
    var contactid = ""
    var phonenum = ""
    
    override func viewDidLoad() {
      super.viewDidLoad()
      infoapi()
       
    }
    
}


//MARK: - INFO UITableViewDelegate,UITableViewDataSource
extension SubProfileInfoVC: UITableViewDelegate,UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            phonenum = self.infolist?.Data?.phoneNumber ?? ""
        
        
        if phonenum == ""{
              return 0
          }
          else{
             return 1
          }
    }
        else{
            let city = self.infolist?.Data?.areaOfService.first?.city ?? ""
            let stateshortcode = self.infolist?.Data?.areaOfService.first?.stateShortCode ?? ""
            if city == "" && stateshortcode == ""{
                  return 0
              }
              else{
                 return 1
              }
           
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = infotableview.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoCell
        cell.infoIMG.image = UIImage(named: InfoImgsarray[indexPath.row])
        cell.titleLbl.text = InfoTitletarray[indexPath.row]
        let city = self.infolist?.Data?.areaOfService.first?.city ?? ""
        let stateshortcode = self.infolist?.Data?.areaOfService.first?.stateShortCode ?? ""
        phonenum = self.infolist?.Data?.phoneNumber ?? ""
        

        if indexPath.section == 0{
            cell.numberLbl.text = phonenum
            cell.infoIMG.image = UIImage(named: "mobile_icon")
            cell.titleLbl.text = "Mobile"
        }
        else{
            if city == "" && stateshortcode == ""{
                  cell.numberLbl.text = ""
              }
              else{
                  var samplecitystate = ""
                  if city != "" && stateshortcode != ""{
                      samplecitystate = "\(city) , \(stateshortcode)"
                  }else{
                      if city == ""{
                          samplecitystate = stateshortcode
                      }else{
                          samplecitystate = city
                      }
                  }
                  cell.numberLbl.text = samplecitystate
                  cell.infoIMG.image = UIImage(named: "location_icon")
                  cell.titleLbl.text = "Location"
              }
            
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            let city = self.infolist?.Data?.areaOfService.first?.city ?? ""
            let stateshortcode = self.infolist?.Data?.areaOfService.first?.stateShortCode ?? ""
            if city == "" && stateshortcode == ""{
                  return 0
              }
              else{
                  return UITableView.automaticDimension
              }
        }else{
                phonenum = self.infolist?.Data?.phoneNumber ?? ""
            
            
            if phonenum == ""{
                  return 0
              }
              else{
                  return UITableView.automaticDimension
              }
        }
    }
}


extension SubProfileInfoVC{
//MARK: - API Call
func infoapi(){
      
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    let params = ["contactid": contactid] as [String : Any]
    
    let url = "http://13.234.177.61/api7/getGeneralContactProfileDetails"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<500) .responseJSON { response in

            switch (response.result) {
            case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.infolist = getGeneralProfileResponse(from:responsedata)
                    self.infotableview.reloadData()
                   // print("headername",self.getservivedetails?.Data?.serviceDetails[0].title)
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        
        
    }
}

}

//MARK: - General Profile Info cell
class InfoCell: UITableViewCell {
    
    @IBOutlet weak var infoview: BaseView!
    @IBOutlet weak var infoIMG: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var numberLbl: UILabel!
    
    
}
