//
//  GeneralContactProfileVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 22/11/22.
//

import UIKit
import Alamofire
import Kingfisher
import IPImage
import Contacts
import FirebaseDynamicLinks

//var selecteduserid = ""

class GeneralContactProfileVC: UIViewController,UITextFieldDelegate {

//MARK:- OUTLETS
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var btnStackview: UIStackView!
    @IBOutlet weak var profileBtn: UIButton!
    @IBOutlet weak var providerBtnView: BaseView!
    @IBOutlet weak var infoBtnView: BaseView!
    @IBOutlet weak var providerLbl: UILabel!
    @IBOutlet weak var infoLbl: UILabel!
    @IBOutlet weak var proContainerview: UIView!
    @IBOutlet weak var infoContainerview: UIView!
    @IBOutlet weak var collectionPopupView: UIView!
    @IBOutlet weak var Blurview: UIView!
    
    
    var getuserdetails : getGeneralProfileResponse?
    var selectedBtn  = 0 // if 0 selected is generalbtn , 1- providerbtn selected
    let appDelegateInstance = UIApplication.shared.delegate as! AppDelegate
    var pickedimage :UIImage?
    var selecteduserid = "" // selected contactid from contactmainVC
    var categorycount = 0
    var getname = ""
    var getmobile = ""
    var getcountry = ""
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as? String ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
       userdetailsapi()
        self.Blurview.isHidden = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        userdetailsapi()
        collectionPopupView.isHidden = true
        self.Blurview.isHidden = true
    }
    

    
 //MARK: - SETUP FUNCTIONS
        func setupUI(){
           // self.profileImageView.image = UIImage(named: "avatar")
            selectedBtn = 0
            self.Blurview.isHidden = true
            btnStackview.layer.cornerRadius = btnStackview.frame.height / 2
            infoBtnView.backgroundColor = UIColor.clear
            providerLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
            infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
            self.proContainerview.isHidden = false
            self.infoContainerview.isHidden = true
            self.collectionPopupView.isHidden = true
         //   self.nameLbl.text = self.getname
        }

    @available(iOS 14.0, *)
    @IBAction func backBTNAction(_ sender: UIButton) {
      //  self.nameLbl.text?.removeAll()
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: - @IBActions
extension GeneralContactProfileVC{
    
    @IBAction func profileBTNTapped(_ sender: UIButton) {
    }
    
    @IBAction func providerBTNTapped(_ sender: UIButton) {
        selectedBtn = 0
        infoBtnView.backgroundColor = UIColor.clear
        providerBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        providerLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        infoLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1)//gray clr
        self.proContainerview.isHidden = false
        self.infoContainerview.isHidden = true
        self.collectionPopupView.isHidden = true
    }
    
    @IBAction func infoBTNTapped(_ sender: UIButton) {
        selectedBtn = 1
        providerBtnView.backgroundColor = UIColor.clear
        infoBtnView.backgroundColor = UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1)
        providerLbl.textColor = UIColor(red: 0.431, green: 0.431, blue: 0.431, alpha: 1) //gray clr
        infoLbl.textColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)//white clr
        self.proContainerview.isHidden = true
        self.infoContainerview.isHidden = false
        self.collectionPopupView.isHidden = true
        
    }
    

    @IBAction func collectionBTNTapped(_ sender: UIButton) {
        if collectionPopupView.isHidden{
            collectionPopupView.isHidden = false
            self.Blurview.isHidden = false
        }
        else{
            collectionPopupView.isHidden = true
        }
    }
    
    @IBAction func collectionCloseTouchBTNTapped(_ sender : UIButton){
        self.Blurview.isHidden = true
        collectionPopupView.isHidden = true
    }
    
//MARK:- PopupView Actions
    @IBAction func inviteBTNTapped(_ sender: UIButton) {
        shareShow()
    }
    
    @IBAction func shareBTNTapped(_ sender: UIButton) {
        let contact = createContact(fname: self.nameLbl.text ?? "", lname: "", mob: getmobile)
        
        do {
            try self.shareContacts(contacts: [contact])
        }
        catch {
            print("failed to share contact with some unknown reasons")
        }
    }    
    
    //MARK: - share contact card from  menu
    func createContact(fname:String,lname:String,mob:String) -> CNContact {
        // Creating a mutable object to add to the contact
        let contact = CNMutableContact()
        contact.imageData = NSData() as Data // The profile picture as a NSData object
        contact.givenName = fname
        contact.familyName = lname
        contact.phoneNumbers = [CNLabeledValue(
            label:CNLabelPhoneNumberiPhone,
            value:CNPhoneNumber(stringValue:mob))]
        
        return contact
    }
   
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
    
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "EditGeneralContactVC") as! EditGeneralContactVC
        vc.getcontactid = self.selecteduserid
        vc.getmobile = self.getuserdetails?.Data?.phoneNumber ?? ""
        vc.getname = self.getuserdetails?.Data?.name ?? ""
        vc.getcountry = self.getuserdetails?.Data?.countryCode ?? ""
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: - API Call
extension GeneralContactProfileVC{
    //MARK: - SHARE INVTIE LINK WITH TYPE
    func shareShow(inviteto:String = "",type:String = "") {
       var shareText = "Hello, world!"
        var sh = ""
        var ln = ""
        //        let linkToShare = "https://mashoproduct.page.link/?pdtid=\(self.productID)&link=\(self.ProductDetailsData.productdetails.webshare!)"
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "eizlukup.page.link"
        components.path = "/invite"
        
        let queryItem1 = URLQueryItem(name: "invitername", value: "ashik")
        let queryItem2 = URLQueryItem(name: "type", value: "personal")
        components.queryItems = [queryItem1,queryItem2]
        
        guard let linkParameter = components.url else {return}
        print("sharing Link :\(linkParameter.absoluteString)")
        guard let shareLink = DynamicLinkComponents.init(link: linkParameter, domainURIPrefix: "https://eizlukup.page.link") else { return }
        // IOS PARAMETERS
        if let bundleID = Bundle.main.bundleIdentifier {
            shareLink.iOSParameters = DynamicLinkIOSParameters(bundleID: bundleID)
        }
        shareLink.iOSParameters?.appStoreID = "1663135116"
        // Android PARAMETERS
        shareLink.androidParameters = DynamicLinkAndroidParameters(packageName: "com.ezlukup")
        // Config MetaData
        shareLink.socialMetaTagParameters = DynamicLinkSocialMetaTagParameters ()
//        shareLink.socialMetaTagParameters?.title = ""
        //        shareLink.socialMetaTagParameters?.descriptionText = self.ProductDetailsData.productdetails.product_description
//        if let imageString = "" , let imageURL = URL(string: imageString) {
//            shareLink.socialMetaTagParameters?.imageURL = imageURL
//        }
        
        guard let longURL = shareLink.url else { return }
        print("The long dynamcLink is :\(longURL)")
        ln = "\(longURL)"
        
        shareLink.shorten { (url, warnings, error) in
            if let error = error {
                print("Oh no! got an error :\(error.localizedDescription)")
                return
            }
            
            if let warnings = warnings {
                for warning in warnings {
                    print("FDL warning :\(warning)")
                }
            }
            
            guard let url = url else { return }
            print("Short url :\(url.absoluteString)")
            sh = "\(url.absoluteString)"
            let vc = UIActivityViewController(activityItems: ["Invite \("") to ezlukup",url], applicationActivities: [])
            self.present(vc, animated: true, completion: nil)
        }
        shareText = "\(sh)"
        print("long URL = \(ln)")
        print("short URL = \(sh)")
        

//            let vc = UIActivityViewController(activityItems: [shareText], applicationActivities: [])
//             present(vc, animated: true, completion: nil)
//
    }
    
    func userdetailsapi(){
        
        let params = ["contactid": selecteduserid] as [String : Any]
        
        let url = "http://13.234.177.61/api7/getGeneralContactProfileDetails"
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in

                switch (response.result) {
                case .success( let JSON):
                    
                    if let responsedata =  JSON as? [String:Any]  {
                        print("responsedata",responsedata)
                        self.getuserdetails = getGeneralProfileResponse(from:responsedata)
                      
                        DispatchQueue.main.async {

//                            self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactImage ?? "")),placeholder: UIImage(named: "avatar"))
//                            if self.getuserdetails?.Data?.contactImage == ""{
//                                self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactUserId?.profilePic ?? "")))
//                            }
                            
                            self.nameLbl.text = self.getuserdetails?.Data?.name ?? ""
                                let ipimage = IPImage(text: self.nameLbl.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
                            self.getmobile = self.getuserdetails?.Data?.phoneNumber ?? ""
                            
                            
//                            if self.getuserdetails?.Data?.contactUserId?.profilePic == nil{
//                                print("profile")
                                self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactImage ?? "")),placeholder: ipimage.generateImage())
//                                }
//                            else{
//                                print("contact")
//                               self.profileImageView.kf.setImage(with: URL(string: kImageUrl + (self.getuserdetails?.Data?.contactUserId?.profilePic ?? "")))
//
//                            }
                        }
                       
                    }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
                }
            
            
        }

    }
}
