//
//  FeedbackToEzVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 09/03/23.
//

import UIKit
import Alamofire


class FeedbackToEzVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var typeOfFeedbackTF: UITextField!
    @IBOutlet weak var dropIMG: UIImageView!
    @IBOutlet weak var typeofFeedbackHeight: NSLayoutConstraint!
    @IBOutlet weak var typeOFFeedbackTV: UITableView!
    @IBOutlet weak var detailText: UITextView!
    @IBOutlet weak var sendBTN: BaseButton!
    
    
    var feedbacktypelist : FeedbackTypesList?
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    var typedabout = ""
    var checktableheight : Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getfeedbacktypesapi()
        setupUI()
    }
  
    func setupUI(){
        detailText.delegate = self
        detailText.text = "Provide details here"
        detailText.textColor = UIColor.lightGray
        detailText.returnKeyType = .done
        sendBTN.backgroundColor = #colorLiteral(red: 0.2588235294, green: 0.5215686275, blue: 0.9568627451, alpha: 1)
    }
    
    //MARK: - textview delegate functions
        func textViewDidBeginEditing(_ textView: UITextView) {
                if detailText.text == "Provide details here" {
                    detailText.text = ""
                    detailText.textColor = UIColor.black
                    detailText.font = UIFont(name: "jost", size: 15.0)
                }
            }
            
            func textView(_ descriptionTxt: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
                if text == "\n" {
                    descriptionTxt.resignFirstResponder()
                }
                return true
            }
            
            func textViewDidEndEditing(_ textView: UITextView) {
                if detailText.text == "" {
                    detailText.text = "Provide details here"
                    detailText.textColor = UIColor.lightGray
                    detailText.font = UIFont(name: "jost", size: 15.0)
                }
                else{
                    typedabout = detailText.text
                }
                
            }
    
}

//MARK: - @IBActions
extension FeedbackToEzVC{
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func dropBTNTapped(_ sender: UIButton) {
        if checktableheight == false {
            checktableheight = true
            typeofFeedbackHeight.constant = 120
            //CGFloat(self.feedbacktypelist?.Data.count ?? 0 * 30)
            self.dropIMG.image = UIImage(named: "dropup_icon")
            
        }
        else{
            typeofFeedbackHeight.constant = 0
            checktableheight = false
            self.dropIMG.image = UIImage(named: "dropdown_icon")
        }
        self.typeOFFeedbackTV.reloadData()
    }
    
    
    @IBAction func sendBTNTapped(_ sender: UIButton) {
        if typeOfFeedbackTF.text == ""{
            showDefaultAlert(viewController: self, title: "Message", msg: "select type of feedback")
        }
        else if typedabout == ""{
            showDefaultAlert(viewController: self, title: "Message", msg: "enter feedback")
        }
        else{
            saveAdminFeedbackapi()
        }
    }
}


//MARK: - Tableview Functions
extension FeedbackToEzVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.feedbacktypelist?.Data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = typeOFFeedbackTV.dequeueReusableCell(withIdentifier: "TypeOfFeedbackCell", for: indexPath) as! TypeOfFeedbackCell
        cell.typesLabel.text = self.feedbacktypelist?.Data[indexPath.row].type ?? ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        UserDefaults.standard.setValue(self.feedbacktypelist?.Data[indexPath.row].id, forKey: "Kfeedbackid")
       
        typeOfFeedbackTF.text =  self.feedbacktypelist?.Data[indexPath.row].type ?? ""
        typeofFeedbackHeight.constant = 0
        self.dropIMG.image = UIImage(named: "dropdown_icon")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
}


//MARK: - API's
extension FeedbackToEzVC{
    
    //MARK: - getFeedbackTypes API
       func getfeedbacktypesapi(){
     
           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
           
        let url = "http://13.234.177.61/api7/getFeedbackTypes"
       AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result)      {
              case .success( let JSON):
                
                if let responsedata =  JSON as? [String:Any]  {
                    print("responsedata",responsedata)
                    self.feedbacktypelist = FeedbackTypesList(from:responsedata)
                    self.typeOFFeedbackTV.reloadData()
                         
                }
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
     }
    
    
    //MARK: - saveAdminFeedback
       func saveAdminFeedbackapi(){

       let feedbackid : String = UserDefaults.standard.value(forKey: "Kfeedbackid") as? String ?? ""
       
       let params: [String : Any] = [
                     "feedback": typedabout,
                     "feedbacktypeid": feedbackid,
                     "userid": userid
                    //"userId":"63d7bae986e8dfe1c06e732b"
                    ]
       
       print(params)
           
        let url = "http://13.234.177.61/api7/saveAdminFeedback"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "Feedback saved sucessfully"{
                               self.navigationController?.popViewController(animated: true)
                               
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
    
}


class TypeOfFeedbackCell: UITableViewCell{
    @IBOutlet weak var typesLabel: UILabel!
}
