//
//  AccountPrivacyVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/03/23.
//

import UIKit
import Alamofire

class AccountPrivacyVC: UIViewController{
    
    @IBOutlet weak var connectionTF: UITextField!
    @IBOutlet weak var connectionsTV: UITableView!
    @IBOutlet weak var conneViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropIMG: UIImageView!
    
    var connArray = ["My connections","All connections"]
    var checktableheight : Bool = false
    var valuekey : Bool = false
    
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
    }
    

    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
   
    @IBAction func dropBTNTapped(_ sender: UIButton) {
        if checktableheight == false {
            checktableheight = true
            conneViewHeight.constant = CGFloat(self.connArray.count * 40)
            self.dropIMG.image = UIImage(named: "dropup_icon")
        }
        else{
            conneViewHeight.constant = 0
            checktableheight = false
            self.dropIMG.image = UIImage(named: "dropdown_icon")
        }
        self.connectionsTV.reloadData()
    }
    
}
    

extension AccountPrivacyVC: UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return connArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AccountPrivacyCell") as! AccountPrivacyCell
        cell.connectionLbl.text = connArray[indexPath.row]
        return cell
    }
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        connectionTF.text =  connArray[indexPath.row]
        connectionTF.resignFirstResponder()
        conneViewHeight.constant = 0
        self.dropIMG.image = UIImage(named: "dropdown_icon")
        uservisibilityapi()
    }
}


extension AccountPrivacyVC{
    //MARK: - change user settings
       func uservisibilityapi(){

           if connectionTF.text == "All connections"{
               valuekey = true
           }else{
               valuekey = false
           }
           
           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
    
           
       let params: [String : Any] = [
                     "userid": userid,
                 "key": "userDetailsVisibleAll",
                 "value": valuekey,
                   // "userId":"632dab05aded9e72781ad266"
                                     ]
       print(params)
           
           
        let url = "http://13.234.177.61/api7/changeSettings"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "User activated sucessfully"{
                               self.navigationController?.popViewController(animated: true)
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
}

class AccountPrivacyCell: UITableViewCell{
    @IBOutlet weak var connectionLbl: UILabel!
}
