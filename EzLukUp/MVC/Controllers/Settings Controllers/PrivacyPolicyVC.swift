//
//  PrivacyPolicyVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/03/23.
//

import UIKit
import WebKit

class PrivacyPolicyVC: UIViewController, WKUIDelegate {

    
  //  @IBOutlet weak var mainwebview: UIWebView!
    @IBOutlet weak var mainview: UIView!
    
    var webView = WKWebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myURL = URL(string:"http://ezlukup.com/privacy-policy.html")
                let myRequest = URLRequest(url: myURL!)
                webView.load(myRequest)
        
    }
    
    override func loadView() {
           let webConfiguration = WKWebViewConfiguration()
           webView = WKWebView(frame: .zero, configuration: webConfiguration)
           webView.uiDelegate = self
           view = webView
       }

    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
