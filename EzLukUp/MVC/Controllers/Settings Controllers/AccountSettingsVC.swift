//
//  AccountSettingsVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/03/23.
//

import UIKit
import Alamofire

class AccountSettingsVC: UIViewController {

    
    @IBOutlet var switchIMG: UIImageView!
    @IBOutlet weak var accountStatusLbl: UILabel!
    @IBOutlet weak var DeleteBTN: UIButton!
    @IBOutlet weak var s2cBTN: UIButton!
    
    
    var fromdisabled : Bool = false
    var switchstatus : Bool = false
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
//    UserDefaults.standard.set(true, forKey: "accstatus")
    var typezc : Bool = UserDefaults.standard.value(forKey: "accstatusz") as? Bool ?? false //true when account is disabled and false whn acnt enabled
    
    override func viewDidLoad() {
        super.viewDidLoad()
       if fromdisabled{
           self.accountStatusLbl.text = "Disable Account"
           self.switchIMG.image = UIImage(named: "on")
           switchstatus = true
           DeleteBTN.isUserInteractionEnabled = false
           s2cBTN.isUserInteractionEnabled = false
           UserDefaults.standard.set(true, forKey: "accstatusz")
       }else{
           self.accountStatusLbl.text = "Disable Account"
           self.switchIMG.image = UIImage(named: "off")
           switchstatus = false
           UserDefaults.standard.set(false, forKey: "accstatusz")
       }
    }
}

//MARK: - IBActions
extension AccountSettingsVC{
    
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func switchBTNTapped(_ sender: UIButton) {
        if switchstatus == false {
            
            self.switchIMG.image = UIImage(named: "on")
//            self.accountStatusLbl.text = "Disable Account"
            switchstatus = true
            disableuserapi()
            
        }else{
            switchstatus = false
           UserDefaults.standard.set(false, forKey: "accstatusz")
            self.switchIMG.image = UIImage(named: "off")
//            self.accountStatusLbl.text = "Disable Account"
            enableuserapi()
        }

    }
    
    @IBAction func deleteaccountBTN(_ sender: UIButton) {
        let alert = UIAlertController(title: "Alert", message: "Do you want to delete?", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.deleteuserapi()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: {(action) in
            
        }))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    @IBAction func switchConsumerBTN(_ sender: UIButton) {
    }
    
}


extension AccountSettingsVC{
    //MARK: - DisableUSer
    func disableuserapi(){

        let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjM0MDhiMTg0ZjZlMzEyNTQ0ZTg3NjE1IiwicGhvbmVOdW1iZXIiOiIrOTE2MjgyNTY3Mjk3IiwiaWF0IjoxNjY1MTc0NzEyfQ.-kabC1ittSsEDloxFv1JzxVtR7zJhNRCxmLjfobed9E"
    
    let params: [String : Any] = [
                  "userid": userid
                // "userid":"632dab05aded9e72781ad266"
                                  ]
    print(params)
        
        
     let url = "http://13.234.177.61/api7/disableUser"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "User deactivated sucessfully"{
                            
                            UserDefaults.standard.set(true, forKey: "accstatusz") // accnt is disabled whn true
                            UserDefaults.standard.setValue("false", forKey: "loggedin")
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate
                            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                            var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                            let nav = UINavigationController(rootViewController: homeViewController)
                            nav.navigationBar.isHidden = true
                            appdelegate.window!.rootViewController = nav
                                    navigationController?.popToRootViewController(animated: true)
                                    loggedin = false
                                    alreadyloggedin = false
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK: - EnableUSer
       func enableuserapi(){

           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
       
       let params: [String : Any] = [
                     "userid": userid
                  //  "userid":"63d7bb7d86e8dfe1c06e748f"
                                     ]
       print(params)
           
           
        let url = "http://13.234.177.61/api7/enableUser"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "User activated sucessfully"{
                               DeleteBTN.isUserInteractionEnabled = true
                               s2cBTN.isUserInteractionEnabled = true
                               
                               UserDefaults.standard.setValue("true", forKey: "loggedin")
                               let appdelegate = UIApplication.shared.delegate as! AppDelegate
                               let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
                               var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
                               let nav = UINavigationController(rootViewController: homeViewController)
                               nav.navigationBar.isHidden = true
                               appdelegate.window!.rootViewController = nav
                                       navigationController?.popToRootViewController(animated: true)
                                       loggedin = true
                                       alreadyloggedin = true
                               
                               
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
    
    
    //MARK: - DeleteUser
       func deleteuserapi(){

           let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
       
       let params: [String : Any] = [
                     "userid": userid
                  //  "userid":"63d7bb7d86e8dfe1c06e748f"
                                     ]
       print(params)
           
           
        let url = "http://13.234.177.61/api7/deleteUser"
       AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
            switch (response.result) {
               case .success( let JSON):
                   if let responsedata =  JSON as? [String:Any]  {
                     print("responsedata :",responsedata)
                       let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                       alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                           if responsedata["message"] as? String ?? "" == "User deleted sucessfully"{
                                let appdelegate = UIApplication.shared.delegate as! AppDelegate
                                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                                var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                                let nav = UINavigationController(rootViewController: homeViewController)
                                nav.navigationBar.isHidden = true
                                appdelegate.window!.rootViewController = nav
                                        navigationController?.popToRootViewController(animated: true)
                                loggedin = false
                                alreadyloggedin = false
                                
                           }else{
                               let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                           }
                           }
                                                     ))
                               self.present(alert, animated: true)
                   }
                   case .failure(let error):
                       print("Request error: \(error.localizedDescription)")
               }
           }
       }
}
