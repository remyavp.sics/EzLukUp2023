//
//  TermsOfUseVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 07/03/23.
//

import UIKit
import WebKit
import SafariServices

class TermsOfUseVC: UIViewController,SFSafariViewControllerDelegate {

    var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        webView = WKWebView()
            view = webView

          //  loadWebPage()
         showLinksClicked()
       
    }
    
    func loadWebPage() {
        let url = URL(string: "https://www.google.co.in/?client=safari&channel=mac_bm")!
        let request = URLRequest(url: url)
        webView.load(request)
    }
    func showLinksClicked() {

        let safariVC = SFSafariViewController(url: URL(string: "http://ezlukup.com/service.html")!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }

    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
