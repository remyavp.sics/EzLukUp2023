//
//  SettingsVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 06/03/23.
//

import UIKit
import SafariServices

class SettingsVC: UIViewController,SFSafariViewControllerDelegate {

    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
 
    @IBAction func closeBTNTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func accsettingsBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "AccountSettingsVC") as! AccountSettingsVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func accprivacyBTN(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
        let vc = storyboard.instantiateViewController(identifier: "AccountPrivacyVC") as! AccountPrivacyVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func privacypolicyBTN(_ sender: UIButton) {
        let safariVC = SFSafariViewController(url: URL(string: "http://ezlukup.com/privacy-policy.html")!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
    }
    
    @IBAction func termsofuseBTN(_ sender: UIButton) {
        let safariVC = SFSafariViewController(url: URL(string: "http://ezlukup.com/service.html")!)
        present(safariVC, animated: true, completion: nil)
        safariVC.delegate = self
//        let storyboard = UIStoryboard(name: "Settings", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "TermsOfUseVC") as! TermsOfUseVC
//        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func signoutBTN(_ sender: UIButton) {
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Login", bundle: nil)
                var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "WelcomeVC") as! WelcomeVC
                let nav = UINavigationController(rootViewController: homeViewController)
                nav.navigationBar.isHidden = true
                appdelegate.window!.rootViewController = nav

                UserDefaults.standard.setValue("false", forKey: "loggedin")
                        navigationController?.popToRootViewController(animated: true)
                        loggedin = false
                        alreadyloggedin = false
    }

    func safariViewControllerDidFinish(controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }


}
