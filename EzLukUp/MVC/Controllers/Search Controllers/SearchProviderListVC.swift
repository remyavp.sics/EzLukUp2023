//
//  SearchProviderListVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 12/02/23.
//

import UIKit
import Contacts
import Alamofire
import CCBottomRefreshControl
import IPImage
import Kingfisher

var recomheadcount = 0
var checkcollectionstatus = ""

class SearchProviderListVC: UIViewController {

//Outlets
    @IBOutlet weak var searchListTV: UITableView!
    @IBOutlet weak var filterIMG: UIImageView!
    @IBOutlet weak var countrystatecityTxt: UITextField!
    @IBOutlet weak var providercategoryTxt: UITextField!
    
    @IBOutlet weak var filterview: UIView!
    @IBOutlet weak var filteroneLbl: UILabel!
    @IBOutlet weak var filtertwoLbl: UILabel!
    @IBOutlet weak var filterthreeLbl: UILabel!
    @IBOutlet weak var nodataLbl: UILabel!
    
    var searchcitystate : ContactList?
    var searchproviderlist : SearchProviderListResponse?
    var searchproviderlistpg : SearchProviderListResponse?
    let token : String = UserDefaults.standard.value(forKey: "Ktoken") as? String ?? ""
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as? String ?? ""
   
    var levelListPaginationCount = 0
    var isloadinglist = false
    var levelTotalCount = 3
    var pageTotalCount = 0
    
    let refreshControl = UIRefreshControl()
    var selectedsection = -1
    var fkey = ""
    var endapi = ""
    var filter = ""
    var getcountry = ""
    var getstate = ""
    var getcity = ""
   // var limit = 0
    var activityView: UIActivityIndicatorView?
    
    var getcountrystatecity = ""
    var getprocategory = [String]()    
    var getlat = ""
    var getlong = ""
    var getradius = ""
    var displycategory = ""
    
    var headerbtnclick = true
    var filterkey = "all"
    
    var providers = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.filterview.isHidden = true
        filterview.layer.cornerRadius = 13
        filterview.layer.maskedCorners = [.layerMinXMaxYCorner,.layerMaxXMaxYCorner]
        
        self.countrystatecityTxt.text = getcountrystatecity
        self.providercategoryTxt.text = displycategory
        api()
//        if self.searchproviderlist?.Data.count == 0{
//                self.nodataLbl.isHidden = false
//        }else{
//            self.nodataLbl.isHidden = true
//        }
        self.nodataLbl.isHidden = true
    }
   
    override func viewWillAppear(_ animated: Bool) {
      //  api(filterkey: "")
        
    }
    
    
}


//MARK: - @IBActions
extension SearchProviderListVC{
    
    @IBAction func headerdropBTNTapped(_ sender: UIButton) {
        
        self.searchproviderlist?.Data[sender.tag].sectionOpened = !(self.searchproviderlist?.Data[sender.tag].sectionOpened ?? true)
        searchListTV.reloadData()
    }
   
    @IBAction func filterBTNTapped(_ sender: UIButton) {
        if filterview.isHidden{
            filterview.isHidden = false
            self.filterIMG.image = UIImage(named: "filterselected")
        }
        else{
            filterview.isHidden = true
            self.filterIMG.image = UIImage(named: "filtericon")
        }
        
        filteroneLbl.text = "All"
        filtertwoLbl.text = "In ez"
        filterthreeLbl.text = "Invited"
        //self.nodataLbl.isHidden = true
    }
    
    @IBAction func editBTNTapped(_ sender: UIButton) {
//        let storyboard = UIStoryboard(name: "Home", bundle: nil)
//        let vc = storyboard.instantiateViewController(identifier: "SearchVC") as! SearchVC
//        vc.typedradius = self.getradius
//        vc.state = self.getstate
//        vc.latitude = self.getlat
//        vc.longitude = self.getlong
//        self.navigationController?.pushViewController(vc, animated: true)
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func dotsBTNTapped(_ sender: UIButton) {
        print("indexpath :",sender.tag)
    }
    
    @IBAction func filteroneBTNTapped(_ sender: UIButton) {
        
            self.searchproviderlist = nil
            filterkey = "all"
            levelListPaginationCount = 0
        pageTotalCount = 0
            api()
            self.searchListTV.reloadData()
            filterview.isHidden = true
            self.filterIMG.image = UIImage(named: "filtericon")
           // self.nodataLbl.isHidden = true
        
//        DispatchQueue.main.async {
//            if self.searchproviderlist?.Data.count == 0{
//                self.nodataLbl.isHidden = false
//        }
     // }
    }
    
    @IBAction func filtertwoBTNTapped(_ sender: UIButton) {
        self.searchproviderlist = nil
        //fkey = "inez"
        filterkey = "inez"
        levelListPaginationCount = 0
        pageTotalCount = 0
        api()
        self.searchListTV.reloadData()
        
        filterview.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
        //self.nodataLbl.isHidden = true
    
//    DispatchQueue.main.async {
//        if self.searchproviderlist?.Data.count == 0{
//            self.nodataLbl.isHidden = false
//    }
//    }
    }

    @IBAction func filterthreeBTNTapped(_ sender: UIButton) {
        self.searchproviderlist = nil
       // fkey = "invited"
        filterkey = "invited"
        levelListPaginationCount = 0
        pageTotalCount = 0
        api()
        self.searchListTV.reloadData()
        
        filterview.isHidden = true
        self.filterIMG.image = UIImage(named: "filtericon")
       // self.nodataLbl.isHidden = true
    
//    DispatchQueue.main.async {
//        if self.searchproviderlist?.Data.count == 0{
//            self.nodataLbl.isHidden = false
//    }
//}
    }
    
}


//MARK: - EXtension (Tableview)
extension SearchProviderListVC: UITableViewDelegate,UITableViewDataSource{
   
    func numberOfSections(in tableView: UITableView) -> Int {
        return searchproviderlist?.Data.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searchproviderlist?.Data[section].contacts.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = searchListTV.dequeueReusableCell(withIdentifier: "SearchHeadercell") as! SearchHeadercell
        cell.searchheaderBTN.tag = section
        if self.searchproviderlist?.Data[section].header == "level_0"{
            cell.headerNameLbl.text = "1st Connections"
            providers = []
        }else if self.searchproviderlist?.Data[section].header == "level_1"{
            cell.headerNameLbl.text = "2nd Connections"
           print("chakkarachyyy")
            print(self.searchproviderlist?.providers?.count)
            providers = self.searchproviderlist?.providers ?? []
        }else if self.searchproviderlist?.Data[section].header == "level_2"{
            cell.headerNameLbl.text = "3rd Connections"
            providers = self.searchproviderlist?.providers ?? []
        }else if self.searchproviderlist?.Data[section].header == "level_3"{
            cell.headerNameLbl.text = "More"
            providers = self.searchproviderlist?.providers ?? []
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = searchListTV.dequeueReusableCell(withIdentifier: "SearchProviderListCell", for: indexPath) as! SearchProviderListCell
        
        cell.nameLbl.text = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].name ??
        searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].twilioCallerName
        
  // search provider city & stateshortcode
//        let city = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].city ?? ""
//        let stateshortcode = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].stateShortCode ?? ""
        
        let areacity = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].areaOfService.first?.city ?? ""
        let areastate = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].areaOfService.first?.stateShortCode ?? ""
        
        
//        if searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].isUser == true{
//            cell.locationLbl.text = "\(areacity) , \(areastate)"
//        }else{
//            cell.locationLbl.text = "\(city) , \(stateshortcode)"
//        }
        
        if areacity == "" || areastate == ""{
            cell.locationLbl.text = ""
        }else{
            cell.locationLbl.text = "\(areacity) , \(areastate)"
        }
        
//        if getcity != ""{
//            cell.locationLbl.text = "\(getcity)"
//        }
//        if getstate != ""{
//            cell.locationLbl.text = "\(getstate)"
//        }
//        if getcity != "" || getstate != ""{
//            cell.locationLbl.text = "\(getcity) , \(getstate)"
//        }
        
        

//   provider profile contact img
        DispatchQueue.main.async {
            let ipimage = IPImage(text: cell.nameLbl.text ?? "", radius: 30, font: UIFont(name: "Jost-Regular", size: 25), textColor: nil, randomBackgroundColor: false)
            cell.profileIMG.kf.setImage(with: URL(string: kImageUrl + (self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].contactImage ?? "")),placeholder: ipimage.generateImage())
        }
        
        
    //Recommended
        let rec = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].recomments.count
        recomheadcount = rec ?? 0
                   print("headcount",rec)
        
        cell.reccomendCVWidth.constant = CGFloat(20 * recomheadcount)
        if recomheadcount > 0 {
            cell.countLabel.text = "\(recomheadcount)"
        }
        else {
            cell.countLabel.text = ""
        }
        cell.recomendCV.reloadData()
        
   //MARK:- popupview
        cell.providerDotsBTN.tag = indexPath.row
        cell.providerDotsBTN.showsMenuAsPrimaryAction = true
        
        //MARK: - Call
            let Call = UIAction(title: "Call",
                                image: UIImage(named: "Call")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { _ in
                // Perform action
                var getmobile = ""
                   
                    print("call normal list")
                getmobile = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].phoneNumber ?? ""
                
                var url:NSURL = NSURL(string: "telprompt:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                print("calling")
            }
           
        //MARK: - Text
            let Text = UIAction(title:"Text",
                                image: UIImage(named: "Chat")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                var getmobile = ""
               
                    print("call normal list")
                    getmobile = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].phoneNumber ?? ""
                
                var url:NSURL = NSURL(string: "sms:\(getmobile)")!
                UIApplication.shared.openURL(url as URL)
                print("texting")
            }
            
        //MARK: - Share
            let Share = UIAction(title: "Share",
                                 image: UIImage(named: "Share")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)) { action in
                // Perform action
                    print("share search provider list")
                    let getmobile = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].phoneNumber ?? ""
                    let getname = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].name ?? ""
                    let contact = createContact(fname: getname, lname: "", mob: getmobile)
                    
                    do {
                        try self.shareContacts(contacts: [contact])
                    }
                    catch {
                        print("failed to share contact with some unknown reasons")
                    }
             
                func createContact(fname:String,lname:String,mob:String) -> CNContact {
                    // Creating a mutable object to add to the contact
                    let contact = CNMutableContact()
                    contact.imageData = NSData() as Data // The profile picture as a NSData object
                    contact.givenName = fname
                    contact.familyName = lname
                    contact.phoneNumbers = [CNLabeledValue(
                        label:CNLabelPhoneNumberiPhone,
                        value:CNPhoneNumber(stringValue:mob))]
                    
                    return contact
                }
                // text to share
                let text = "This is some text that I want to share."
                
                // set up activity view controller
                let textToShare = [ text ]
                let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
                activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
                
                // exclude some activity types from the list (optional)
                activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
                
                // present the view controller
                //                self.present(activityViewController, animated: true, completion: nil)
                print("sharing")
            }
  
    //MARK: - Save
        let Save = UIAction(title: "Save",
                            image: UIImage(named: "Save")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
        ) { action in
            self.savesearchcontactapi()
            print("opening")
        }

    //MARK: - Feedback
            let Feedback = UIAction(title: "Feedback",
                                    image: UIImage(named: "Feedback")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "FeedBackVC") as! FeedBackVC
                vc.getcontactid = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                print("typing")
            }
   
    //MARK: - Report
            let Report = UIAction(title: "Report",
                                  image: UIImage(named: "Report")?.withTintColor(.systemBlue,renderingMode:.alwaysOriginal)
            ) { action in
                // Perform action
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(identifier: "ReportVC") as! ReportVC
                vc.getcontactid = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].id ?? ""
                self.navigationController?.pushViewController(vc, animated: true)
                print("reporting")
            }
            

            
            cell.providerDotsBTN.menu = UIMenu(title:"", children: [Call, Text, Share, Save, Feedback, Report])
       
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return self.searchproviderlist?.Data.count != 0 ? 40 : 0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return self.searchproviderlist?.Data.count == 0 ? 0 :  (self.searchproviderlist?.Data[indexPath.section].sectionOpened ?? true) ? UITableView.automaticDimension : 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = searchListTV.dequeueReusableCell(withIdentifier: "SearchProviderListCell", for: indexPath) as! SearchProviderListCell
            print("seach provider")
            print(self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].id ?? "")
            
            UserDefaults.standard.setValue(self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].id, forKey: "Kcontactid")
            cell.providerView.isExclusiveTouch = true
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "ProviderContactProfileVC") as! ProviderContactProfileVC
            vc.selecteduserid = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].id ?? ""
            vc.getname = self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].name ?? ""
            checkcollectionstatus = "fromsearch"
            self.navigationController?.pushViewController(vc, animated: true)
       
    }
    
    
    //MARK: - Did scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print("scrollenabled")
        let pos = searchListTV.contentOffset.y
        if pos > searchListTV.contentSize.height-CGFloat(pageTotalCount) - scrollView.frame.size.height{
            guard !isloadinglist else{
                return
            }
           
            let totalcount = self.searchproviderlist?.Data.count ?? 0
            print("total count showing when pagination = \(totalcount)")

            if self.levelListPaginationCount <= levelTotalCount && self.searchListTV.isScrollEnabled{
                self.searchListTV.isScrollEnabled = false
                
                loadMore()
            }
            else{
                self.searchListTV.isScrollEnabled = true
                return
            }
        }
    }
    
    func loadMore(){
        levelListPaginationCount += 1
        self.api()
    }
    
    //MARK: - share contact card from 3dot menu
    func shareContacts(contacts: [CNContact]) throws {
        
        guard let directoryURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first else {
            return
        }
        
        var filename = NSUUID().uuidString
        
        // Create a human friendly file name if sharing a single contact.
        if let contact = contacts.first, contacts.count == 1 {
            
            if let fullname = CNContactFormatter().string(from: contact) {
                filename = fullname.components(separatedBy: " ").joined(separator: "")
            }
        }
        
        let fileURL = directoryURL
            .appendingPathComponent(filename)
            .appendingPathExtension("vcf")
        
        let data = try CNContactVCardSerialization.data(with: contacts)
        
        print("filename: \(filename)")
        print("contact: \(String(describing: String(data: data, encoding: String.Encoding.utf8)))")
        
        try data.write(to: fileURL, options: [.atomicWrite])
        
        let activityViewController = UIActivityViewController(
            activityItems: [fileURL],
            applicationActivities: nil
        )
        
        present(activityViewController, animated: true, completion: nil)
    }
}

//MARK: - API Call
extension SearchProviderListVC{
  
        func api(){
       // showActivityIndicator()
           // var providers = [String]()
            
            isloadinglist = true
            
//            if provider == "level_0"{
//                provider = []
//            }else{
//
//            }

            print("dynamic userid = \(userid)")
            print("dynamic tocken = \(token)")
            print("filterkey = \(filterkey)")
        let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjQwODgyZmYzNmM1NTE2NmI2OTljZWRkIiwiaWF0IjoxNjc4NTE0MzE3fQ.iDisZEd-rbeUYdu111ff8hL4iaYEBNjZ7djXOUCYmVo"
        
        let params : [String:Any]?
        filter = filterkey
       params = [
                  "userId":userid,
                  "searchCatagory": getprocategory,
                 "country":"us",
                  "state":getstate,
                 "city":getcity,
                  "radius":getradius,
                "latitude":getlat,
                "longitude":getlong,
                 "level":String(levelListPaginationCount),
                  "filter":filterkey,
                 // "providers":providers
                //  "filter":filterkey,
                //  "level":String(levelListPaginationCount),
                //  "userId":"63d7bae986e8dfe1c06e732b",
                 // "userId":"640882ff36c55166b699cedd",
//                  "searchCatagory":["63db5b37d1855fedb191ecc4"],
//                  "state":"TX",
//                  "city":"",
//                  "radius":"",
//                  "latitude":"31.57184",
//                  "longitude":"-85.25049",
                   "providers":self.searchproviderlist?.providers ?? []
        ]
        
        let url = "http://13.234.177.61/api7/searchProvider"
        print("parameters =",params)
        
        AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { response in
        
            switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                    self.searchproviderlistpg = SearchProviderListResponse(fromData:responsedata)
                 //   print(self.sear)
//                    if self.searchproviderlistpg?.status == true{
                        self.isloadinglist = false
                        
                        self.searchproviderlist?.status = self.searchproviderlistpg?.status
                    self.searchproviderlist?.providers = self.searchproviderlistpg?.providers
                    
                        if let _ = self.searchproviderlist?.Data{
                            self.searchproviderlist?.Data.append(contentsOf: self.searchproviderlistpg?.Data ?? [])
                        }
                        else{
                            self.searchproviderlist = SearchProviderListResponse(fromData: [:])
                            self.searchproviderlist?.Data = self.searchproviderlistpg?.Data ?? []

                        }
                        
                        let countt = self.searchproviderlistpg?.Data.count ?? 0
                        self.pageTotalCount = self.pageTotalCount + countt
                    
                    print("abcdefg")
                    print(self.pageTotalCount)
                    print(self.levelListPaginationCount)
                        if self.pageTotalCount <= 4 && self.levelListPaginationCount < 3{
                            self.loadMore()
                        }
                    
                    if self.searchproviderlist?.Data.count == 0{
                             self.nodataLbl.isHidden = false
                         }
                        
                        self.searchListTV.reloadData()
                    

                }
                self.nodataLbl.isHidden = self.searchproviderlist?.Data.count != 0
//                    DispatchQueue.main.async {
//                         if self.searchproviderlist?.Data.count == 0{
//                                  self.nodataLbl.isHidden = false
//                              }
//                }
                
              
               
            case .failure(let error):
                print("Request error: \(error.localizedDescription)")
            }
        }
    }
    

 //MARK: - Save
    func savesearchcontactapi(){

//        let tokenn = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiNjMyZGFiMDVhZGVkOWU3Mjc4MWFkMjY2IiwiZW1haWwiOiJndWVzdC5zd2VldHRyZWF0QGdtYWlsLmNvbSIsImlhdCI6MTY2NTQwNDk1Nn0.XZ_y_QbQ9PgovjlZcgtHqWN8oicgUnVeRqXtiLq26_8"
        
        
    let userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    let contactid : String = UserDefaults.standard.value(forKey: "Kcontactid") as! String
    
    let params: [String : Any] = [
                  "userid": userid,
                 //"userId":"63d7bae986e8dfe1c06e732b",
                  "contactid": contactid]
    
    print(params)
        
     let url = "http://13.234.177.61/api7/saveSearchContact"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result) {
            case .success( let JSON):
                if let responsedata =  JSON as? [String:Any]  {
                  print("responsedata :",responsedata)
                    let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default,handler: { action in
                        if responsedata["message"] as? String ?? "" == "Contact added sucessfully"{
                            self.searchListTV.reloadData()
                            
                        }else{
                            let alert = UIAlertController(title: "", message: "\(responsedata["message"] ?? "")", preferredStyle: .alert)
                        }
                        }
                                                  ))
                            self.present(alert, animated: true)
                }
                case .failure(let error):
                    print("Request error: \(error.localizedDescription)")
            }
        }
    }
}

extension SearchProviderListVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }
    
    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}

//MARK: - Search Provider Profile contacts cell
class SearchProviderListCell: UITableViewCell {
    @IBOutlet weak var profileIMG: BaseImageView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var locationLbl: UILabel!
    @IBOutlet weak var dotsIMG: UIImageView!
    @IBOutlet weak var providerDotsBTN: UIButton!
    @IBOutlet weak var providerView: BaseView!
    @IBOutlet weak var recomendCV: UICollectionView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var reccomendCVWidth: NSLayoutConstraint!
    var searchproviderlist : SearchProviderListResponse?
}

class SearchHeadercell: UITableViewCell {
    @IBOutlet weak var dropdownIMG: UIImageView!
    @IBOutlet weak var headerNameLbl: UILabel!
    @IBOutlet weak var searchheaderBTN: UIButton!
    
}

class RecomendedCVCell: UICollectionViewCell{
    @IBOutlet weak var viewBg: BaseView!
    @IBOutlet weak var recomIMG: UIImageView!
    override func awakeFromNib() {
        self.recomIMG.layer.cornerRadius = 10
    }
}


// MARK: -  recommended collectionview
extension SearchProviderListCell: UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recomheadcount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RecomendedCVCell", for: indexPath) as? RecomendedCVCell
      // cell?.recomIMG.kf.setImage(with: URL(string: kImageUrl + (searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].recomments[indexPath.row].userId?.profilePic ?? "")),placeholder: UIImage(named: "profile_icon"))
        
        if self.searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].recomments[indexPath.row].userId?.profilePic == nil{
            print("profile is empty")
            cell?.recomIMG.kf.setImage(with: URL(string: kImageUrl + (searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].contactImage ?? "")),placeholder: UIImage(named: "profile_icon"))
        }else{
            print("profileimage")
            cell?.recomIMG.kf.setImage(with: URL(string: kImageUrl + (searchproviderlist?.Data[indexPath.section].contacts[indexPath.row].recomments[indexPath.row].userId?.profilePic ?? "")))
        }
        
        
        return cell!
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w:CGFloat = 20
        let h:CGFloat = 20
        return CGSize(width: w, height: h)
    }
}



