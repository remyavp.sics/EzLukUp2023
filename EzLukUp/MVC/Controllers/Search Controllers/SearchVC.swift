//
//  SearchVC.swift
//  EzLukUp
//
//  Created by REMYA V P on 01/02/23.
//

import UIKit
import Alamofire
import DropDown
import IQKeyboardManagerSwift

var SsearchArrRes = [getcatDataModel]()
var Ssearching = false
var Sselectedcategories = [String]()
var SselectedcategoriesID = [String]()
var SCatnameArray = [String]()
var Snewcatarray = [getcatDataModel]()
var Scategorylist : getCategoriesResponse?

var typedcity = ""
var typedstate = ""
var typedradius = "25 miles"

class SearchVC: UIViewController,searchdeletearraydelegate {
    func diddelete(index: Int) {
        let newindex = Snewcatarray.firstIndex{$0.name == Sselectedcategories[index]}
        Snewcatarray[newindex ?? 0].isselected = false
        Sselectedcategories.remove(at: index)
        categoryPopupTV.reloadData()
        searchCategoryTV.reloadData()
        
    }

  //MARK: - Outlets
  
    @IBOutlet weak var categoryPopupTV: UITableView!
    @IBOutlet var SearchCatPopupView: BaseView!
    @IBOutlet weak var searchCategoryTV: UITableView!
    @IBOutlet weak var stackview: UIStackView!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var countrystatecityTF: UITextField!
    @IBOutlet weak var radiusTF: UITextField!
    @IBOutlet weak var providercatresultTF: UITextField!
    @IBOutlet weak var searchBTN: UIButton!
    
    
  //Mark- Views
    @IBOutlet weak var providercatView: UIView!
    @IBOutlet weak var countryView: BaseView!
    @IBOutlet weak var stateView: BaseView!
    @IBOutlet weak var statesearchview: BaseView!
    @IBOutlet weak var citysearchview: BaseView!
    @IBOutlet weak var cscfinalview: UIView!
    @IBOutlet weak var cityview: BaseView!
    @IBOutlet weak var radiusview: BaseView!
    @IBOutlet weak var providercatfinalview: UIView!
    @IBOutlet weak var searchListTV: UITableView!
    @IBOutlet weak var stateTableView: UITableView!
    @IBOutlet weak var cityTableView: UITableView!
    @IBOutlet weak var radiusTableView: UITableView!
    
    @IBOutlet weak var cityTVviewheight: NSLayoutConstraint!
    @IBOutlet weak var stateTVviewheight: NSLayoutConstraint!
    @IBOutlet weak var radiusTVviewheight: NSLayoutConstraint!
    @IBOutlet weak var providerCatviewheight: NSLayoutConstraint!
    
    @IBOutlet weak var radiusBTN: UIButton!
    @IBOutlet weak var dropIMG: UIImageView!
    @IBOutlet weak var CountrymainviewH: BaseView!
    @IBOutlet weak var statemainviewH: UIView!
    @IBOutlet weak var citymainviewH: UIView!
    @IBOutlet weak var RadiusmainviewH: UIView!
    
    @IBOutlet weak var placeholderCat: UILabel!
    
    
    var countrylist : SearchCountryList?
    var statelist : SearchStateList?
    var statelistfilter : SearchStateList?
    var citylist : SearchCityList?
    
    var activityView: UIActivityIndicatorView?
    var dropstatus = false
    var token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
    var userid : String = UserDefaults.standard.value(forKey: "Kuserid") as! String
    
    let dropDown = DropDown()
    var radiusArray = ["25 miles","50 miles","75 miles","100 miles"]
    var isClickDown = false
    
    var hidecitystate = ""    
    var checktableheight : Bool = false
//    var typedcity = ""
//    var typedstate = ""
    var typedcountry = "USA"
    var state = ""
    var latitude = "38.00000000"
    var longitude = "-97.00000000"
    var selectedcategory = ""
    
    var searchArray = [[String:Any]]()
    var searching:Bool = false
    var statearray = [[String:Any]]()
    var SearchcityArray = [[String:Any]]()
    var cityarray = [[String:Any]]()
    var searchText = ""
    var typedcategory = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.placeholderCat.isHidden = false
        self.getcategoryapi()
        NotificationCenter.default.addObserver(self, selector: #selector(self.onContinueClicked(notification:)), name: Notification.Name("reloadcat"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.ontrue(notification:)), name: Notification.Name("true"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.onfalse(notification:)), name: Notification.Name("false"), object: nil)
        self.RadiusmainviewH.isHidden = true
        stateTF.delegate = self
        countryTF.delegate = self
        cityTF.delegate = self
        statelistapi()
        
        cityTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
        stateTF.keyboardToolbar.doneBarButton.setTarget(self, action: #selector(doneButtonClicked))
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let indexPath = categoryPopupTV.indexPathForSelectedRow {
            categoryPopupTV.deselectRow(at: indexPath, animated: true)
        }
        self.stateTF.text = typedstate
        self.cityTF.text = typedcity
        
        if self.cityTF.text == ""{
            self.radiusTF.text = ""
            self.RadiusmainviewH.isHidden = true
        }else{
            self.radiusTF.text = typedradius
            
            self.RadiusmainviewH.isHidden = false
        }
        statelistapi()
        if Sselectedcategories.count == 0{
        self.placeholderCat.isHidden = false
        }else{
            self.placeholderCat.isHidden = true
        }
        
    }
    
    @objc func onContinueClicked(notification: Notification) {
        categoryPopupTV.reloadData()
    }
    
    @objc func ontrue(notification: Notification) {
        Ssearching = true
    }
    
    @objc func onfalse(notification: Notification) {
        Ssearching = false
    }
    
    override func viewDidLayoutSubviews() {
        self.SearchCatPopupView.roundCorners(corners: [.topLeft, .topRight, .bottomLeft, .bottomRight], radius: 15)
    }
    
}
   
 //MARK: - Button actions
extension SearchVC{
    
    @IBAction func clearAllBTNTapped(_ sender: UIButton) {
        self.stateTF.text?.removeAll()
        self.cityTF.text?.removeAll()
        self.radiusTF.text?.removeAll()
        self.RadiusmainviewH.isHidden = true
        let tcell = searchCategoryTV.cellForRow(at: IndexPath(row: 0, section: 0)) as! SearchCategoryCell
        Sselectedcategories.removeAll()
        searchCategoryTV.reloadData()
        
    }
    
    @IBAction func searchBTNtapped(_ sender: UIButton) {
        print("btn tapped")
        
        if Sselectedcategories.isEmpty == true{
            showDefaultAlert(viewController: self, title: "Message", msg: "Please select a category")
        }else{
            
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "SearchProviderListVC") as! SearchProviderListVC
            print("\(cityTF.text ?? ""),\(stateTF.text ?? ""),\(countryTF.text ?? "")")
            
            print("city " + (cityTF.text ?? ""))
            if cityTF.text != ""{
                vc.getcountrystatecity = "\(cityTF.text ?? ""),"
            }
            if stateTF.text != ""{
                vc.getcountrystatecity = vc.getcountrystatecity + "\(stateTF.text ?? ""),"
            }
            if countryTF.text != ""{
                vc.getcountrystatecity = vc.getcountrystatecity + "\(countryTF.text ?? "")"
            }
            
            if Sselectedcategories.count > 0{
                vc.displycategory = Sselectedcategories[0]
            }else{
                vc.displycategory = ""
            }
            
            print(latitude)
            print(longitude)
                vc.getlat = latitude
                vc.getlong = longitude
            
                vc.getprocategory = SselectedcategoriesID
                vc.getstate = state
                vc.getcity = self.cityTF.text ?? ""
            
//            if self.cityTF.text == ""{
//                vc.getradius = ""
//            }else{
//                vc.getradius = typedradius
//
//            }
            
            if self.cityTF.text == ""{
                vc.getradius = ""
            }else{
                vc.getradius = self.radiusTF.text ?? ""
               
            }
            
            print("city " + (cityTF.text ?? ""))
                self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }

    @IBAction func dropdownBTNTapped(_ sender: UIButton) {
        dropstatus = !dropstatus
        self.searchCategoryTV.reloadSections([0], with: .none)
        if Sselectedcategories.count > 0{
        self.placeholderCat.isHidden = true
        }else{
            self.placeholderCat.isHidden = false
//            dropstatus = !dropstatus
//            self.searchCategoryTV.reloadSections([0], with: .none)
        }
        
        
    }
    
    @IBAction func radiusDropdownAction(_ sender: UIButton) {
        if checktableheight == false {
            checktableheight = true
            radiusTVviewheight.constant = CGFloat(self.radiusArray.count * 30)
            self.dropIMG.image = UIImage(named: "dropup_icon")
        }
        else{
            radiusTVviewheight.constant = 0
            checktableheight = false
            self.dropIMG.image = UIImage(named: "dropdown_icon")
        }
        self.radiusTableView.reloadData()
        
    }
}


//MARK: - Tableview Delegate & Datasource
extension SearchVC: UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if tableView == searchCategoryTV{
            return 1
        }else if tableView == stateTableView{
            return 1
        }else if tableView == cityTableView{
            return 1
        }else if tableView == radiusTableView{
            return 1
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == searchCategoryTV{
            return 1
        }
        else if tableView == stateTableView{
            print("searchcount",searchArray)
            //check search text & original text
            if( searching == true){
             return searchArray.count
            }else{
                return statearray.count
            }
        }
        else if tableView == cityTableView{
            print("searchcount",SearchcityArray)
            //check search text & original text
            if( searching == true){
             return SearchcityArray.count
            }else{
                return cityarray.count
            }
        }
        else if tableView == radiusTableView{
            return radiusArray.count
        }
        else{
            //check search text & original text
            if( Ssearching == true){
                return SsearchArrRes.count
            }else{
                return Snewcatarray.count
            }

        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == searchCategoryTV{
         //   if indexPath.section == 0{
            let cell = searchCategoryTV.dequeueReusableCell(withIdentifier: "SearchCategoryCell", for: indexPath) as! SearchCategoryCell
            cell.delegate = self
            cell.selectedArray = Sselectedcategories
            cell.CategoryTV.reloadData()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.01, execute: {
                
                if self.dropstatus {
                    cell.searchView.isHidden = false
                    self.placeholderCat.isHidden = true
                    self.providerCatviewheight.constant = 500
                    cell.searchViewHeight.constant = 40
                    cell.dropviewHeight.constant = 500
                    cell.dropview.isHidden = false
                    cell.dropimg.image = UIImage(named: "dropup_icon")
                    cell.dropview.addSubview(self.SearchCatPopupView)
                   
                }
                else{
                    self.providerCatviewheight.constant = 52
                    cell.dropviewHeight.constant = 0
                    cell.searchViewHeight.constant = 0
                    cell.searchView.isHidden = true
                    cell.dropview.isHidden = true
                    cell.dropimg.image = UIImage(named: "dropdown_icon")
                    //self.searchText.removeAll()
                }
                if Sselectedcategories.count == 1{
                    if self.dropstatus {
                        cell.catgoryviewHeight.constant = 83

                    }else {
                        cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11 + 8 )
                    }
                }
                else if Sselectedcategories.count > 1{
                    if self.dropstatus{
                        cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 40 + 11)
                    }
                    else{
                        cell.catgoryviewHeight.constant = CGFloat(cell.CategoryTV.contentSize.height + 11)
                    }
                }
                    else{
                        
                        cell.catgoryviewHeight.constant = 52
                        
                    }
                
            }
        )
         
            return cell
       // }
        }
        else if tableView == stateTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchStateTVCell") as! SearchStateTVCell
            
            //check search text & original text
//            if( searching == true){
//               // var dict = searchArray[indexPath.row]
//                cell.stateLbl.text = statelist?.Data[indexPath.row].name ?? ""
//
//            }else{
//               // var dict = statearray[indexPath.row]
//                cell.stateLbl.text = statelist?.Data[indexPath.row].name ?? ""
//               }
            
            if( searching == true){
                var dict = searchArray[indexPath.row]
                cell.stateLbl.text = dict["name"] as? String
            }else{
                var dict = statearray[indexPath.row]
                cell.stateLbl.text = dict["name"] as? String
               
            }
           
           
            return cell
        }
        else if tableView == cityTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCityTVCell") as! SearchCityTVCell
            
          //  cell.cityLbl.text = citylist?.Data[indexPath.row].name ?? ""
            if( searching == true){
                if SearchcityArray.count > 0{
                    
                    var dict = SearchcityArray[indexPath.row]
                    cell.cityLbl.text = dict["name"] as? String
                }
                
            }else{
                if cityarray.count > 0{
                    
                    var dict = cityarray[indexPath.row]
                    cell.cityLbl.text = dict["name"] as? String
                }
                
               
            }
           
            return cell
        }
        else if tableView == radiusTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchRadiusCell") as! SearchRadiusCell
            cell.radiusLbl.text = radiusArray[indexPath.row]
            return cell
        }
        
        
 //MARK: - category tableview cellforrowat
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPopupTVCell") as! SearchPopupTVCell
            print("status",Ssearching)

            if SsearchArrRes.count == 0 {
                print("search list contains , \(SsearchArrRes.count)")
                print("no of rows in sections = = \(SsearchArrRes.count)")
                cell.catLabel.text = "No category found"
                cell.isExclusiveTouch = false
            }else{
                
                if let index = SsearchArrRes.firstIndex(where: { $0.isselected  }) {
                    SsearchArrRes[index].isselected = false
                           }
                
                cell.catLabel.text = SsearchArrRes[indexPath.row].name ?? ""
                cell.catLabel.textColor = SsearchArrRes[indexPath.row].isselected ? UIColor(red: 0.259, green: 0.522, blue: 0.957, alpha: 1) : UIColor(red: 0.251, green: 0.251, blue: 0.251, alpha: 1)
                
            }
            
            return cell
        }
    }
 
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == searchCategoryTV{
                if Sselectedcategories.count == 1{
                    if self.dropstatus {
                        
                       // self.placeholderCat.isHidden = true
                        
                        return 83 + 9 + 500

                    }else {
                       return CGFloat((Sselectedcategories.count * 32) + 11 + 11)
//                        return UITableView.automaticDimension
                    }
                }
                else if Sselectedcategories.count > 1{
                    if self.dropstatus{
                        return CGFloat((Sselectedcategories.count * 32) + 40 + 11 + 9 + 500)
                    }
                    else{
                        return CGFloat((Sselectedcategories.count * 32) + 11 + 11)
//                        return UITableView.automaticDimension
                    }
                }
                    else{
                        if self.dropstatus {
                            return 52 + 500
                            
                        }
                     //   self.providerCatviewHeight.constant = 52
                        return 52

                    }
            
            return UITableView.automaticDimension
        }
        
        else if tableView == stateTableView{
            return 30
        }
        
        else if tableView == cityTableView{
            return 30
        }
        
        else if tableView == radiusTableView{
            return 30
        }
        
        else{
            return 45
        }
    }
   
 //MARK: - categorypopup tableview did select
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.placeholderCat.isHidden = true
        if tableView == categoryPopupTV{
            
            if let index = SsearchArrRes.firstIndex(where: { $0.isselected  }) {
                SsearchArrRes[index].isselected = false
                       }
            SsearchArrRes[indexPath.row].isselected = !SsearchArrRes[indexPath.row].isselected
            let index = Snewcatarray.firstIndex(where: { $0.id ==  SsearchArrRes[indexPath.row].id }) ?? 0
            Snewcatarray[index].isselected = SsearchArrRes[indexPath.row].isselected
            Sselectedcategories = Snewcatarray.filter{$0.isselected}.map{$0.name ?? ""}
            SselectedcategoriesID = Snewcatarray.filter{$0.isselected}.map{$0.id ?? ""}
          //  Sselectedcategories.append(selectedcategory)
            categoryPopupTV.reloadData()
            searchCategoryTV.reloadData()
           
        }
        
        else if tableView == stateTableView{
            if( searching == true){
                let dict = searchArray[indexPath.row]
                print("state name - ",dict["name"] as? String)
                searching = false
                
                stateTF.text = dict["name"] as? String
//                stateTF.resignFirstResponder()
                stateTVviewheight.constant = 0
                self.state = dict["isoCode"] as? String ?? ""
                self.cityarray.removeAll()
                self.cityTF.text?.removeAll()
                self.cityTableView.reloadData()
                self.RadiusmainviewH.isHidden = true
                self.citylistapi(stateiso: dict["isoCode"] as? String ?? "")
                
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
                
            }
            else{
               let dict = statearray[indexPath.row]
                print("state name - ",dict["name"] as? String)
                searching = false

                stateTF.text = dict["name"] as? String
                stateTVviewheight.constant = 0
                self.state = dict["isoCode"] as? String ?? ""
                self.cityarray.removeAll()
                self.cityTF.text?.removeAll()
                self.cityTableView.reloadData()
                self.RadiusmainviewH.isHidden = true
                self.citylistapi(stateiso: dict["isoCode"] as? String ?? "")
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
//                self.state = statelist?.Data[indexPath.row].isoCode ?? ""
//                self.citylistapi(stateiso: statelist?.Data[indexPath.row].isoCode ?? "")
//                self.latitude = statelist?.Data[indexPath.row].latitude ?? ""
//                self.longitude = statelist?.Data[indexPath.row].longitude ?? ""
                
            }
        }
        
        else if tableView == cityTableView{
            
            if( searching == true){
                let dict = SearchcityArray[indexPath.row]
                print("city name - ",dict["name"] as? String)
                searching = false
                
                cityTF.text = dict["name"] as? String
                cityTVviewheight.constant = 0
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
            }
            else{
                let dict = cityarray[indexPath.row]
                print("id -" ,dict["_id"] as? String)
                print("city name - ",dict["name"] as? String)
                searching = false
                
                cityTF.text = dict["name"] as? String
                cityTVviewheight.constant = 0
                self.latitude = dict["latitude"] as? String ?? ""
                self.longitude = dict["longitude"] as? String ?? ""
            }


//            cityTF.text = citylist?.Data[indexPath.row].name ?? ""
//        //    cityTF.resignFirstResponder()
//            cityTVviewheight.constant = 0
//            self.latitude = citylist?.Data[indexPath.row].latitude ?? ""
//            self.longitude = citylist?.Data[indexPath.row].longitude ?? ""
        }
        
        else if tableView == radiusTableView{
            radiusTF.text =  radiusArray[indexPath.row]
            typedradius = radiusArray[indexPath.row]
            radiusTF.resignFirstResponder()
            cityTVviewheight.constant = 0
           // typedradius = self.radiusTF.text ?? ""
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView == categoryPopupTV{
            
        }
    }

}

//MARK: - textfield delegates and functions
 extension SearchVC{
   
     func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == stateTF{
           
          //  searching = false
            
            if(searchText.count == 0){
                searching = false
                statearray.removeAll()
                statelistapi()
                stateTableView.reloadData()
            }
            
            self.stateTVviewheight.constant =  350
        } else if textField == cityTF{
            self.cityTVviewheight.constant =  350
        } else if textField == radiusTF{
            self.radiusTVviewheight.constant =  CGFloat(self.radiusArray.count * 30)
          //  self.radiusTF.text = "25 miles"
            self.radiusTableView.reloadData()
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == cityTF{
            typedcity = cityTF.text ?? ""
         //   cityTVviewheight.constant = 0
            if typedcity == ""{
                self.RadiusmainviewH.isHidden = true
            }else{
                self.radiusTF.text = "25 miles"
                typedradius = self.radiusTF.text ?? ""
                self.RadiusmainviewH.isHidden = false
               // self.radiusTF.text = "25 miles"
            }
           //
          //  self.placeholderCat.isHidden = false
        }else if textField == stateTF{
            typedstate = stateTF.text ?? ""
            stateTVviewheight.constant = 0
           // self.placeholderCat.isHidden = false
        }
        
        print("*\(countryTF.text ?? ""),\(stateTF.text ?? ""),\(cityTF.text ?? "")")
    
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == cityTF {
            typedcity = cityTF.text ?? ""
            
        }
        if textField == stateTF{
            typedstate = stateTF.text ?? ""
           // self.stateTVviewheight.constant = 350
        }
        print("*\(countryTF.text ?? ""),\(stateTF.text ?? ""),\(cityTF.text ?? "")")
  
        return true
    }

    
    @objc func doneButtonClicked(_ sender: Any) {
        typedcity = cityTF.text ?? ""
        typedstate = stateTF.text ?? ""
        searchText = ""
        typedradius = radiusTF.text ?? ""

        stateTVviewheight.constant = 0
        cityTVviewheight.constant = 0
       // self.placeholderCat.isHidden = false
        
     print("*\(countryTF.text ?? ""),\(stateTF.text ?? ""),\(cityTF.text ?? "")")
    }
   
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        searchText  = textField.text! + string
        print(searchText)
       
        if textField == stateTF{
            cityarray.removeAll()
            self.cityTF.text?.removeAll()
            self.RadiusmainviewH.isHidden = true
            cityTableView.reloadData()
            
            //add matching text to array
            searchArray = self.statearray.filter({(($0["name"] as! String).localizedCaseInsensitiveContains(searchText))})
            
            if(searchArray.count == 0){
                searching = false
//                statearray.removeAll()
//                statelistapi()
//                stateTableView.reloadData()
                //stateTableView.reloadData()
            }
            else{
                searching = true
            }
            print("remya")
            print(textField.text?.count)
            
//            if searchText == ""{
//                self.stateTVviewheight.constant = 0
//            }else{
                self.stateTVviewheight.constant = 350
                self.stateTableView.reloadData()
           // }
            
        }
        else if textField == cityTF{
            //input text
//            searchText  = textField.text! + string
//            print(searchText)
           // statelistapi(state: searchText)
            //add matching text to array
            SearchcityArray = self.cityarray.filter({(($0["name"] as! String).localizedCaseInsensitiveContains(searchText))})
            
            if(SearchcityArray.count == 0){
                searching = false
            }else{
                searching = true
            }
            self.cityTVviewheight.constant = 350
            self.cityTableView.reloadData();
        }
        
        
        return true
    }
    
}

 
//MARK: SetUpForDropDown
extension SearchVC{
    func radiusDropdown(){
        dropDown.anchorView = radiusTF
        dropDown.dataSource = radiusArray
        dropDown.bottomOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.topOffset = CGPoint(x: 0, y: (dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.direction = .bottom
            dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
              print("Selected item: \(item) at index: \(index)")
                self.radiusTF.text = radiusArray[index]
        }
     }
}


//MARK: - API's
extension SearchVC{
    
 //MARK: - category api
        func getcategoryapi() {
         showActivityIndicator()
            let url = "http://13.234.177.61/api7/getCategories"
            let token : String = UserDefaults.standard.value(forKey: "Ktoken") as! String
           
            AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.prettyPrinted,headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
             //   print(response)
                switch (response.result) {
                           case .success( let JSON):
                            if let responsedata =  JSON as? [String:Any]  {
                                 hideActivityIndicator()
                                Scategorylist = getCategoriesResponse(from: responsedata)

                                
                               Snewcatarray = Scategorylist?.Data ?? []
                                DispatchQueue.main.async {
//                                    if let categories = UserDefaults.standard.value(forKey: "selectedIDs") as? [String]{
//                                        Snewcatarray.forEach({ $0.isselected = categories.contains($0.id ?? "") })
//                                        Sselectedcategories = Snewcatarray.filter({ $0.isselected }).map({ $0.name ?? "" })
//                                    }
                                    SsearchArrRes = Snewcatarray
                                    print(Snewcatarray)
                                    self.searchCategoryTV.reloadData()
                                }
                               }
                   
               
                            case .failure(let error):
                               print("Request error: \(error.localizedDescription)")
                        }
            }
        }
  
    
 //MARK: - Country,State,City API
    func countrylistapi(){
    showActivityIndicator()
  
     let url = "http://13.234.177.61/api7/countriesList"
    AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result)      {
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                 self.countrylist = SearchCountryList(from:responsedata)
               //  self.servicestableview.reloadData()
                      
             }
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
  }
 
//MARK: -state
    func statelistapi(){
   // showActivityIndicator()
  
        let params: [String : Any] = [
                      "country_iso_code": "US"]
    
     let url = "http://13.234.177.61/api7/statesList"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
        print(response)
         switch (response.result)      {
             
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                 self.statelist = SearchStateList(from:responsedata)
                 if let dataa = responsedata["data"] as? [[String:Any]]{
//                        print("dataa = ", dataa)
                    
                     for i in dataa{
                         print("looped data =",i)
                         self.statearray.append(i)
                     }
                 }
             }
                 
                 self.stateTableView.reloadData()
                      
           
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
  }

//MARK: -city
    func citylistapi(stateiso:String){
 //   showActivityIndicator()
  
        let params: [String : Any] = [
                      "country_iso_code": "US",
                      "state_iso_code": stateiso]
    
     let url = "http://13.234.177.61/api7/citiesList"
    AF.request(url, method: .post, parameters: params, encoding: JSONEncoding.prettyPrinted, headers: ["x-access-token":token]).validate(statusCode: 200..<510) .responseJSON { [self] response in
         switch (response.result)      {
           case .success( let JSON):
             
             if let responsedata =  JSON as? [String:Any]  {
                 print("responsedata",responsedata)
                // self.citylist = SearchCityList(from:responsedata)
                 if let dataa = responsedata["data"] as? [[String:Any]]{

                     for i in dataa{
                         print("looped data =",i)
                         self.cityarray.append(i)
                     }
                 }
             }
             
                 self.cityTableView.reloadData()
             
         case .failure(let error):
             print("Request error: \(error.localizedDescription)")
         }
     }
}
    
}

extension SearchVC{
    func showActivityIndicator() {
        activityView = UIActivityIndicatorView(style: .large)
        activityView?.center = self.view.center
        self.view.addSubview(activityView!)
        activityView?.startAnimating()
    }

    func hideActivityIndicator(){
        if (activityView != nil){
            activityView?.stopAnimating()
        }
    }
}

class Scat
{
    var category = ""
    var isselected = false
    init(data:String)
    {
        self.category = data
    }
}

class SearchPopupTVCell: UITableViewCell{
    @IBOutlet weak var catLabel: UILabel!
}

class SearchStateTVCell: UITableViewCell{
    @IBOutlet weak var stateLbl: UILabel!
}

class SearchCityTVCell: UITableViewCell{
    @IBOutlet weak var cityLbl: UILabel!
}

class SearchRadiusCell: UITableViewCell{
    @IBOutlet weak var radiusLbl: UILabel!
}


protocol searchdeletearraydelegate{
    func diddelete(index:Int)
}

//class searchuserdetails{
//    var name = ""
//    var searchtext = ""
//    func saveuserdetail(name:String,searchtext:String){
//        self.name = name
//        self.searchtext = searchtext
//    }
//    func readuserdtail() -> (String,String){
//        return (self.name,self.searchtext)
//    }
//}
