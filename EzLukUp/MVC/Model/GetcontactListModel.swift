
//  GetcontactListModel.swift
//  AlamofireTest
//
//  Created by Srishti on 26/11/22.
//

import Foundation

class getcontactResponse{
    var status : Bool?
    var Data : getcontactDataModel?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            
            self.Data = getcontactDataModel(from: dataarray)
            
        }
        
    }
}
class getcontactDataModel{
    var normalList :[getcontactDataListModel] = []
    var normalListCount = 0
    var apinormalList :[contactListModel] = []
    var taggedContact :[contactListModel] = []
    var taggedContactCount = 0
    var possibleProviders:[contactListModel] = []
    var possibleProvidersCount = 0
    init(from data : [String:Any]) {
        if let normallist = data["normalList"] as? [[String:Any]]{
            for contact in normallist{
                self.apinormalList.append(contactListModel(from: contact))
            }
        }
        if let taggedcontact = data["taggedContact"] as? [[String:Any]]{
            for contact in taggedcontact{
                self.taggedContact.append(contactListModel(from: contact))
            }
        }
        if let possibleproviders = data["possibleProviders"] as? [[String:Any]]{
            for contact in possibleproviders{
                self.possibleProviders.append(contactListModel(from: contact))
            }
        }
        self.normalListCount = data["normalListCount"] as? Int ?? 0
        self.possibleProvidersCount = data["possibleProvidersCount"] as? Int ?? 0
        self.taggedContactCount = data["taggedContactCount"] as? Int ?? 0
    }
    func gettotalcount() -> Int{
        return self.normalListCount + self.possibleProvidersCount + self.taggedContactCount
    }
    func gettotalcountfromlist() -> Int{
        return self.possibleProviders.count + self.normalList.count + self.taggedContact.count
    }
    init(normalList :[getcontactDataListModel],taggedContact :[contactListModel],possibleProviders:[contactListModel]){
        self.normalList = normalList
        self.taggedContact = taggedContact
        self.possibleProviders = possibleProviders
    }
}
class getcontactDataListModel: Equatable{
    static func == (lhs: getcontactDataListModel, rhs: getcontactDataListModel) -> Bool {
        return true
    }
    
    var sectionOpened : Bool = true
    var Headername : String?
    var contactsList : [contactListModel] = []
    init(headername: String, contacts: [contactListModel]) {
        self.Headername = headername
        self.contactsList = contacts
        self.sectionOpened = true
    }
}
class contactListModel{
    var sectionOpened : Bool = true
    var  id :String?
    var  categoryIds : [String]?
    var  contactUserId :String?
    var city: String?
    var stateShortCode :String?
    var  notInUs: Bool?
    var  isRecommended :Int?
    var  isReferred :Bool?
    var  isUser :Bool?
    var  name :String?
    var countryCode : String?
    var  phoneNumber :String?
    var  twilioCallerName :String?
    var  twilioCarrierType :String?
    var  phoneNumberLocation : phoneNumberLocationModel?
    var  type :String?
    var  userId :String?
    var  contactImage:String?
    var recommendedCount :Any?
    var areaOfService : [String] = []
    var taggedCategoryIds : [taggedcatlist] = []
    
    
    init(from data : [String:Any]) {
        self.id = data["_id"] as? String
        self.categoryIds = data["categoryIds"] as? [String]
        self.contactUserId = data["contactUserId"] as? String
        self.contactImage = data["contactImage"] as? String
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        self.isRecommended = data["isRecommended"] as? Int
//        self.recommendedCount = data["recommendedCount"] as?
        if let count = data["recommendedCount"] as? String{
            self.recommendedCount = count
        }
        else {
            self.recommendedCount = data["recommendedCount"] as? Int
        }
        self.notInUs = data["notInUs"] as? Bool
        self.isReferred = data["isReferred"] as? Bool
        self.isUser = data["isUser"] as? Bool
        self.name = data["name"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.twilioCallerName = data["twilioCallerName"] as? String
        self.twilioCarrierType = data["twilioCarrierType"] as? String
        self.type = data["type"] as? String
        self.userId = data["userId"] as? String
        if let colorData = data["phoneNumberLocation"] as? [String:Any]{
            self.phoneNumberLocation = phoneNumberLocationModel(fromData: colorData)
        }
        if let areaofservice = data["areaOfService"] as? [[String]]{
            self.areaOfService = areaofservice.first ?? []
        }
//        if let tagData = data["taggedCategoryIds"] as? [String:Any]{
//            self.taggedCategoryIds = taggedcatlist(fromData: tagData)
//        }
        if let tagges = data["taggedCategoryIds"] as? [[String:Any]]{
            for contact in tagges{
                self.taggedCategoryIds.append(taggedcatlist(fromData: contact))
            }
        }
    }
}

class phoneNumberLocationModel{
    var id : String!
    var area_code : String!
    var city : String!
    var city_short_code : String!
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.area_code = data["area_code"] as? String
        self.city = data["city"] as? String
        self.city_short_code = data["city_short_code"] as? String
    }
}

class taggedcatlist{
    var id :String?
    var categoryImage :String?
    var name :String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.categoryImage = data["categoryImage"] as? String
    }
   
}


//MARK: - Provider models
class providerResponseModel {
    var status : Bool?
    var Data : [providerListModel] = []
    var count: Int?
    init(fromData data: [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.count = data["categoryCount"] as? Int ?? 0
        if let providercontactlist = data["data"] as? [[String:Any]]{
            for contact in providercontactlist{
                self.Data.append(providerListModel(fromData: contact))
            }
        }
    }
}

class providerListModel{
    var sectionOpened : Bool = true
    var id :String?
    var name :String?
    var categoryImage :String?
    var providercontacts : [providerlist] = []
    init(fromData data: [String:Any]) {
        self.sectionOpened = true
        self.id = data["id"] as? String
        self.name = data["name"] as? String
        self.categoryImage = data["categoryImage"] as? String
        if let providercontactlist = data["contacts"] as? [[String:Any]]{
            for contact in providercontactlist{
                self.providercontacts.append(providerlist(fromData: contact))
            }
        }
    }
}

class providerlist{
    var sectionOpened : Bool = true
    var id :String?
    var userId :String?
    var name :String?
    var contactImage :String?
    var phoneNumber :String?
    var countryCode : String?
    var isUser :Bool?
    var contactUserId :String?
    var contactFullPhoneNumber :String?
    var isReferred :Bool?
    var type :String?
    var isRecommended :Bool?
    var categoryIds :[String]?
    var phoneNumberLocation :String?
    var areaCode :String?
    var city :String?
    var stateShortCode :String?
    var twilioCallerName :String?
    var twilioCallerType :String?
    var twilioCarrierName :String?
    var twilioCarrierType :String?
    var possibleProvider :Bool?
    var markedAsGeneralConatct :Bool?
    var areaOfService : [String] = []
    var recomended :[recomendedlist] = []
    var contact_recomend_status :[recomendstatuslist] = []
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.contactImage = data["contactImage"] as? String
        self.userId = data["userId"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.countryCode = data["countryCode"] as? String
        self.isUser = data["isUser"] as? Bool ?? false
        self.contactUserId = data["contactUserId"] as? String
        self.contactFullPhoneNumber = data["contactFullPhoneNumber"] as? String
        self.isReferred = data["isReferred"] as? Bool ?? false
        self.type = data["type"] as? String
        self.isRecommended = data["isRecommended"] as? Bool ?? false
        self.categoryIds = data["categoryIds"] as? [String]
        self.phoneNumberLocation = data["phoneNumberLocation"] as? String
        self.areaCode = data["areaCode"] as? String
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        self.twilioCallerName = data["twilioCallerName"] as? String
        self.twilioCallerType = data["twilioCallerType"] as? String
        self.twilioCarrierName = data["twilioCarrierName"] as? String
        self.twilioCarrierType = data["twilioCarrierType"] as? String
        self.possibleProvider = data["possibleProvider"] as? Bool ?? false
        self.markedAsGeneralConatct = data["markedAsGeneralConatct"] as? Bool ?? false
        if let areaofservice = data["areaOfService"] as? [[String]]{
            self.areaOfService = areaofservice.first ?? []
        }
        if let recomendedcontactlist = data["recomended"] as? [[String:Any]]{
            for contact in recomendedcontactlist{
                self.recomended.append(recomendedlist(fromData: contact))
            }
        }
        if let recomendedstatus = data["contact_recomend_status"] as? [[String:Any]]{
            for contact in recomendedstatus{
                self.contact_recomend_status.append(recomendstatuslist(fromData: contact))
            }
        }
        
    }
}

class recomendedlist{
    var id :String?
    var userId :String?
    var name :String?
    var contactImage :String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.userId = data["userId"] as? String
        self.name = data["name"] as? String
        self.contactImage = data["contactImage"] as? String
    }
}

class recomendstatuslist{
    var totalcount :Int?
    init(fromData data: [String:Any]) {
        self.totalcount = data["totalcount"] as? Int
    }
}
