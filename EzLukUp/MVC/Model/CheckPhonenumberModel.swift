//
//  CheckPhonenumberModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 19/10/22.
//

import Foundation
public class CheckPhonenumberModel {
    public var message : String?
    public var user : User?
    public var status : Bool?

    public class func modelsFromDictionaryArray(array:NSArray) -> [CheckPhonenumberModel]
    {
        var models:[CheckPhonenumberModel] = []
        for item in array
        {
            models.append(CheckPhonenumberModel(dictionary: item as! NSDictionary)!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary) {

        message = dictionary["message"] as? String
        if (dictionary["user"] != nil) { user = User(dictionary: dictionary["user"] as! NSDictionary) }
        status = dictionary["status"] as? Bool
    }

    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self.message, forKey: "message")
        dictionary.setValue(self.user?.dictionaryRepresentation(), forKey: "user")
        dictionary.setValue(self.status, forKey: "status")

        return dictionary
    }

}

public class User {
    public var _id : String?
    public var userId : String?
    public var name : String?
    public var phoneNumber : String?
    public var isUser : Bool?
    public var isReferred : Bool?
    public var type : String?
    public var feedback : String?
    public var categoryIds : Array<String>?
    public var areaOfService : Array<String>?
    public var predefinedResponseStatus : Bool?
    public var createdAt : String?
    public var updatedAt : String?
    public var __v : Int?

    public class func modelsFromDictionaryArray(array:NSArray) -> [User]
    {
        var models:[User] = []
        for item in array
        {
            models.append(User(dictionary: item as! NSDictionary)!)
        }
        return models
    }

    required public init?(dictionary: NSDictionary) {

        _id = dictionary["_id"] as? String
        userId = dictionary["userId"] as? String
        name = dictionary["name"] as? String
        phoneNumber = dictionary["phoneNumber"] as? String
        isUser = dictionary["isUser"] as? Bool
        isReferred = dictionary["isReferred"] as? Bool
        type = dictionary["type"] as? String
        feedback = dictionary["feedback"] as? String
        
        if (dictionary["categoryIds"] != nil) { categoryIds = CategoryIds.modelsFromDictionaryArray(array: dictionary["categoryIds"] as! NSArray) }
        if (dictionary["areaOfService"] != nil) { areaOfService = AreaOfService.modelsFromDictionaryArray(array: dictionary["areaOfService"] as! NSArray) }
        predefinedResponseStatus = dictionary["predefinedResponseStatus"] as? Bool
        createdAt = dictionary["createdAt"] as? String
        updatedAt = dictionary["updatedAt"] as? String
        __v = dictionary["__v"] as? Int
    }

        
    public func dictionaryRepresentation() -> NSDictionary {

        let dictionary = NSMutableDictionary()

        dictionary.setValue(self._id, forKey: "_id")
        dictionary.setValue(self.userId, forKey: "userId")
        dictionary.setValue(self.name, forKey: "name")
        dictionary.setValue(self.phoneNumber, forKey: "phoneNumber")
        dictionary.setValue(self.isUser, forKey: "isUser")
        dictionary.setValue(self.isReferred, forKey: "isReferred")
        dictionary.setValue(self.type, forKey: "type")
        dictionary.setValue(self.feedback, forKey: "feedback")
        dictionary.setValue(self.predefinedResponseStatus, forKey: "predefinedResponseStatus")
        dictionary.setValue(self.createdAt, forKey: "createdAt")
        dictionary.setValue(self.updatedAt, forKey: "updatedAt")
        dictionary.setValue(self.__v, forKey: "__v")

        return dictionary
    }

}
