//
//  GetUserDetailsModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 12/12/22.
//

import Foundation

class getUserDetailsResponse{
    var status : Bool?
    var Data : getUserDataModel?
    var Feedback : [getFeedbackModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = getUserDataModel(fromData: dataarray)
        }
        if let feedbacklist = data["feedback"] as? [[String:Any]]{
            for feedbacks in feedbacklist{
                self.Feedback.append(getFeedbackModel(fromData: feedbacks))
            }
        }
    }
}

class getUserDataModel{
    var id : String?
    var fullName : String?
    var refPhoneNumber : String?
    var countryCode : String?
    var phoneNumber : String?
    var type : String?
    var city : String?
    var state : String?
    var aboutBusiness : String?
    var isSignup : Bool?
    var categoryIds : [categorieslist] = []
    var serviceDetails : [servicelist] = []
    var profilePic : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.refPhoneNumber = data["refPhoneNumber"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.type = data["type"] as? String
        self.city = data["city"] as? String
        self.state = data["state"] as? String
        self.aboutBusiness = data["aboutBusiness"] as? String
        self.isSignup = data["isSignup"] as? Bool ?? false
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(categorieslist(fromData: cat))
            }
        }
        if let servicedetaillist = data["serviceDetails"] as? [[String:Any]]{
            for service in servicedetaillist{
                self.serviceDetails.append(servicelist(fromData: service))
            }
        }
        self.profilePic = data["profilePic"] as? String
    }
}

class categorieslist{
    var id : String?
    var name : String?
    var categoryImage : String?
    init(fromData data: [String:Any]) {
    self.id = data["_id"] as? String
    self.name = data["name"] as? String
    self.categoryImage = data["categoryImage"] as? String
}
}

class servicelist{
    var url : String?
    var title : String?
    var description : String?
    init(fromData data: [String:Any]) {
        self.url = data["url"] as? String
        self.title = data["title"] as? String
        self.description = data["description"] as? String
    }
    
}
class getFeedbackModel{
    var id : String?
    var providerId : String?
    var userId : userlist?
    var feedback : String?
    var createdAt : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.providerId = data["providerId"] as? String
        if let userarray = data["userId"] as? [String:Any]{
            self.userId = userlist(fromData: userarray)
        }
        self.feedback = data["feedback"] as? String
        self.createdAt = data["createdAt"] as? String
    }
    
}

class userlist{
    var id : String?
    var fullName : String?
    var profilePic : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.profilePic = data["profilePic"] as? String
    }
}
