//
//  GetFeedbackTypesModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 09/03/23.
//

import Foundation

class FeedbackTypesList{
    var status : Bool?
    var Data : [DataList] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datalist = data["data"] as? [[String:Any]]{
            for datas in datalist{
                self.Data.append(DataList(from: datas))
            }
        }
    }
}

class DataList{
    var id : String?
    var type : String?
    var status : String?
    init(from data : [String:Any]) {
        self.id = data["_id"] as? String
        self.type = data["type"] as? String
        self.status = data["status"] as? String
    }
}
