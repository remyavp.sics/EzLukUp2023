//
//  LoginModels.swift
//  EzLukUp
//
//  Created by Srishti on 14/09/22.
//

import Foundation

class getchecknumberResponse{
    var message : String?
    var status : Bool?
    var token : String?
    var Data : userDataModel?
    var Referer : refererDataModel?
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.token = data["token"] as? String
        self.message = data["message"] as? String
        if let dataarray = data["user"] as? [String:Any]{
            self.Data = userDataModel(fromData: dataarray)
        }
        if let refererarray = data["referer"] as? [String:Any]{
            self.Referer = refererDataModel(fromData: refererarray)
        }
        }
    }

class userDataModel{
    var id : String?
    var fullName : String?
    var countryCode: String?
    var phoneNumber : String?
    var refPhoneNumber : String?
    var type : String?
    var refCountryCode : String?
    var categoryIds : [categorylists] = []
    var tipstatus : tipstatuslist?
    init(fromData data : [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.countryCode = data["countryCode"] as? String
        self.refPhoneNumber = data["refCountryCode"] as? String
        self.refCountryCode = data["refCountryCode"] as? String
        self.type = data["type"] as? String
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(categorylists(fromData: cat))
            }
        }
        if let tipstat = data["tooltip"] as? [String:Any]{
            self.tipstatus = tipstatuslist(fromData: tipstat)
        }
    }
    
}

class tipstatuslist{
    var contactswipe : Bool?
    var contactswipeprovider : Bool?
    init(fromData data :[String:Any]) {
        self.contactswipe = data["contactswipe"] as? Bool ?? false
        self.contactswipeprovider = data["contactswipeprovider"] as? Bool ?? false
    }
        
}

class categorylists{
    var id : String?
    var name : String?
    init(fromData data : [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
    }
}

class refererDataModel{
    var userId : userIdModel?
    init(fromData data : [String:Any]) {
        if let dataarray = data["userId"] as? [String:Any]{
            self.userId = userIdModel(fromData: dataarray)
        }
        }
}
class userIdModel{
    var fullName : String?
    init(fromData data : [String:Any]) {
        self.fullName = data["fullName"] as? String
    }
}

class loginapiResponseModel{
    var Data : loginDataModel?
    var Tocken : String?
    var Status: Bool?
    init(from data : [String:Any]) {
        if let dataa = data["data"] as? [String:Any]{
            self.Data = loginDataModel(fromData: dataa)
        }
        self.Tocken = data["token"] as? String ?? ""
        self.Status = data["status"] as? Bool ?? false
    }
}

class loginDataModel{
//    var PrivacySettings : privacyModel?
    var id : String?
    var fullName : String?
    var refPhoneNumber : String?
    var countryCode : String?
    var phoneNumber : String?
    var type : String?
    var city : String?
    var state : String?
    var aboutBusiness : String?
    var isSignup : Bool?
    var categoryIds : [categorieslist] = []
    var serviceDetails : [servicelist] = []
    var tipstatus : tipstatuslist?
    var profilePic : String?
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.fullName = data["fullName"] as? String
        self.refPhoneNumber = data["refPhoneNumber"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.type = data["type"] as? String
        self.city = data["city"] as? String
        self.state = data["state"] as? String
        self.aboutBusiness = data["aboutBusiness"] as? String
        self.isSignup = data["isSignup"] as? Bool ?? false
        if let catlist = data["categoryIds"] as? [[String:Any]]{
            for cat in catlist{
                self.categoryIds.append(categorieslist(fromData: cat))
            }
        }
        if let servicedetaillist = data["serviceDetails"] as? [[String:Any]]{
            for service in servicedetaillist{
                self.serviceDetails.append(servicelist(fromData: service))
            }
        }
        if let tipstat = data["tooltip"] as? [String:Any]{
            self.tipstatus = tipstatuslist(fromData: tipstat)
        }
        self.profilePic = data["profilePic"] as? String
    }
}

class privacyModel{
       var addressVisible : Bool?
       var calls : Bool?
       var canMessage : Bool?
       var canTag : Bool?
       var syncContacts : Bool?
       var textMessage : Bool?
       var userDetailsVisible : Bool?
    init(fromData data: [String:Any]){
        self.addressVisible = data["addressVisible"] as? Bool ?? false
        self.calls = data["calls"]  as? Bool ?? false
        self.canMessage = data["canMessage"] as? Bool ?? false
        self.canTag = data["canTag"]  as? Bool ?? false
        self.syncContacts = data["syncContacts"] as? Bool ?? false
        self.textMessage = data["textMessage"] as? Bool ?? false
        self.userDetailsVisible = data["userDetailsVisible"] as? Bool ?? false
    }
}



class getCategoriesResponse{
    var status : Bool?
    var Data : [getcatDataModel] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [[String:Any]]{
            for catlist in dataarray{
                self.Data.append(getcatDataModel(from: catlist))
            }
        }
        
    }
}
class getcatDataModel{
    var isselected = false
    var id : String?
    var name : String?
    var categoryImage : String?
    init(from data : [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.categoryImage = data["categoryImage"] as? String
    }
}
//class getcatDataModel{
//    var isselected : Bool?
//    var id : String?
//    var name : String?
//    var categoryImage : String?
//    init(from data : [String:Any]) {
//        self.id = data["_id"] as? String
//        self.name = data["name"] as? String
//        self.categoryImage = data["categoryImage"] as? String
//    }
//}
//Dashboard models
class DashboardDataModel{
    var status :Bool?
    var Data : dashboarddatas?
    init(from data : [String:Any]){
        self.status = data["status"] as? Bool ?? false
        if let dataarray = data["data"] as? [String:Any]{
            self.Data = dashboarddatas(from : dataarray)
        }
        
    }
}
class dashboarddatas{
    var rewardPoints : Int?
    var genralContactsCount : Int?
    var serviceProviderContactsCount : Int?
    var recentRecommendProviders : [recentContactdict] = []
    var recentlyJoinedUsers : [recentlyJoinedUserslist] = []
    var recentFeedback : [recentFeedbacklist] = []
    init(from data : [String:Any]){
        self.rewardPoints = data["rewardPoints"] as? Int
        self.genralContactsCount = data["genralContactsCount"] as? Int
        self.serviceProviderContactsCount = data["serviceProviderContactsCount"] as? Int
        if let recentRecommend = data["recentRecommendProviders"] as? [[String:Any]]{
            for provider in recentRecommend{
                self.recentRecommendProviders.append(recentContactdict(from: provider))
            }
        }
        if let recentlyJoinedUsers = data["recentlyJoinedUsers"] as? [[String:Any]]{
            for user in recentlyJoinedUsers{
                self.recentlyJoinedUsers.append(recentlyJoinedUserslist(from: user))
            }
        }
        if let recentFeedbacks = data["recentFeedback"] as? [[String:Any]]{
            for feedback in recentFeedbacks{
                self.recentFeedback.append(recentFeedbacklist(from: feedback))
            }
        }
    }
}
class recentContactdict{
    var contacts : recentrecomendationList?
    init(from data: [String:Any])
    {
        if let dataarray = data["contacts"] as? [String:Any]{
            self.contacts = recentrecomendationList(from : dataarray)
        }
    }
}

class recentrecomendationList{
       var id: String?
       var userId:  String?
       var name:  String?
       var countryCode: String?
       var phoneNumber:  String?
       var contactImage:  String?
       var isUser: Bool?
       var contactUserId:  String?
       var contactFullPhoneNumber: String?
       var isReferred: Bool?
       var type:  String?
       var isRecommended:Bool?
//       var categoryIds: []
       var phoneNumberLocation:  String?
       var areaCode:  String?
       var city: String?
       var stateShortCode: String?
       var twilioCallerName: String?
       var taggedAsProvider: Bool?
//       var taggedCategoryIds:
//           "6343cf7cd337becb4e365bc7"
       var possibleProvider:Bool?
       var recommendedCount: Int?
       var markedAsGeneralConatct:Bool?
       var notInUs: Bool?
       var isDeleted: Bool?
//       var areaOfService: []
       var createdAt:  String?
       var updatedAt: String?
    init(from data:[String:Any]){
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        self.contactImage = data["contactImage"] as? String
        self.createdAt = data["createdAt"] as? String
    }
}
class recentFeedbacklist{
    var id:  String?
    var feedback:  String?
    var createdAt:  String?
    init(from data:[String:Any]){
        self.id = data["_id"] as? String
        self.feedback = data["feedback"] as? String
        self.createdAt = data["createdAt"] as? String
    }
}
class recentlyJoinedUserslist{
    var id:  String?
    var fullName:  String?
    var profilePic:   String?
    var city:   String?
    var state:  String?
    init(from data:[String:Any]){
        self.id = data["_id"] as? String
    }
}

