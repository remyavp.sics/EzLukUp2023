//
//  SearchDetailsModel.swift
//  EzLukUp
//
//  Created by REMYA V P on 06/02/23.
//

import Foundation

class SearchCountryList{
    var status : Bool?
    var Data : [CountryList] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datalist = data["data"] as? [[String:Any]]{
            for datas in datalist{
                self.Data.append(CountryList(from: datas))
            }
        }
    }
}

class CountryList{
    var name : String?
    var isoCode : String?
    var latitude : String?
    var longitude : String?
    init(from data : [String:Any]) {
        self.name = data["name"] as? String
        self.isoCode = data["isoCode"] as? String
        self.latitude = data["latitude"] as? String
        self.longitude = data["longitude"] as? String
    }
}

class SearchStateList{
    var status : Bool?
    var Data : [StateList] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datalist = data["data"] as? [[String:Any]]{
            for datas in datalist{
                self.Data.append(StateList(from: datas))
            }
        }
    }
}

class StateList{
    var name : String?
    var isoCode : String?
    var countryCode : String?
    var latitude : String?
    var longitude : String?
    init(from data : [String:Any]) {
        self.name = data["name"] as? String
        self.isoCode = data["isoCode"] as? String
        self.countryCode = data["countryCode"] as? String
        self.latitude = data["latitude"] as? String
        self.longitude = data["longitude"] as? String
    }
}

class SearchCityList{
    var status : Bool?
    var Data : [CityList] = []
    init(from data : [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        if let datalist = data["data"] as? [[String:Any]]{
            for datas in datalist{
                self.Data.append(CityList(from: datas))
            }
        }
    }
}

class CityList{
    var name : String?
    var stateCode : String?
    var countryCode : String?
    var latitude : String?
    var longitude : String?
    init(from data : [String:Any]) {
        self.name = data["name"] as? String
        self.stateCode = data["stateCode"] as? String
        self.countryCode = data["countryCode"] as? String
        self.latitude = data["latitude"] as? String
        self.longitude = data["longitude"] as? String
    }
}


class SearchProviderListResponse{
    var status : Bool?
    var message : String?
    var Data : [SearchProviderModel] = []
    var providers : [String]?
    init(fromData data: [String:Any]) {
        self.status = data["status"] as? Bool ?? false
        self.message = data["message"] as? String
        self.providers = data["providers"] as? [String]
        if let datalist = data["data"] as? [[String:Any]]{
            for datas in datalist{
                self.Data.append(SearchProviderModel(fromData: datas))
            }        
        }
    }
}
  
class SearchProviderModel{
    var sectionOpened : Bool = true
    var header : String?
    var contactsCount : Int?
    var contacts : [ContactList] = []
    init(fromData data : [String:Any]) {
        self.header = data["header"] as? String
        self.contactsCount = data["contactsCount"] as? Int
        self.sectionOpened = true
        if let contactlist = data["contacts"] as? [[String:Any]]{
            for contacts in contactlist{
                self.contacts.append(ContactList(fromData: contacts))
            }
        }
    }
}

class ContactList{
    var sectionOpened : Bool = true
    var id : String?
    var userId : String?
    var name : String?
    var countryCode : String?
    var phoneNumber : String?
    var contactImage : String?
    var isUser : Bool?
    var contactUserId : String?
    var isReferred : Bool?
    var type : String?
    var isRecommended : Bool?
    var categoryIds :[String]?
    var twilioCallerName : String?
    var areaOfService : [areaOfServicelist] = []
    var city : String?
    var stateShortCode : String?
    var recomments : [recommentlist] = []
    init(fromData data: [String:Any]) {
        self.id = data["_id"] as? String
        self.userId = data["userId"] as? String
        self.name = data["name"] as? String
        self.countryCode = data["countryCode"] as? String
        self.phoneNumber = data["phoneNumber"] as? String
        self.contactImage = data["contactImage"] as? String
        self.isUser = data["isUser"] as? Bool ?? false
        self.contactUserId = data["contactUserId"] as? String
        self.isReferred = data["isReferred"] as? Bool ?? false
        self.type = data["type"] as? String
        self.isRecommended = data["isRecommended"] as? Bool ?? false
        self.categoryIds = data["categoryIds"] as? [String]
        self.twilioCallerName = data["twilioCallerName"] as? String
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        if let arealist = data["areaOfService"] as? [[String:Any]]{
            for area in arealist{
                self.areaOfService.append(areaOfServicelist(fromData: area))
            }
        }
        if let contactlist = data["recomments"] as? [[String:Any]]{
            for recom in contactlist{
                self.recomments.append(recommentlist(fromData: recom))
            }
        }
    }
}

class areaOfServicelist{
    var country : String?
    var city : String?
    var stateShortCode : String?
    var radius : String?
    init(fromData data : [String:Any]) {
        self.country = data["country"] as? String
        self.city = data["city"] as? String
        self.stateShortCode = data["stateShortCode"] as? String
        self.radius = data["radius"] as? String
    }
}

class recommentlist{
    var id : String?
    var name : String?
    var userId : searchuserlist?
    init(fromData data : [String:Any]) {
        self.id = data["_id"] as? String
        self.name = data["name"] as? String
        if let dataarray = data["userId"] as? [String:Any]{
            self.userId = searchuserlist(fromData: dataarray)
        }
    }
}

class searchuserlist{
    var profilePic : String?
    init(fromData data : [String:Any]) {
        self.profilePic = data["profilePic"] as? String
    }
}

class providertypes{
    
}
