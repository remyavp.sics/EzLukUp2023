//
//  AppDelegate.swift
//  EzLukUp
//
//  Created by Srishti on 14/09/22.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import FirebaseDynamicLinks
@main

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
       

        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        GoogleApi.shared.initialiseWithKey("AIzaSyA65srhYNsdTotca-WWRjLNoHKNanRXMq4")
        let logstat = UserDefaults.standard.value(forKey: "loggedin") ?? "false"
        if logstat as! String == "true" {
//            let appdelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let mainStoryboard: UIStoryboard = UIStoryboard(name: "Home", bundle: nil)
            var homeViewController = mainStoryboard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
            let nav = UINavigationController(rootViewController: homeViewController)
            nav.navigationBar.isHidden = true
            self.window!.rootViewController = nav
//            approoter()
        }
        else{
            print("Not logged in -- app delegate msg")
        }
        return true
    }
    func approoter(){
        let navigationController = self.window?.rootViewController as! UINavigationController
        
        let logstat = UserDefaults.standard.value(forKey: "loggedin") ?? "false"
        if logstat as! String == "true" {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if #available(iOS 14.0, *) {
                let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                navigationController.pushViewController(vc, animated: false)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WelcomeVC") as! WelcomeVC
            navigationController.pushViewController(vc, animated: false)
            
        }
    }

    // MARK: UISceneSession Lifecycle

//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "EzLukUp")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
   
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
            print("Deep link received \(url)")
            return true
        }
    
    
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
        if let incomingURL = userActivity.webpageURL {
            print("Incoming URL is :\(incomingURL)")
            let linkHandled = DynamicLinks.dynamicLinks().handleUniversalLink(incomingURL) { (dynamicLink, error) in
                guard error == nil else {
                    print("Found an error :\(String(describing: error?.localizedDescription))")
                    return
                }
                if let dynamicLink = dynamicLink {
                    self.handleIncomingDynamicLink(dynamicLink)
                }
            }
            if linkHandled {
                return true
            } else {
                return false
                // Handle Other cases
            }
        }
        return false
    }
    
    
    //MARK: - dynamic link incoming handler
    fileprivate func handleIncomingDynamicLink(_ dynamicLink:DynamicLink) {
        guard let url = dynamicLink.url else {
            print("link Object have no url")
            return
        }
        print("Incoming link parameter is :\(url.absoluteString)")
        var info: [String: String] = [:]
        
        URLComponents(url: url, resolvingAgainstBaseURL: false)?.queryItems?.forEach {
            info[$0.name] = $0.value
        }
        
        guard dynamicLink.matchType == .unique || dynamicLink.matchType == .default else {return}
        print(info)
        redirect(withInfo: info)
    }
    
    fileprivate func redirect(withInfo info:[String:Any]) {
        let navigationController = self.window?.rootViewController as! UINavigationController

        let logstat = UserDefaults.standard.value(forKey: "loggedin") ?? "false"
        if logstat as! String == "true" {
            let storyboard = UIStoryboard(name: "Home", bundle: nil)
            if #available(iOS 14.0, *) {
                let vc = storyboard.instantiateViewController(identifier: "CustomTabbarVC") as! CustomTabbarVC
                navigationController.pushViewController(vc, animated: false)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let vc = storyboard.instantiateViewController(identifier: "WelcomeVC") as! WelcomeVC
            navigationController.pushViewController(vc, animated: false)
        }

        if let value = info.first?.value {
//            print(value)
            print("invite by \(info["invitername"] ?? "") type of account suggested = \(info["type"] ?? "")")
            let product_id = (value as AnyObject).description
//            let destinationVC = StoryBoard.login.instantiateViewController(withIdentifier: "MHProductDetailsVC") as! MHProductDetailsVC
//            let navigationVC = UINavigationController(rootViewController: destinationVC)
//            navigationVC.navigationBar.isHidden = true
//            destinationVC.isFrom = .other
            //destinationVC.productID = product_id ?? ""
//            self.window?.rootViewController = navigationVC
//            self.window?.makeKeyAndVisible()
        }
    }

}

//let rootVC:LoginViewController = self.storyboard?.instantiateViewControllerWithIdentifier("LoginViewController") as! LoginViewController
//let nvc:UINavigationController = self.storyboard?.instantiateViewControllerWithIdentifier("RootNavigationController") as! UINavigationController
//nvc.viewControllers = [rootVC]
//UIApplication.sharedApplication().keyWindow?.rootViewController = nvc
