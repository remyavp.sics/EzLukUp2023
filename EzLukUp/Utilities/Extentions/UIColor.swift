//
//  UIColor.swift
//  DayToFresh
//
//  Created by farhan k on 10/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let lightGreen:UIColor = #colorLiteral(red: 0.2980392157, green: 0.8196078431, blue: 0.7803921569, alpha: 1)
    static let darkGreen : UIColor = #colorLiteral(red: 0, green: 0.3843137255, blue: 0.4588235294, alpha: 1)
    static let acceptGreen:UIColor = #colorLiteral(red: 0, green: 0.8, blue: 0.3490196078, alpha: 1)
    static let declineRed : UIColor = #colorLiteral(red: 0.8392156863, green: 0.01960784314, blue: 0.01960784314, alpha: 1)
    static let favYellow : UIColor = #colorLiteral(red: 1, green: 0.7333333333, blue: 0, alpha: 1)
    static let coralPink:UIColor = #colorLiteral(red: 1, green: 0.1411764706, blue: 0.3529411765, alpha: 1)
    static let orange : UIColor = #colorLiteral(red: 0.9725490196, green: 0.3843137255, blue: 0.3490196078, alpha: 1)
    static let purple : UIColor = #colorLiteral(red: 0.5058823529, green: 0.3333333333, blue: 0.7960784314, alpha: 1)
    static let theme : UIColor = #colorLiteral(red: 0.1490196078, green: 0.7411764706, blue: 0.4078431373, alpha: 1)
    static let themeDark : UIColor = #colorLiteral(red: 0.02352941176, green: 0.4039215686, blue: 0.3490196078, alpha: 1)
    
    static let unselectedGray:UIColor = UIColor.init(red: 78/255, green: 78/255, blue: 78/255, alpha: 1)
    static let unselectedLightGray: UIColor = UIColor.init(red: 209/255, green: 209/255, blue: 209/255, alpha: 1)
    static let menuCellLightGray : UIColor = UIColor.init(red: 201/255, green: 201/255, blue: 201/255, alpha: 0.84)
    static let planCellLight:UIColor = UIColor(red: 200/255, green: 200/255, blue: 200/255, alpha: 0.19)
    static let tvPlaceHolder:UIColor = UIColor(red: 0, green: 0, blue: 0 , alpha: 0.24)
    static let paleGreen: UIColor = UIColor(red: 229/255, green: 255/255, blue: 251/255, alpha: 0.5)
    
    class func darkGreenWithOpacity(_ opacity: CGFloat) -> UIColor {
        return darkGreen.withAlphaComponent(opacity)
    }
  
    static var officialApplePlaceholderGray: UIColor {
        return UIColor(red: 0, green: 0, blue: 0.0980392, alpha: 0.22)
    }
    
    struct KMColor {
        static let kmPrimaryDark = UIColor(named: "KMPrimaryDark")!
        static let kmPrimaryLight = UIColor(named: "KMPrimaryLight")!
        static let kmOrange = UIColor(named: "KMOrange")!
    }
    
    
    
}

extension UIColor{
    convenience init(hexColorCode: String) {
        var red:   CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue:  CGFloat = 0.0
        var alpha: CGFloat = 1.0
        
        if hexColorCode.hasPrefix("#") {
            let index = hexColorCode.index(hexColorCode.startIndex, offsetBy: 1)
            
            let hex     = String(hexColorCode[index...]) // hexColorCode.substring(from: index)
            print("hex :\(hex)")
            let scanner = Scanner(string: hex)
            var hexValue: CUnsignedLongLong = 0
            if scanner.scanHexInt64(&hexValue) {
                switch (hex.count) {
                case 3:
                    red   = CGFloat((hexValue & 0xF00) >> 8)       / 15.0
                    green = CGFloat((hexValue & 0x0F0) >> 4)       / 15.0
                    blue  = CGFloat(hexValue & 0x00F)              / 15.0
                case 4:
                    red   = CGFloat((hexValue & 0xF000) >> 12)     / 15.0
                    green = CGFloat((hexValue & 0x0F00) >> 8)      / 15.0
                    blue  = CGFloat((hexValue & 0x00F0) >> 4)      / 15.0
                    alpha = CGFloat(hexValue & 0x000F)             / 15.0
                case 6:
                    red   = CGFloat((hexValue & 0xFF0000) >> 16)   / 255.0
                    green = CGFloat((hexValue & 0x00FF00) >> 8)    / 255.0
                    blue  = CGFloat(hexValue & 0x0000FF)           / 255.0
                case 8:
                    red   = CGFloat((hexValue & 0xFF000000) >> 24) / 255.0
                    green = CGFloat((hexValue & 0x00FF0000) >> 16) / 255.0
                    blue  = CGFloat((hexValue & 0x0000FF00) >> 8)  / 255.0
                    alpha = CGFloat(hexValue & 0x000000FF)         / 255.0
                default:
                    print("Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8", terminator: "")
                }
            } else {
                print("Scan hex error")
            }
        } else {
            print("Invalid RGB string, missing '#' as prefix", terminator: "")
        }
        self.init(red:red, green:green, blue:blue, alpha:alpha)
    }
}
