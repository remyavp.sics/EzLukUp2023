//
//  Common.swift
//  DayToFresh
//
//  Created by farhan k on 11/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import Kingfisher

extension UISearchBar {
    
    func change(textFont : UIFont?,backGroundColor:UIColor?,textColor:UIColor?,tintColor:UIColor?,is_IOS_13_Available:Bool) {
        for view : UIView in (self.subviews[0]).subviews {
            if let textField = view as? UITextField {
                textField.font = textFont
                if let bgColor = backGroundColor {
                    textField.backgroundColor = bgColor
                }
                if let _textColor = textColor {
                    textField.textColor = _textColor
                }
                if let _tintColor = tintColor {
                    textField.tintColor = _tintColor
                }
            }
        } 
    }
}

extension UIImageView {
    func setImage(withUrl url: String?) {
        self.kf.setImage(with: URL(string: url ?? ""))
    }
    
}


extension Double {
    var cleanValue: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
    
    func rounded(digits: Int) -> Double {
        let multiplier = pow(10.0, Double(digits))
        return (self * multiplier).rounded() / multiplier
    }
    
    var getCurrency: String {
        
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_US")// Change this to another locale if you want to force a specific locale, otherwise this is redundant as the current locale is the default already
        formatter.numberStyle = .currency
        if let currencyDiscountAmount = formatter.string(from: self as NSNumber) {
            let newString = String(currencyDiscountAmount).replacingOccurrences(of: "$", with: "", options: .literal, range: nil)
            return String(newString) + " AED"
        } else {return ""}
    }
    
}


extension Array {
    func group<U: Hashable>(by key: (Element) -> U) -> [[Element]] {
        //keeps track of what the integer index is per group item
        var indexKeys = [U : Int]()

        var grouped = [[Element]]()
        for element in self {
            let key = key(element)

            if let ind = indexKeys[key] {
                grouped[ind].append(element)
            }
            else {
                grouped.append([element])
                indexKeys[key] = grouped.count - 1
            }
        }
        return grouped
    }

}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }

        return result
    }
}

struct CappedCollection<T> {

    private var elements: [T]
    var maxCount: Int

    init(elements: [T], maxCount: Int) {
        self.elements = elements
        self.maxCount = maxCount
    }
}

extension CappedCollection: Collection, ExpressibleByArrayLiteral {

    typealias Index = Int
    typealias Element = T

    init(arrayLiteral elements: Element...) {
        self.elements = elements
        maxCount = elements.count
    }

    var startIndex: Index { return elements.startIndex }
    var endIndex: Index { return elements.endIndex }

    subscript(index: Index) -> Iterator.Element {
        get { return elements[index] }
    }

    func index(after i: Index) -> Index {
        return elements.index(after: i)
    }

    @discardableResult
    mutating func append(_ newElement: Element) -> Element? {
        elements.append(newElement)
        return removeExtraElements().first
    }

    @discardableResult
    mutating func append<C>(contentsOf newElements: C) -> [Element] where C : Collection, CappedCollection.Element == C.Element {
        elements.append(contentsOf: newElements)
        return removeExtraElements()
    }

    @discardableResult
    mutating func insert(_ newElement: Element, at i: Int) -> Element? {
        elements.insert(newElement, at: i)
        return removeExtraElements().first
    }

    @discardableResult
    mutating func insert<C>(contentsOf newElements: C, at i: Int) -> [Element] where C : Collection, CappedCollection.Element == C.Element {
        elements.insert(contentsOf: newElements, at: i)
        return removeExtraElements()
    }

    private mutating func removeExtraElements() -> [Element] {
        guard elements.count > maxCount else { return [] }

        var poppedElements: [Element] = []
        poppedElements.append(contentsOf: elements[maxCount..<elements.count])
        elements.removeLast(elements.count - maxCount)
        return poppedElements
    }
    
    public var storedArray:[T] {
        return elements
    }
}
