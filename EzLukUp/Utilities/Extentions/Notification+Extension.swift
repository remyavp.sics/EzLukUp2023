//
//  Notification+Extension.swift
//  WayToNikah
//
//  Created by WC-64 on 22/03/21.
//  Copyright © 2021 appzoc. All rights reserved.
//

import Foundation


extension NSNotification.Name {
    
    static var didChangeNotificationCount:NSNotification.Name {
        return NSNotification.Name(rawValue: "didChangeNotificationCount")
    }
    
}

