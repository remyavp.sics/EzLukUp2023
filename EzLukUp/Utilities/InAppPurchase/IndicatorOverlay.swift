
import UIKit

class IndicatorOverlay: UIView
{
    static let indicatortag = 99103
    
    @IBOutlet fileprivate var activityIndicator:UIActivityIndicatorView!
    @IBOutlet fileprivate var overlayContainerView:UIView!
    @IBOutlet fileprivate var overlayMessageLbl:UILabel!
    static var parentView : UIView?  = nil
    
    /// To get the app window
    ///
    /// - Returns: return app window
    class func getAppWindow() -> UIWindow?
    {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.connectedScenes.filter({$0.activationState == .foregroundActive}).map({$0 as? UIWindowScene}).compactMap({$0}).first?.windows.filter({$0.isKeyWindow}).first
        } else {
            // Fallback on earlier versions
            return UIApplication.shared.keyWindow
        }
    }
    
    /// To initialize the indicator
    fileprivate func initialize()
    {
        overlayContainerView.clipsToBounds = true
        self.backgroundColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha:0.1)
        overlayContainerView.layer.cornerRadius = 5.0
        self.activityIndicator.startAnimating()
        self.isUserInteractionEnabled = false
    }
    
    /// To hide indicator
    fileprivate class func hideIndicator(view:UIView? = nil)
    {
        if(view != nil){
            
            view?.isUserInteractionEnabled = true
            if let overlay = view?.viewWithTag(indicatortag)
            {
                overlay.removeFromSuperview()
            }
            parentView = nil
        }
        else{
        if let window = IndicatorOverlay.getAppWindow()
        {
            window.isUserInteractionEnabled = true
            if let overlay = window.viewWithTag(indicatortag)
            {
                overlay.removeFromSuperview()
            }
        }
        }
    }
    
    /// To handle indicator hide
    class func hideActivityIndicator(view:UIView? = nil)
    {
        if Thread.isMainThread
        {
            hideIndicator(view: view)
        }
        else
        {
            DispatchQueue.main.async { () -> Void in
                
                hideIndicator(view: view)
            }
        }
    }
    
    /// To display acticity indicator with message
    ///
    /// - Parameter activityMessage: activityMessage
    class func showActivityIndicatorWithMessage(_ activityMessage:String, _ view:UIView? = nil)
    {
        if Thread.isMainThread
        {
            showIndicatorWithMessage(activityMessage, view)
        }
        else
        {
            DispatchQueue.main.async { () -> Void in
                
                hideActivityIndicator(view: view)
                showIndicatorWithMessage(activityMessage,view)
            }
        }
    }
    
    /// To display indicator with given on message
    ///
    /// - Parameter activityMessage: activityMessage
    private  class func showIndicatorWithMessage(_ activityMessage:String, _ view:UIView? = nil)
    {
        if(view != nil){
            
            self.parentView = view
            let overlay:IndicatorOverlay = Bundle.main.loadNibNamed("IndicatorOverlay", owner: self, options: nil)!.first as! IndicatorOverlay
            overlay.tag = indicatortag
            overlay.frame = UIScreen.main.bounds
            parentView?.addSubview(overlay)
            parentView?.isUserInteractionEnabled = false
            overlay.initialize()
            overlay.overlayMessageLbl.text = activityMessage

            
        }else
        {
        if let window = IndicatorOverlay.getAppWindow() //if app window is available
        {
            let overlay:IndicatorOverlay = Bundle.main.loadNibNamed("IndicatorOverlay", owner: self, options: nil)!.first as! IndicatorOverlay
            overlay.tag = indicatortag
            overlay.frame = UIScreen.main.bounds
            window.addSubview(overlay)
            window.isUserInteractionEnabled = false
            overlay.initialize()
            overlay.overlayMessageLbl.text = activityMessage
        }
        }
        
    }
}
