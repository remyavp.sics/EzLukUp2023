//
//  ToggleButton.swift
//  WayToNikkah
//
//  Created by Appzoc on 06/09/19.
//  Copyright © 2019 Mohamed Shafi. All rights reserved.
//

import UIKit


/// Initial Button state set to be selected by storyBoard
class ToggleButton: UIButton {
    
    @IBInspectable var unselectedImage : UIImage?
    @IBInspectable var selectedImage : UIImage?
    @IBInspectable var toggleImage : Bool = false
 
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateToggleState()
        self.addTarget(self, action: #selector(self.toggleState(_:)), for: .touchUpInside)
    }
    
    @objc func toggleState(_ sender:UIButton){
       // print("current : \(isSelected)")
        self.isSelected = !isSelected
       // print("after : \(isSelected)")
    }
    
    fileprivate func updateToggleState() {
        if toggleImage {
            if isSelected {
                DispatchQueue.main.async {
                    self.setImage(self.unselectedImage, for: .normal)
                }
            } else {
                DispatchQueue.main.async {
                    self.setImage(self.selectedImage, for: .normal)
                }
                
            }
        }
    }

}
