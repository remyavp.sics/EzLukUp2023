//
//  ShareManager.swift
//  DayToFresh
//
//  Created by farhan k on 27/02/20.
//  Copyright © 2020 Appzoc. All rights reserved.
//

import UIKit
import MessageUI

class ShareManager : NSObject , MFMailComposeViewControllerDelegate {
    
    static let shared = ShareManager()
    
    var contactNo:String?
    var contactMail:String?
    var subject:String?
    var viewController = UIViewController()
    
    private override init(){}
    
    
    
    func dialNumber(present onVC:UIViewController,_ number:String?) {
        self.contactNo = number ?? nil
        self.viewController = onVC
        self.contactNo = contactNo?.replacingOccurrences(of: " ", with: "")
        if self.contactNo == nil {
            self.showAlert(message: "Enter a valid contact number")
            return
        }
        DispatchQueue.main.async {
            if let url = URL(string: "tel://\(self.contactNo ?? "")"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler:nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            } else {
                // add error message here
                self.showAlert(message: "Enter a valid contact number")
                print("invalid contact no")
            }
        }
        
    }
    
    func sendMail(present onVC:UIViewController,_ mailID:String?,subject:String?) {
        self.contactMail = mailID ?? nil
        self.subject = subject ?? nil
        //        if self.contactMail == "" {
        //            BaseAlert.alert(withTitle: "Way To Nikah", message: "Sorry no emailid,Please try again later", tintColor: .black, andPresentOn: onVC)
        //            return
        //        }
        if self.contactMail == nil {
            //  self.showAlert(message: "Enter a valid email")
            return
        }
        
        DispatchQueue.main.async {
            let mailComposeViewController = self.configureMailComposer()
            if MFMailComposeViewController.canSendMail(){
                onVC.present(mailComposeViewController, animated: true, completion: nil)
            }else{
                print("Can't send email")
                //self.showAlert(message: "Mail has not configured in your device")
            }
        }
        
    }
    
    private func configureMailComposer() -> MFMailComposeViewController {
        let mailComposeVC = MFMailComposeViewController()
        mailComposeVC.mailComposeDelegate = self
        mailComposeVC.setToRecipients([self.contactMail!])
        if let _subject = self.subject {
            mailComposeVC.setSubject(_subject)
        }
        // mailComposeVC.setMessageBody("", isHTML: false)
        return mailComposeVC
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func shareAppLink(presentOn ViewController:UIViewController,yourMessage:String?,url:String?,yourImage:UIImage?){
        guard let _appUrl = URL(string: url ?? "") else { print("invalid app URL")
            return
        }
        var activityItems:[Any] = []
        if let message = yourMessage {
            activityItems.append(message)
        }
        if let image = yourImage {
            activityItems.append(image)
        }
        activityItems.append(_appUrl)
        let activityController = UIActivityViewController(activityItems: activityItems , applicationActivities: nil)
        
        ViewController.present(activityController, animated: true, completion: nil)
    }
    
    func openWhatsApp(presentOn ViewController:UIViewController,yourMessage:String?,number:String?){
        var url : String?
        if let message = yourMessage {
            // Contains Message
            let encodedMessage = message.addPrecentageEncoding()
            url = "send?phone=\(number ?? "-8")&text=\(encodedMessage)"
        } else {
            // No Message
            url = "send?phone=\(number ?? "-8")"
        }
        guard let openURL = url  else { return }
        if let appUrl = URL(string: "whatsapp://\(openURL)") ,UIApplication.shared.canOpenURL(appUrl) {
            print("Opening youtube app with url \(appUrl)")
            UIApplication.shared.open(appUrl, options: [:], completionHandler: nil)
        } else {
            if let message = yourMessage {
               let encodedMessage = message.addPrecentageEncoding()
                if let webURL = URL(string: "https://wa.me/\(number ?? "-8")?text=\(encodedMessage)"),UIApplication.shared.canOpenURL(webURL) {
                              UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                   }
            } else {
                if let webURL = URL(string: "https://wa.me/\(number ?? "-8")"),UIApplication.shared.canOpenURL(webURL) {
                    UIApplication.shared.open(webURL, options: [:], completionHandler: nil)
                }
            }
        }
        
    }
    

    
    private func showAlert(message:String) {
        if let infoDict = Bundle.main.infoDictionary {
            if let appName = infoDict["CFBundleDisplayName"]as? String {
                let alert = UIAlertController(title: appName, message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                self.viewController.present(alert, animated: true, completion: nil)
            }
        }
    }
    
}

