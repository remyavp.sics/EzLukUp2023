//
//  Constants.swift
//  EzLukUp
//
//  Created by REMYA V P on 19/10/22.
//

import Foundation
import Alamofire
import UIKit

let isNetworkReachable  = (NetworkReachabilityManager()?.isReachable)!
let kBaseUrl = "http://13.234.177.61/api7/"
let kImageUrl = "http://13.234.177.61/project/ezlukup/uploads/"
